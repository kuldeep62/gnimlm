-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 24, 2021 at 11:35 PM
-- Server version: 8.0.20
-- PHP Version: 7.3.24-(to be removed in future macOS)

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `level_plan`
--

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` bigint UNSIGNED NOT NULL,
  `captcha_time` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ci_session`
--

CREATE TABLE `ci_session` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_session`
--

INSERT INTO `ci_session` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0tmcod3aasvqpeohd6etvn79oa5dkn9c', '::1', 1600836537, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303833363533303b757365725f69647c733a353a2261646d696e223b726f6c657c733a313a2241223b),
('11buimmtm5if2i7r948o14sba7fkohon', '::1', 1615201948, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631353230313637373b),
('664gret593590hm5509rcskc6l206vr5', '::1', 1598620264, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383632303236323b757365725f69647c733a353a2261646d696e223b726f6c657c733a313a2241223b),
('ah19hbmk59khu5qa42muttn8c0pl2j2e', '::1', 1615228123, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631353232383131363b),
('b0v7acjrrueb6rvd9ehpa012tferm3q6', '::1', 1598683567, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638333535363b),
('buq8d7mfru6t5jc8aa46splk2f35kq6s', '::1', 1606546062, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630363534363035343b),
('bv95khmgumere0kfugrdib0cdpaaem3s', '::1', 1615202549, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631353230323534393b),
('e6rtm7o0bafk7v9s07ghn5gnt174ec38', '::1', 1610476048, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631303437363034383b),
('eb0qi0l7jlafgppqo5t8ioeen29f8bqp', '::1', 1615202655, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631353230323534393b),
('eqj3t5pp7673gcm45tule4tjtveu7okk', '::1', 1615202717, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631353230323730343b),
('g2j7j7n3mj3a7jm88f9hasp88f0jteql', '::1', 1615200226, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631353230303232363b),
('gohib64ffp9rati1pvigudrqngkukl79', '::1', 1598620262, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383632303236323b757365725f69647c733a353a2261646d696e223b726f6c657c733a313a2241223b),
('i2tmm2m23q3aimv24hc5f3ene29rnlc2', '::1', 1621899109, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632313839393130393b757365725f69647c733a353a2261646d696e223b726f6c657c733a313a2241223b),
('ij6e5qgqgomjk3gofavd5omjl1ngmmp9', '::1', 1621899260, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632313839393130393b757365725f69647c733a353a2261646d696e223b726f6c657c733a313a2241223b),
('jt929uftluqqtuqm4e8agim1cs0tgn0k', '::1', 1598618709, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383631383730393b757365725f69647c733a353a2261646d696e223b726f6c657c733a313a2241223b),
('r3b7jun637ga1h3s2l4t5kd11aufcg2v', '::1', 1606229377, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630363232393337363b),
('r3nl130bt8oghgd6cqvincsnhoti1jk6', '::1', 1615200393, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631353230303232363b757365725f69647c733a353a2261646d696e223b726f6c657c733a313a2241223b),
('vsebvdete0bon1ieji05ppsmjq8eoco9', '::1', 1615200171, 0x5f5f63695f6c6173745f726567656e65726174657c693a313631353139393837353b);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_address`
--

CREATE TABLE `tbl_address` (
  `id` int NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `address_line1` varchar(100) NOT NULL,
  `address_line2` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `role` varchar(10) NOT NULL,
  `access` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `user_id`, `password`, `role`, `access`, `email`, `name`, `created_at`) VALUES
(2, 'admin', 'admin', 'A', '', 'admin@gmail.com', 'Admin', '2020-05-30 20:30:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cron`
--

CREATE TABLE `tbl_cron` (
  `id` int NOT NULL,
  `cron_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_epins`
--

CREATE TABLE `tbl_epins` (
  `id` int NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `epin` varchar(100) NOT NULL,
  `amount` int NOT NULL,
  `status` int NOT NULL,
  `sender_id` varchar(50) NOT NULL,
  `used_for` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_epins`
--

INSERT INTO `tbl_epins` (`id`, `user_id`, `epin`, `amount`, `status`, `sender_id`, `used_for`, `created_at`) VALUES
(1, 'first_user', '969d9d1889f0cb989e420f1510facc92', 500, 1, '', 'first_user', '2020-05-28 22:38:58'),
(2, 'first_user', '29b020bee905094207e94d51e67ae554', 500, 1, '', 'first_user', '2020-05-28 22:38:58'),
(3, 'first_user', 'c29a8ae7f6f255e4c5cdea17259313cd', 500, 1, '', 'first_user', '2020-05-28 22:38:58'),
(4, 'first_user', 'b6de0ff2b36836992888793befaaccdb', 1000, 1, '', 'GNI1005', '2020-05-28 22:38:58'),
(5, 'first_user', '4e8a194f563f716dcb9465b00b71949a', 1000, 1, '', 'GNI8212', '2020-05-28 22:38:58'),
(6, 'first_user', '40b245ff8d2bfa655dce14b50639251c', 500, 1, '', 'GNI2857', '2020-05-28 22:38:58'),
(7, 'first_user', '0299649600be1833374273b791f87300', 500, 1, '', 'GNI4830', '2020-05-28 22:38:58'),
(8, 'first_user', '6ec15c6cc7ffe3047495cc7305ac7988', 500, 1, '', 'GNI3525', '2020-05-28 22:38:58'),
(9, 'first_user', '8e2231817364f2ec03a475c39271caa0', 500, 1, '', 'GNI3067', '2020-05-28 22:38:58'),
(10, 'first_user', '5f0ac65f675258f8797c4b2b1b5b1ade', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(11, 'first_user', '2ff92e98c96e7ec3df5ab3a51742e208', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(12, 'first_user', '59d573f1c24aad2778adf37cdbbd2bff', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(13, 'first_user', '31be9e9d140fe4ea9dca65d3ef0918b0', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(14, 'first_user', 'eff36f22e382a074f921c6799c5a1e0a', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(15, 'first_user', 'bc3c57f35453afa7326a2adb8e6cca49', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(16, 'first_user', '25e2717f7356d2d840b713d8a860ade6', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(17, 'first_user', '6cf43f6ce54b53e058e97c6ba549e193', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(18, 'first_user', 'd5f7711d06ac044729ff539239b02ddc', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(19, 'first_user', '3c25d60e5eca9b6e34f0979d8d92259b', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(20, 'first_user', '623aceb395b2cb3898c5a96e85d729f2', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(21, 'first_user', 'ac1cebd3afd3c220c2998b7ae5bcfdec', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(22, 'first_user', 'c961f0ede903c2a6a6565224604a8888', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(23, 'first_user', '373efc85f5d219a619a5c0533e06032c', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(24, 'first_user', 'f755f6b092f19429e6f7974ddc6b47a2', 500, 2, '', 'first_user', '2020-05-28 22:38:58'),
(25, 'first_user', 'e614c3e415155b0b3890bbf78d8b88f3', 500, 2, '', 'GNI8212', '2020-05-30 22:38:58'),
(26, 'first_user', '868fd3efd89b89753d28b218bd01abd7', 500, 2, '', 'GNI8212', '2020-05-28 22:38:58'),
(27, 'first_user', '7d5ce00cf3d54fc441d64633f7cd7341', 500, 2, '', 'GNI8212', '2020-05-30 22:38:58'),
(28, 'first_user', 'c73c0e6254e82d718f4c98f22142b3e3', 500, 1, '', 'GNI2916', '2020-05-30 22:38:58'),
(29, 'first_user', '9b5d4490161feb7299d679c0571e3e2b', 500, 1, '', 'GNI7827', '2020-05-30 22:38:58'),
(30, 'first_user', 'e6457734cc50eee11f4faf73bca254a6', 500, 0, '', '', '2020-05-30 22:38:58'),
(31, 'first_user', 'da4f6b3e9d6ebd10f794a3deba76e80e', 500, 1, '', 'GNI5575', '2020-05-30 22:38:58'),
(32, 'first_user', '98e73a029ded6c808a8d46cbb6fae23c', 500, 0, '', '', '2020-05-30 22:38:58'),
(33, 'first_user', '60cb8d5560915e347dd3b17f9e8a9836', 500, 0, '', '', '2020-05-30 22:38:58'),
(34, 'first_user', '1117b86df4e2fd3de2630fee88d7c046', 500, 0, '', '', '2020-05-30 22:38:58'),
(35, 'first_user', '69cde804f52ef45d4719dd9e83ba57d9', 500, 0, '', '', '2020-05-30 22:38:58'),
(36, 'first_user', '872af6e861b20e4f27b539c77d4edc58', 500, 0, '', '', '2020-05-30 22:38:58'),
(37, 'first_user', '2f9e5a16b22ffde249e3b944f31da570', 500, 0, '', '', '2020-05-30 22:38:58'),
(38, 'first_user', '82e934b62031a15209accd28031c698f', 500, 0, '', '', '2020-05-30 22:38:58'),
(39, 'first_user', '5b3b2275d9971d3cc7f6dc0a325a9e65', 500, 0, '', '', '2020-05-30 22:38:58'),
(40, 'first_user', '46bfce173c79afc72ca7f7a0b1b4ce88', 500, 0, '', '', '2020-05-30 22:38:58'),
(41, 'first_user', '3b4af5ae063abf4b41b371304e4bc402', 500, 0, '', '', '2020-05-30 22:38:58'),
(42, 'first_user', 'bcaa5e4222d0c403ec9f21b54f5fbf67', 500, 0, '', '', '2020-05-30 22:38:58'),
(43, 'first_user', '73d8fc669cd93dc2e5221910ef3038d2', 500, 0, '', '', '2020-05-30 22:38:58'),
(44, 'first_user', '895a18fb0ae75d9390678204a4ddb453', 500, 0, '', '', '2020-05-30 22:38:58'),
(45, 'first_user', '036e86ab5722be5624fd455af925cca9', 500, 0, '', '', '2020-05-30 22:38:58'),
(46, 'GNI1005', 'e62e862c2b84631e2aa258d01e196374', 1000, 0, '', '', '2020-06-04 20:31:04'),
(47, 'GNI1005', '14fc8a45bb8116d774cd6a89d5c30e83', 1000, 0, '', '', '2020-06-04 20:31:04'),
(48, 'GNI1005', '40710f04ab5e348a59ed02f04b4fd3f2', 1000, 0, '', '', '2020-06-04 20:31:04'),
(49, 'first_user', '5ec3d57ed51e64d743b6e4f3449332fd', 1000, 1, '', 'GNI6007', '2020-06-06 06:56:22'),
(50, 'first_user', 'c21c8c9027f623988778285c976959f8', 1000, 1, '', 'GNI7348', '2020-06-06 06:56:22'),
(51, 'first_user', '688ec772f65e4bc7b22dd9760935251d', 1000, 0, '', '', '2020-06-06 06:56:22'),
(52, 'first_user', 'a1a2a19417f8da19cf1ac2c0a2cc0c0a', 200, 2, 'first_user', 'GNI8212', '2020-06-08 23:03:19'),
(53, 'first_user', 'b1be58be2c59a8a1564dcf6361ff37f1', 200, 2, 'first_user', 'GNI8212', '2020-06-08 23:03:19'),
(54, 'first_user', 'b3f38e0a770652683feebf2b0d17687f', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(55, 'first_user', '78b821dbdf5253e76978976b5145f28a', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(56, 'first_user', '1e09fcea99b2d63ac8938783ffd88ec7', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(57, 'first_user', '2b4e80f82651ff2a0d9c3f1a781e299b', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(58, 'first_user', 'be405b642c343a947bca605511820f55', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(59, 'first_user', '8e3d40e619560ad7c0733508fa9acd8e', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(60, 'first_user', 'ba0cb92b07e62067daa3e56b0df251ef', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(61, 'first_user', '101baa9c6eca81db269c81bacede96fa', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(62, 'first_user', 'b353ae98ea07d474eac3e07a65286c15', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(63, 'first_user', '346e67a2e284949ed71194667136720d', 200, 0, 'first_user', '', '2020-06-08 23:03:19'),
(64, 'first_user', '3394903ade9b5846fad3bb14170627be', 200, 0, 'first_user', '', '2020-06-08 23:03:31'),
(65, 'GNI8212', '1c41a00042e45f8432f35e1a82274fd0', 200, 0, 'first_user', '', '2020-06-08 23:03:43'),
(66, 'GNI8212', '55c2c497202ec50488bef333557d52ed', 500, 0, 'first_user', '', '2020-06-09 11:21:47'),
(67, 'GNI8212', 'a9153d00686d85bd91ac2eba3e1ff1d6', 500, 0, 'first_user', '', '2020-06-09 11:21:47'),
(68, 'GNI8212', '69834733eeb2841ff3865a0f954258dc', 200, 0, 'first_user', '', '2020-06-09 11:21:59'),
(69, 'GNI8212', '7c197f29cdd5fd4e17293ce7a649010a', 200, 0, 'first_user', '', '2020-06-09 11:21:59'),
(70, 'first_user', '410eeb0c76d3719b15c53ed949125e73', 500, 0, 'first_user', '', '2020-06-09 16:14:44'),
(71, 'first_user', '2389f9aa59eb673d28f4b6cc5d9d17d9', 500, 0, 'first_user', '', '2020-06-09 16:15:18'),
(72, 'first_user', '60b6bb407a6f09a84d43d8fc89088ced', 500, 1, 'first_user', 'GNI6670', '2020-06-09 16:15:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_income_wallet`
--

CREATE TABLE `tbl_income_wallet` (
  `id` int NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_income_wallet`
--

INSERT INTO `tbl_income_wallet` (`id`, `user_id`, `amount`, `type`, `description`, `created_at`) VALUES
(184042, 'first_user', '50', 'direct_income', 'Direct Income from Activation of Member GNI8212', '2020-06-06 06:48:17'),
(184043, 'first_user', '50', 'direct_income', 'Direct Income from Activation of Member GNI2857', '2020-06-06 06:49:14'),
(184044, 'first_user', '50', 'direct_income', 'Direct Income from Activation of Member GNI4830', '2020-06-06 06:50:42'),
(184045, 'GNI8212', '50', 'direct_income', 'Direct Income from Activation of Member GNI3525', '2020-06-06 06:54:18'),
(184046, 'GNI8212', '50', 'direct_income', 'Direct Income from Activation of Member GNI3067', '2020-06-06 06:55:01'),
(184047, 'GNI8212', '25', 'level_income', 'Level Income from Activation of Member GNI3067 At level 1', '2020-06-06 06:55:01'),
(184048, 'first_user', '20', 'level_income', 'Level Income from Activation of Member GNI3067 At level 2', '2020-06-06 06:55:01'),
(184049, 'GNI3067', '100', 'direct_income', 'Direct Income from Activation of Member GNI7348', '2020-06-06 07:03:30'),
(184050, 'GNI3067', '50', 'level_income', 'Level Income from Activation of Member GNI7348 At level 1', '2020-06-06 07:03:30'),
(184051, 'GNI8212', '40', 'level_income', 'Level Income from Activation of Member GNI7348 At level 2', '2020-06-06 07:03:30'),
(184052, 'first_user', '30', 'level_income', 'Level Income from Activation of Member GNI7348 At level 3', '2020-06-06 07:03:30'),
(184053, 'first_user', '0', 'pin_generation', 'Amount Used For Pin Generation', '2020-06-09 16:14:44'),
(184054, 'first_user', '-1', 'pin_generation', 'Amount Used For Pin Generation', '2020-06-09 16:15:18'),
(184055, 'first_user', '-500', 'pin_generation', 'Amount Used For Pin Generation', '2020-06-09 16:15:39'),
(184056, 'GNI7348', '100', 'direct_income', 'Direct Income from Activation of Member GNI3512', '2020-06-09 23:51:44'),
(184057, 'GNI7348', '50', 'level_income', 'Level Income from Activation of Member GNI3512 At level 1', '2020-06-09 23:51:44'),
(184058, 'GNI3067', '40', 'level_income', 'Level Income from Activation of Member GNI3512 At level 2', '2020-06-09 23:51:44'),
(184059, 'GNI8212', '30', 'level_income', 'Level Income from Activation of Member GNI3512 At level 3', '2020-06-09 23:51:44'),
(184060, 'first_user', '20', 'level_income', 'Level Income from Activation of Member GNI3512 At level 4', '2020-06-09 23:51:44'),
(184061, 'GNI3512', '50', 'direct_income', 'Direct Income from Activation of Member GNI2916', '2020-06-10 00:10:38'),
(184062, 'GNI3512', '25', 'level_income', 'Level Income from Activation of Member GNI2916 At level 1', '2020-06-10 00:10:38'),
(184063, 'GNI7348', '20', 'level_income', 'Level Income from Activation of Member GNI2916 At level 2', '2020-06-10 00:10:38'),
(184064, 'GNI3067', '15', 'level_income', 'Level Income from Activation of Member GNI2916 At level 3', '2020-06-10 00:10:38'),
(184065, 'GNI8212', '10', 'level_income', 'Level Income from Activation of Member GNI2916 At level 4', '2020-06-10 00:10:38'),
(184066, 'first_user', '5', 'level_income', 'Level Income from Activation of Member GNI2916 At level 5', '2020-06-10 00:10:38'),
(184067, 'first_user', '50', 'direct_income', 'Direct Income from Activation of Member GNI4133', '2020-06-10 00:11:27'),
(184068, 'first_user', '25', 'level_income', 'Level Income from Activation of Member GNI4133 At level 1', '2020-06-10 00:11:27'),
(184071, 'first_user', '-200', 'withdraw_request', 'Withdraw Request', '2020-06-12 07:29:57'),
(184072, 'first_user', '-20', 'fund_generation', 'Amount Used for fund generation', '2020-06-12 08:47:47'),
(184073, 'first_user', '200', 'withdraw_reject', 'sfd fsd', '2020-06-12 21:18:47'),
(184074, 'first_user', '-200', 'withdraw_request', 'Withdraw Request', '2020-06-12 21:21:57'),
(184075, 'first_user', '160', 'pool_income', 'test amount', '2020-06-14 17:11:02'),
(184078, 'GNI2916', '100', 'direct_income', 'Direct Income from Activation of Member GNI6007', '2020-06-17 06:49:36'),
(184079, 'GNI2916', '50', 'level_income', 'Level Income from Activation of Member GNI6007 At level 1', '2020-06-17 06:49:36'),
(184080, 'GNI3512', '40', 'level_income', 'Level Income from Activation of Member GNI6007 At level 2', '2020-06-17 06:49:36'),
(184081, 'GNI7348', '30', 'level_income', 'Level Income from Activation of Member GNI6007 At level 3', '2020-06-17 06:49:36'),
(184082, 'GNI3067', '20', 'level_income', 'Level Income from Activation of Member GNI6007 At level 4', '2020-06-17 06:49:36'),
(184083, 'GNI8212', '10', 'level_income', 'Level Income from Activation of Member GNI6007 At level 5', '2020-06-17 06:49:36'),
(184084, 'GNI6007', '50', 'direct_income', 'Direct Income from Activation of Member GNI7827', '2020-06-17 06:50:04'),
(184085, 'GNI2857', '250', 'pool_income', 'Pool Income At Level 1', '2020-06-17 06:50:04'),
(184086, 'first_user', '500', 'pool_income', 'Pool Income At Level 2', '2020-06-17 06:50:04'),
(184087, 'GNI6007', '25', 'level_income', 'Level Income from Activation of Member GNI7827 At level 1', '2020-06-17 06:50:04'),
(184088, 'GNI2916', '20', 'level_income', 'Level Income from Activation of Member GNI7827 At level 2', '2020-06-17 06:50:04'),
(184089, 'GNI3512', '15', 'level_income', 'Level Income from Activation of Member GNI7827 At level 3', '2020-06-17 06:50:04'),
(184090, 'GNI7348', '10', 'level_income', 'Level Income from Activation of Member GNI7827 At level 4', '2020-06-17 06:50:04'),
(184091, 'GNI3067', '5', 'level_income', 'Level Income from Activation of Member GNI7827 At level 5', '2020-06-17 06:50:04'),
(184092, 'first_user', '50', 'direct_income', 'Direct Income from Activation of Member GNI6670', '2020-06-17 06:53:15'),
(184093, 'first_user', '25', 'level_income', 'Level Income from Activation of Member GNI6670 At level 1', '2020-06-17 06:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `news` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `title`, `news`, `created_at`) VALUES
(9, 'Registration Alert', 'Registration Will be Started From 1st July.', '2020-06-18 07:34:19'),
(10, 'Pin Distribution', 'Pins Distribution Starts from 25th June.', '2020-06-18 07:39:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package`
--

CREATE TABLE `tbl_package` (
  `id` int NOT NULL,
  `title` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `price` float NOT NULL,
  `direct_income` varchar(50) NOT NULL,
  `level_income` varchar(100) NOT NULL,
  `pool_income` varchar(30) NOT NULL,
  `capping` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_package`
--

INSERT INTO `tbl_package` (`id`, `title`, `description`, `price`, `direct_income`, `level_income`, `pool_income`, `capping`, `created_at`) VALUES
(2, 'Basic Package', 'this is basic package', 500, '50', '25,20,15,10,5', '250,500,750', 0, '2020-05-30 22:31:52'),
(3, 'Profession Package', 'this is professional pack', 1000, '100', '50,40,30,20,10', '500,1000,1500', 0, '2020-05-30 22:31:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_request`
--

CREATE TABLE `tbl_payment_request` (
  `id` int NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `payment_method` varchar(50) NOT NULL,
  `amount` int NOT NULL,
  `image` varchar(50) NOT NULL,
  `status` int NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment_request`
--

INSERT INTO `tbl_payment_request` (`id`, `user_id`, `payment_method`, `amount`, `image`, `status`, `remarks`, `created_at`) VALUES
(31, 'first_user', 'bank', 5000, 'payment_slip.png', 1, '', '2020-06-07 20:11:25'),
(32, 'first_user', 'googlepay', 30, 'payment_slip1.png', 1, 'approved', '2020-06-09 22:38:18'),
(33, 'GNI7348', 'bank', 1000, 'payment_slip2.png', 0, '', '2020-07-05 08:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_point_matching_income`
--

CREATE TABLE `tbl_point_matching_income` (
  `id` int NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `left_bv` int NOT NULL,
  `right_bv` int NOT NULL,
  `amount` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pool`
--

CREATE TABLE `tbl_pool` (
  `id` int NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `upline_id` varchar(30) NOT NULL,
  `level1` int NOT NULL,
  `level2` int NOT NULL,
  `level3` int NOT NULL,
  `level1income` int NOT NULL,
  `level2income` int NOT NULL,
  `level3income` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pool`
--

INSERT INTO `tbl_pool` (`id`, `user_id`, `upline_id`, `level1`, `level2`, `level3`, `level1income`, `level2income`, `level3income`, `created_at`) VALUES
(1, 'first_user', 'none', 3, 9, 1, 0, 0, 0, '2020-06-05 21:45:15'),
(2, 'GNI1005', 'first_user', 3, 1, 0, 0, 0, 0, '2020-06-05 21:45:58'),
(3, 'GNI8212', 'first_user', 3, 0, 0, 0, 0, 0, '2020-06-06 06:48:17'),
(4, 'GNI2857', 'first_user', 3, 0, 0, 0, 0, 0, '2020-06-06 06:49:14'),
(5, 'GNI4830', 'GNI1005', 1, 0, 0, 0, 0, 0, '2020-06-06 06:50:42'),
(6, 'GNI3525', 'GNI1005', 0, 0, 0, 0, 0, 0, '2020-06-06 06:54:18'),
(7, 'GNI3067', 'GNI1005', 0, 0, 0, 0, 0, 0, '2020-06-06 06:55:01'),
(8, 'GNI7348', 'GNI8212', 0, 0, 0, 0, 0, 0, '2020-06-06 07:03:30'),
(9, 'GNI3512', 'GNI8212', 0, 0, 0, 0, 0, 0, '2020-06-09 23:51:44'),
(10, 'GNI2916', 'GNI8212', 0, 0, 0, 0, 0, 0, '2020-06-10 00:10:38'),
(11, 'GNI4133', 'GNI2857', 0, 0, 0, 0, 0, 0, '2020-06-10 00:11:27'),
(12, 'GNI6007', 'GNI2857', 0, 0, 0, 0, 0, 0, '2020-06-17 06:49:36'),
(13, 'GNI7827', 'GNI2857', 0, 0, 0, 0, 0, 0, '2020-06-17 06:50:04'),
(14, 'GNI6670', 'GNI4830', 0, 0, 0, 0, 0, 0, '2020-06-17 06:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_popup`
--

CREATE TABLE `tbl_popup` (
  `id` int NOT NULL,
  `media` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `caption` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_popup`
--

INSERT INTO `tbl_popup` (`id`, `media`, `type`, `caption`, `created_at`) VALUES
(1, 'Ww3Af0CtpjQ', 'video', 'Now WithDraw Timing is 8AM to 11AM And Minimum withdraw Rs.200 and Multiple by Rs. 200', '2020-06-13 10:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roi`
--

CREATE TABLE `tbl_roi` (
  `id` int NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `amount` int NOT NULL,
  `type` varchar(50) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `status` int NOT NULL,
  `level` int NOT NULL,
  `days` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider_images`
--

CREATE TABLE `tbl_slider_images` (
  `id` int NOT NULL,
  `image` varchar(50) NOT NULL,
  `caption` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sms_counter`
--

CREATE TABLE `tbl_sms_counter` (
  `id` int NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `message` varchar(300) NOT NULL,
  `response` varchar(300) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sponser_count`
--

CREATE TABLE `tbl_sponser_count` (
  `id` int NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `position` varchar(5) NOT NULL,
  `downline_id` varchar(20) NOT NULL,
  `level` int NOT NULL,
  `paid_status` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sponser_count`
--

INSERT INTO `tbl_sponser_count` (`id`, `user_id`, `position`, `downline_id`, `level`, `paid_status`, `created_at`) VALUES
(150116, 'first_user', '', 'GNI8212', 1, 1, '0000-00-00 00:00:00'),
(150117, 'none', '', 'GNI8212', 2, 1, '0000-00-00 00:00:00'),
(150118, 'first_user', '', 'GNI2857', 1, 1, '0000-00-00 00:00:00'),
(150119, 'none', '', 'GNI2857', 2, 1, '0000-00-00 00:00:00'),
(150120, 'first_user', '', 'GNI4830', 1, 1, '0000-00-00 00:00:00'),
(150121, 'none', '', 'GNI4830', 2, 1, '0000-00-00 00:00:00'),
(150122, 'first_user', '', 'GNI5575', 1, 0, '0000-00-00 00:00:00'),
(150123, 'none', '', 'GNI5575', 2, 0, '0000-00-00 00:00:00'),
(150124, 'first_user', '', 'GNI6670', 1, 1, '0000-00-00 00:00:00'),
(150125, 'none', '', 'GNI6670', 2, 1, '0000-00-00 00:00:00'),
(150126, 'first_user', '', 'GNI4133', 1, 1, '0000-00-00 00:00:00'),
(150127, 'none', '', 'GNI4133', 2, 1, '0000-00-00 00:00:00'),
(150128, 'GNI8212', '', 'GNI3067', 1, 1, '0000-00-00 00:00:00'),
(150129, 'first_user', '', 'GNI3067', 2, 1, '0000-00-00 00:00:00'),
(150130, 'none', '', 'GNI3067', 3, 1, '0000-00-00 00:00:00'),
(150131, 'GNI3067', '', 'GNI7348', 1, 1, '0000-00-00 00:00:00'),
(150132, 'GNI8212', '', 'GNI7348', 2, 1, '0000-00-00 00:00:00'),
(150133, 'first_user', '', 'GNI7348', 3, 1, '0000-00-00 00:00:00'),
(150134, 'none', '', 'GNI7348', 4, 1, '0000-00-00 00:00:00'),
(150135, 'GNI7348', '', 'GNI3512', 1, 1, '0000-00-00 00:00:00'),
(150136, 'GNI3067', '', 'GNI3512', 2, 1, '0000-00-00 00:00:00'),
(150137, 'GNI8212', '', 'GNI3512', 3, 1, '0000-00-00 00:00:00'),
(150138, 'first_user', '', 'GNI3512', 4, 1, '0000-00-00 00:00:00'),
(150139, 'none', '', 'GNI3512', 5, 1, '0000-00-00 00:00:00'),
(150140, 'GNI3512', '', 'GNI2916', 1, 1, '0000-00-00 00:00:00'),
(150141, 'GNI7348', '', 'GNI2916', 2, 1, '0000-00-00 00:00:00'),
(150142, 'GNI3067', '', 'GNI2916', 3, 1, '0000-00-00 00:00:00'),
(150143, 'GNI8212', '', 'GNI2916', 4, 1, '0000-00-00 00:00:00'),
(150144, 'first_user', '', 'GNI2916', 5, 1, '0000-00-00 00:00:00'),
(150145, 'none', '', 'GNI2916', 6, 1, '0000-00-00 00:00:00'),
(150146, 'GNI2857', '', 'GNI7546', 1, 0, '0000-00-00 00:00:00'),
(150147, 'first_user', '', 'GNI7546', 2, 0, '0000-00-00 00:00:00'),
(150148, 'none', '', 'GNI7546', 3, 0, '0000-00-00 00:00:00'),
(150149, 'GNI2916', '', 'GNI6007', 1, 1, '0000-00-00 00:00:00'),
(150150, 'GNI3512', '', 'GNI6007', 2, 1, '0000-00-00 00:00:00'),
(150151, 'GNI7348', '', 'GNI6007', 3, 1, '0000-00-00 00:00:00'),
(150152, 'GNI3067', '', 'GNI6007', 4, 1, '0000-00-00 00:00:00'),
(150153, 'GNI8212', '', 'GNI6007', 5, 1, '0000-00-00 00:00:00'),
(150154, 'first_user', '', 'GNI6007', 6, 1, '0000-00-00 00:00:00'),
(150155, 'none', '', 'GNI6007', 7, 1, '0000-00-00 00:00:00'),
(150156, 'GNI6007', '', 'GNI7827', 1, 1, '0000-00-00 00:00:00'),
(150157, 'GNI2916', '', 'GNI7827', 2, 1, '0000-00-00 00:00:00'),
(150158, 'GNI3512', '', 'GNI7827', 3, 1, '0000-00-00 00:00:00'),
(150159, 'GNI7348', '', 'GNI7827', 4, 1, '0000-00-00 00:00:00'),
(150160, 'GNI3067', '', 'GNI7827', 5, 1, '0000-00-00 00:00:00'),
(150161, 'GNI8212', '', 'GNI7827', 6, 1, '0000-00-00 00:00:00'),
(150162, 'first_user', '', 'GNI7827', 7, 1, '0000-00-00 00:00:00'),
(150163, 'none', '', 'GNI7827', 8, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_support_message`
--

CREATE TABLE `tbl_support_message` (
  `id` int NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `subject` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `message` varchar(3000) NOT NULL,
  `status` int NOT NULL,
  `remark` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_support_message`
--

INSERT INTO `tbl_support_message` (`id`, `user_id`, `subject`, `message`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(135, 'first_user', 'account_activation', 'this is an test message', 1, 'query resolved', '2020-06-13 07:58:49', '2020-06-13 02:28:49'),
(136, 'first_user', 'kyc', 'sd fdfsdfsdfsdfsdsd', 1, 'this is an updated request  jldkf sdlkf sjdfklsj f', '2020-06-13 07:59:15', '2020-06-13 02:29:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int NOT NULL,
  `user_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sponser_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `upline_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `directs` int NOT NULL,
  `master_key` int NOT NULL,
  `paid_status` int NOT NULL,
  `package_id` int NOT NULL,
  `package_amount` int NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `phone` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `otp` int NOT NULL,
  `wallet_amount` float NOT NULL,
  `income` float NOT NULL,
  `pin_amount` float NOT NULL,
  `disabled` int NOT NULL,
  `capping` int NOT NULL,
  `topup_date` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_id`, `password`, `sponser_id`, `upline_id`, `directs`, `master_key`, `paid_status`, `package_id`, `package_amount`, `name`, `phone`, `email`, `user_type`, `role`, `otp`, `wallet_amount`, `income`, `pin_amount`, `disabled`, `capping`, `topup_date`, `created_at`) VALUES
(15536, 'first_user', '123', 'none', 'none', 2, 12, 1, 2, 500, 'First User121', '1231231231', 'kushmalout@gmail.com', '0', 'U', 0, 567, 954, 0, 0, 0, '2020-06-05 16:15:15', '2020-05-20 22:28:43'),
(15537, 'GNI1005', '123123', 'first_user', '', 0, 7726, 1, 2, 500, 'kush', '7710562000', '', '', 'U', 0, 0, 0, 0, 0, 0, '2020-06-05 16:15:58', '2020-05-22 17:39:27'),
(15538, 'GNI8212', '123123', 'first_user', '', 0, 5506, 1, 2, 500, 'kush', '7710562000', '', '', 'U', 0, 0, 215, 0, 0, 0, '2020-06-06 01:18:17', '2020-05-24 11:15:27'),
(15539, 'GNI2857', '123123', 'first_user', '', 0, 6846, 1, 2, 500, 'kush', '7710562000', '', '', 'U', 0, 0, 250, 0, 0, 0, '2020-06-06 01:19:14', '2020-05-25 11:15:39'),
(15540, 'GNI4830', '123123', 'first_user', '', 0, 4285, 1, 2, 500, 'kush', '7710562000', '', '', 'U', 0, 0, 0, 0, 0, 0, '2020-06-06 01:20:42', '2020-05-25 11:16:45'),
(15541, 'GNI5575', '123123', 'first_user', '', 0, 8113, 0, 0, 500, 'kush', '7710562000', '', '', 'U', 0, 0, 0, 0, 0, 0, '2020-06-04 17:34:10', '2020-05-26 11:17:40'),
(15542, 'GNI6670', '123123', 'first_user', '', 0, 8462, 1, 2, 500, 'kush', '7710562000', '', '', 'U', 0, 0, 0, 0, 0, 0, '2020-06-17 06:53:15', '2020-05-26 11:18:54'),
(15543, 'GNI4133', '123123', 'first_user', '', 0, 2141, 1, 2, 500, 'kush', '7710562000', '', '', 'U', 0, 0, 0, 0, 0, 0, '2020-06-10 00:11:27', '2020-05-31 11:19:18'),
(15544, 'GNI3525', '123', 'GNI8212', '', 0, 5661, 1, 2, 500, 'second user', '7710562000', '', '', 'U', 0, 0, 0, 0, 0, 0, '2020-06-06 01:24:18', '2020-06-03 20:20:20'),
(15545, 'GNI3067', '123', 'GNI8212', '', 0, 8707, 1, 2, 500, 'second user', '7710562000', '', '', 'U', 0, 0, 230, 0, 0, 0, '2020-06-06 01:25:01', '2020-06-03 20:21:07'),
(15546, 'GNI7348', '123', 'GNI3067', '', 0, 6145, 1, 3, 1000, 'third user', '1212313', '', '', 'U', 0, 0, 1710, 0, 0, 0, '2020-06-06 01:33:30', '2020-06-06 06:57:47'),
(15547, 'GNI3512', '123', 'GNI7348', '', 1, 3435, 1, 3, 1000, 'fourth user', '1212313', '', '', 'U', 0, 0, 130, 0, 0, 0, '2020-06-09 18:21:44', '2020-06-06 06:58:06'),
(15548, 'GNI2916', '123123', 'GNI3512', '', 1, 3987, 1, 2, 500, 'fifth user', '232232', '', '', 'U', 0, 0, 170, 0, 0, 0, '2020-06-10 00:10:38', '2020-06-10 00:04:23'),
(15549, 'GNI7546', '123', 'GNI2857', '', 0, 7613, 0, 0, 0, 'kush', '21121212', '', '', 'U', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '2020-06-15 13:26:43'),
(15550, 'GNI6007', '123', 'GNI2916', '', 1, 9524, 1, 3, 1000, 'sixth user', '23232', '', '', 'U', 0, 0, 75, 0, 0, 0, '2020-06-17 06:49:36', '2020-06-17 06:48:26'),
(15551, 'GNI7827', '123', 'GNI6007', '', 0, 1372, 1, 2, 500, 'seventh user', '23232', '', '', 'U', 0, 0, 0, 0, 0, 0, '2020-06-17 06:50:04', '2020-06-17 06:48:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_details`
--

CREATE TABLE `tbl_user_details` (
  `id` int NOT NULL,
  `user_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `account_type` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bank_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bank_account_number` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `account_holder_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `aadhar` varchar(50) NOT NULL,
  `pan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ifsc_code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_proof` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_proof2` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_proof3` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_proof4` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kyc_status` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_details`
--

INSERT INTO `tbl_user_details` (`id`, `user_id`, `account_type`, `bank_name`, `bank_account_number`, `account_holder_name`, `aadhar`, `pan`, `ifsc_code`, `id_proof`, `id_proof2`, `id_proof3`, `id_proof4`, `kyc_status`, `created_at`) VALUES
(15536, 'GNI1005', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-05-31 11:15:02'),
(15537, 'GNI8212', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-05-31 11:15:27'),
(15538, 'GNI2857', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-05-31 11:15:39'),
(15539, 'GNI4830', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-05-31 11:16:45'),
(15540, 'GNI5575', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-05-31 11:17:40'),
(15541, 'GNI6670', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-05-31 11:18:54'),
(15542, 'GNI4133', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-05-31 11:19:18'),
(15543, 'first_user', '', 'Punjab National Bank', '0337000101245123', 'Kuldeep Kumar', '123456789012', '3543', 'HDFC0001344', 'proof1591029288.png', 'id_proof1591115437.png', 'id_proof1591115444.png', '', 2, '2020-06-01 21:29:45'),
(15544, 'GNI3067', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-06-03 20:21:07'),
(15545, 'GNI7348', '', '', '', '', '', '', '', '', 'id_proof1593916398.png', '', '', 0, '2020-06-06 06:57:47'),
(15546, 'GNI3512', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-06-06 06:58:06'),
(15547, 'GNI2916', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-06-10 00:04:23'),
(15548, 'GNI7546', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-06-15 13:26:43'),
(15549, 'GNI6007', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-06-17 06:48:26'),
(15550, 'GNI7827', '', '', '', '', '', '', '', '', '', '', '', 0, '2020-06-17 06:48:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wallet`
--

CREATE TABLE `tbl_wallet` (
  `id` int NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `sender_id` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wallet`
--

INSERT INTO `tbl_wallet` (`id`, `user_id`, `amount`, `sender_id`, `type`, `remark`, `created_at`) VALUES
(18551, 'first_user', '20', 'admin', 'admin_amount', 'Fund Sent By Admin', '2020-06-09 22:37:06'),
(18552, 'first_user', '30', 'admin', 'admin_fund', 'approved', '2020-06-09 22:39:48'),
(18553, 'first_user', '2000', 'admin', 'admin_amount', 'Fund Sent By Admin', '2020-06-09 23:51:05'),
(18554, 'first_user', '-1000', '', 'account_activation', 'Fund Used For GNI3512 Account Activation ', '2020-06-09 23:51:44'),
(18555, 'first_user', '-500', '', 'account_activation', 'Fund Used For GNI4133 Account Activation ', '2020-06-10 00:11:27'),
(18556, 'first_user', '17', 'first_user', 'income_transfer', 'Fund generated from income', '2020-06-12 08:47:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_withdraw`
--

CREATE TABLE `tbl_withdraw` (
  `id` int NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `tds` float NOT NULL,
  `admin_charges` float NOT NULL,
  `fund_conversion` float NOT NULL,
  `payable_amount` float NOT NULL,
  `type` varchar(30) NOT NULL,
  `status` int NOT NULL,
  `remark` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_withdraw`
--

INSERT INTO `tbl_withdraw` (`id`, `user_id`, `amount`, `tds`, `admin_charges`, `fund_conversion`, `payable_amount`, `type`, `status`, `remark`, `created_at`) VALUES
(2895, 'first_user', 200, 10, 20, 0, 170, 'withdraw', 2, 'sfd fsd', '2020-06-12 07:29:57'),
(2896, 'first_user', 20, 1, 2, 0, 17, 'fund_generation', 1, '', '2020-06-12 08:47:47'),
(2897, 'first_user', 200, 10, 20, 0, 170, 'withdraw', 1, 'approved', '2020-06-12 21:21:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_withdraw_control`
--

CREATE TABLE `tbl_withdraw_control` (
  `id` int NOT NULL,
  `type` varchar(50) NOT NULL,
  `caption` varchar(100) NOT NULL,
  `start_day` int NOT NULL,
  `end_day` int NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tbl_withdraw_control`
--

INSERT INTO `tbl_withdraw_control` (`id`, `type`, `caption`, `start_day`, `end_day`, `start_time`, `end_time`, `created_at`) VALUES
(1, 'withdraw', 'Withdraw timing on 09.00 Am to 7.00 PM on Weekdays', 1, 7, '07:23:00', '09:59:59', '2020-06-12 21:45:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `ci_session`
--
ALTER TABLE `ci_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_address`
--
ALTER TABLE `tbl_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cron`
--
ALTER TABLE `tbl_cron`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_epins`
--
ALTER TABLE `tbl_epins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_income_wallet`
--
ALTER TABLE `tbl_income_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_package`
--
ALTER TABLE `tbl_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment_request`
--
ALTER TABLE `tbl_payment_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_point_matching_income`
--
ALTER TABLE `tbl_point_matching_income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pool`
--
ALTER TABLE `tbl_pool`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_popup`
--
ALTER TABLE `tbl_popup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roi`
--
ALTER TABLE `tbl_roi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider_images`
--
ALTER TABLE `tbl_slider_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sms_counter`
--
ALTER TABLE `tbl_sms_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sponser_count`
--
ALTER TABLE `tbl_sponser_count`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_support_message`
--
ALTER TABLE `tbl_support_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_details`
--
ALTER TABLE `tbl_user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_wallet`
--
ALTER TABLE `tbl_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_withdraw`
--
ALTER TABLE `tbl_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_withdraw_control`
--
ALTER TABLE `tbl_withdraw_control`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_address`
--
ALTER TABLE `tbl_address`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_cron`
--
ALTER TABLE `tbl_cron`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `tbl_epins`
--
ALTER TABLE `tbl_epins`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `tbl_income_wallet`
--
ALTER TABLE `tbl_income_wallet`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184094;

--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_package`
--
ALTER TABLE `tbl_package`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_payment_request`
--
ALTER TABLE `tbl_payment_request`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tbl_point_matching_income`
--
ALTER TABLE `tbl_point_matching_income`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pool`
--
ALTER TABLE `tbl_pool`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_popup`
--
ALTER TABLE `tbl_popup`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_roi`
--
ALTER TABLE `tbl_roi`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11215;

--
-- AUTO_INCREMENT for table `tbl_slider_images`
--
ALTER TABLE `tbl_slider_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_sms_counter`
--
ALTER TABLE `tbl_sms_counter`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14087;

--
-- AUTO_INCREMENT for table `tbl_sponser_count`
--
ALTER TABLE `tbl_sponser_count`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150164;

--
-- AUTO_INCREMENT for table `tbl_support_message`
--
ALTER TABLE `tbl_support_message`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15552;

--
-- AUTO_INCREMENT for table `tbl_user_details`
--
ALTER TABLE `tbl_user_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15551;

--
-- AUTO_INCREMENT for table `tbl_wallet`
--
ALTER TABLE `tbl_wallet`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18557;

--
-- AUTO_INCREMENT for table `tbl_withdraw`
--
ALTER TABLE `tbl_withdraw`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2898;

--
-- AUTO_INCREMENT for table `tbl_withdraw_control`
--
ALTER TABLE `tbl_withdraw_control`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
