<?php

class Site_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_single_record($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->row_array();
        return $res;
    }

    public function get_single_record_desc($table, $where, $select) {
        $this->db->select($select);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get_where($table, $where);
        $res = $query->row_array();
        return $res;
    }

    public function get_records($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->result_array();
        return $res;
    }
    public function get_limit_records($table, $where, $select , $limit , $offset) {
        $this->db->select($select);
        $this->db->where($where);
        $this->db->limit($limit , $offset);
        $query = $this->db->get($table);
        $res = $query->result_array();
        return $res;
    }
    public function add($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update($table, $where, $data) {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }
}