<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'security', 'email'));
        $this->load->model(array('site_model'));
        $this->load->helper(array('security'));
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	/**To Register New User As A free Account */
	public function register(){
        $response = array();
        $sponser_id = $this->input->get('sponser_id');
        if($sponser_id == ''){
            $sponser_id = '';
        }
        $response['sponser_id'] = $sponser_id;
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('sponser_id', 'Sponser ID', 'trim|required|xss_clean');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|xss_clean');
            $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
            if ($this->form_validation->run() == TRUE) {
				$data = $this->security->xss_clean($this->input->post());
				$sponser_id = $data['sponser_id'];
                $response['sponser_id'] = $sponser_id;
                $sponser = $this->site_model->get_single_record('tbl_users', array('user_id' => $sponser_id), 'user_id');
                if(!empty($sponser)){
                    $user_id = $this->getUserIdForRegister();
                    $userData['user_id'] =  $user_id;
                    $userData['sponser_id'] = $sponser_id;
                    $userData['name'] = $data['name'];
                    $userData['phone'] = $data['phone'];
                    $userData['password'] = $data['password'];
                    $userData['master_key'] = rand(1000,9999);
                    $userData['role'] = 'U';
                    $res = $this->site_model->add('tbl_users', $userData);
                    $res = $this->site_model->add('tbl_user_details',array('user_id' => $userData['user_id']));
                    if ($res) {
                        $this->add_counts($userData['user_id'],$userData['user_id'], $level = 1);
                        $sms_text = 'Dear ' .$userData['name']. ', Your Account Successfully created. User ID :  ' . $userData['user_id'] . ' Password :' . $userData['password'] . ' Transaction Password:' .$userData['master_key'] . ' '.base_url();
                        // notify_user($userData['user_id'] , $sms_text);
                        $response['message'] = 'Dear ' .$userData['name']. ', <br> Your Account Successfully created. <br>User ID :  ' . $userData['user_id'] . ' <br> Password :' . $userData['password'] . ' <br> Transaction Password:' .$userData['master_key'];
						$this->load->view('success', $response);
                    }
                    else {
						$this->session->set_flashdata('error', 'Error while Registraion please try Again');
						$this->load->view('register',$response);
                    }
                }else{
					$this->session->set_flashdata('error', 'Invalid Sponser ID');
					$this->load->view('register',$response);
                }
            }else{
				$this->session->set_flashdata('error', validation_errors());
				$this->load->view('register',$response);
            } 
        }else{
			$this->load->view('register',$response);
		}
	}

	public function success($meesage){
		$this->load->view('success', $response);
	}
    public function getUserIdForRegister() {
        $user_id = 'MLM' . rand(1000, 9999);
        $sponser = $this->site_model->get_single_record('tbl_users', array('user_id' => $user_id), 'user_id,name');
        if (!empty($sponser)) {
            $this->getUserIdForRegister();
        } else {
            return $user_id;
        }
    }

	public function check_sponser($user_id){
		$response = array();
        $response['success'] = 0;
        $user = $this->site_model->get_single_record('tbl_users', array('user_id' => $user_id), 'id,user_id,sponser_id,name');
        if(!empty($user)){
			$response['success'] = 1;
			$response['user'] = $user;
        }else{
			$response['message'] = 'Sponser Not Found';
		}
		echo json_encode($response);
	}

    function add_counts($user_name , $downline_id , $level) {
        $user = $this->site_model->get_single_record('tbl_users', array('user_id' => $user_name), $select = 'sponser_id,user_id');
        if ($user['sponser_id'] != '') {
                $downlineArray = array(
                    'user_id' => $user['sponser_id'],
                    'downline_id' => $downline_id,
                    // 'created_at' => 'CURRENT_TIMESTAMP',
                    'level' => $level,
                );
                $this->site_model->add('tbl_sponser_count', $downlineArray);
                $user_name = $user['sponser_id'];
                $this->add_counts($user_name, $downline_id, $level + 1);
        }
    }
	public function login(){
		$response = array();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->form_validation->set_rules('user_id', 'UserID', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
            if ($this->form_validation->run() == TRUE) {
				$data = $this->security->xss_clean($this->input->post());
				$user = $this->site_model->get_single_record('tbl_users', array('user_id' => $data['user_id'], 'password' => $data['password']), 'id,user_id,role,name,email,paid_status,disabled');
				if (!empty($user)) {
					if ($user['disabled'] == 0) {
						$this->session->set_userdata('user_id', $user['user_id']);
						$this->session->set_userdata('role', $user['role']);
						redirect('Dashboard/');
					} else {
						$this->session->set_flashdata('error', 'This Account Is Blocked Please Contact to Administrator');
					}
				} else {
					$this->session->set_flashdata('error', 'Invalid Credentials');
				}
			}
        }
		$this->load->view('login', $response);
	}
}
