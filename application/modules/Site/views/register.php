<?php
$this->load->view('header');
?>
<body>

    <div class="container">
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-4">
                    <div class="">
                        <div class="">
                            <img src="<?php echo logo;?>" style="width:100%"/>
                        </div>
                        <div class="details password-form">
                            <fieldset>
                                <span class="text-danger">
                                    <?php echo $this->session->flashdata('error'); ?>
                                </span>
                                <?php echo form_open('Site//register?sponser_id=' . $sponser_id, array('id' => 'RegisterForm')); ?>
                                <div class="form-group">
                                    <label for="Sponser ID">Sponser ID:</label>
                                    <input type="text" class="form-control" id="sponser_id" placeholder="Enter Sponser ID" value="<?php echo $sponser_id; ?>" name="sponser_id" required>
                                    <span class="text-danger" id="errorMessage"> <?php echo form_error('sponser_id'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Name:</label>
                                    <input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo set_value('name'); ?>" required>
                                    <span class="text-danger"> <?php echo form_error('name'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Phone:</label>
                                    <input type="phone" class="form-control" placeholder="Enter Phone" name="phone" value="<?php echo set_value('phone'); ?>" required>
                                    <span class="text-danger"> <?php echo form_error('phone'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="text" class="form-control" placeholder="Enter Passowrd" name="password" value="<?php echo set_value('password'); ?>" required>
                                    <span class="text-danger"> <?php echo form_error('password'); ?></span>
                                </div>
                                <div class="Accept">
                                    <span>
                                        <input id="chTerms" name="chTerms" type="checkbox" required="required">
                                    </span>&nbsp;
                                    I have read the   <a style="cursor:pointer;color:red; font-size:16px" target="_blank" href="<?php echo base_url('Site/Main/content/terms');?>" target="_blank">Terms &amp; Conditions</a>,
                                     I am well aware fully of the risks. Being in sound mind, I have decided to become a member of <?php echo title;?> Company.

                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block">Submit</button>
                                </div>

                                <?php echo form_close(); ?>
                                <div class="form-group">
                                    <span>Have Account ? <a href="<?php echo base_url('Site/login');?>">Login Now</a></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
<script>
    $(document).on('blur', '#sponser_id', function () {
        check_sponser();
    })
    function check_sponser() {
        var user_id = $('#sponser_id').val();
        if (user_id != '') {
            var url = '<?php echo base_url("Site/check_sponser/") ?>' + user_id;
            $.get(url, function (res) {
                if(res.success == 1){
                    $('#errorMessage').html(res.user.name);
                }else{
                    $('#errorMessage').html(res.message);
                }
            },'json')
        }
    }
    check_sponser();
    $(document).on('submit', '#RegisterForm', function () {
        if (confirm('Please Check All The Fields Before Submit')) {
            yourformelement.submit();
        } else {
            return false;
        }
    })
</script>
</html>