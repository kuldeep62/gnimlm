<?php
$this->load->view('header');
?>
<body class="" style="">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-4 rounded-lg card-authentication1 mx-auto my-5" style="">
                <div class="text-center">
                    <img src="<?php echo logo;?>" class="img-responsive" style="width:100%" />
                </div>
                <div class="">
                    <div class="">
                        <p style="color:red;text-align: center;"><?php echo $this->session->flashdata('error');; ?></p>
                        <fieldset>
                            <?php echo form_open(); ?>
                            <div class="form-group">
                                <label>Username or Email:</label>
                                <input type="text" name="user_id" value="<?php echo set_value('user_id')?>" class="form-control" placeholder="User ID  or Email Address" required="true" />
                                <span class="text-danger"> <?php echo form_error('user_id'); ?></span>
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input type="password" name="password" value="" class="form-control" placeholder="Enter Your Password" required="true" />
                                <span class="text-danger"> <?php echo form_error('password'); ?></span>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary" name="Submit" value="Login">Sign in
                                </button>
                            </div>
                            <div class="form-group">
                                <span>Don't have an account? <a href="<?php echo base_url('Site/register');?>"> Click here</a> for
                                    Signup</span>
                            </div>
                            <!-- <div class="form-group">
                                <a href="" class="text-danger">Forgot Password</a>
                            </div> -->
                            <?php echo form_close(); ?>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>