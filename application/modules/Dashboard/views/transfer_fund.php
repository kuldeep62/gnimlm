<?php $this->load->view('header');?>

    <main class="page-content">
        <div class="container-fluid">
            <h2>Fund Transfer (<?php echo currency . $wallet_amount['wallet_amount']?>)</h2>
            <hr>
            <div class="row">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Fund/requests')?>">Fund Management</a></li>
                    <li class="breadcrumb-item active">Transfer Fund</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo form_open(base_url('Dashboard/Fund/transfer_fund'), array('method' => 'post')); ?>
                        <div class="row">
                            <span class="text-center text-danger">
                                <h3><?php echo $this->session->flashdata('message');?></h3>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <?php
                                    echo form_input(array('type' => 'number', 'name' => 'amount', 'class' => 'form-control'));
                                    ?>
                                    <span class="text-danger"><?php echo form_error('amount');?></span>
                                </div>
                                <div class="form-group">
                                    <label>User ID:</label>
                                    <?php
                                    echo form_input(array('type' => 'text', 'name' => 'user_id', 'class' => 'form-control' , 'id' => 'user_id'));
                                    ?>
                                    <span class="text-danger" id="userName"><?php echo form_error('user_id');?></span>
                                </div>
                                <div class="form-group">
                                    <label>Remark:</label>
                                    <?php
                                    echo form_input(array('type' => 'text', 'name' => 'remark', 'class' => 'form-control'));
                                    ?>
                                    <?php echo form_error('remark');?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_input(array('type' => 'submit' , 'class' => 'btn btn-success pull-right','name' => 'fundbtn','value' => 'Transfer'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </main>
</div>
<?php $this->load->view('footer');?>
<script>
$(document).on('blur','#user_id',function(){
    var url = '<?php echo base_url("Dashboard/Fund/check_sponser/")?>'+$(this).val();
    $.get(url,function(res){
        if(res.success == 1){
            $('#userName').text(res.user.name);
        }else{
            $('#userName').text(res.message);
        }
    },'json')
})    
</script>