<?php $this->load->view('header');?>

    <main class="page-content">
        <div class="container-fluid">
            <h2><?php echo $header;?> (<?php echo currency . $user['income']?>)</h2>
            <hr>
            <div class="row">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                    <li class="breadcrumb-item active"><?php echo $header;?></li>
                </ul>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <?php echo form_open(); ?>
                        <div class="row">
                            <span class="text-center text-danger">
                                <h3><?php echo $this->session->flashdata('message');?></h3>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <?php
                                    echo form_input(array('type' => 'number', 'name' => 'amount', 'class' => 'form-control'));
                                    ?>
                                    <span class="text-danger"><?php echo form_error('amount');?></span>
                                </div>
                                <div class="form-group">
                                    <label>Txn. Pin:</label>
                                    <?php
                                    echo form_input(array('type' => 'password', 'name' => 'master_key', 'class' => 'form-control'));
                                    ?>
                                     <span class="text-danger"><?php echo form_error('master_key');?></span>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_input(array('type' => 'submit' , 'class' => 'btn btn-success pull-right','name' => 'fundbtn','value' => 'Transfer'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </main>
</div>
<?php $this->load->view('footer');?>