<?php $this->load->view('header');?>
<style>
.flip-box {
  background-color: transparent;
  /* width: 300px;
  height: 200px; */
  border: 1px solid #f1f1f1;
  perspective: 1000px;
}

.flip-box-inner {
  position: relative;
  /* width: 100%;
  height: 100%; */
  text-align: center;
  transition: transform 0.8s;
  transform-style: preserve-3d;
}

.flip-box:hover .flip-box-inner {
  transform: rotateY(180deg);
}

.flip-box-front, .flip-box-back {
  position: absolute;
  width: 100%;
  height: 100%;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
}

.flip-box-front {
  /* background-color: #bbb;
  color: black; */
}

.flip-box-back {
  /* background-color: #555;
  color: white; */
  transform: rotateY(180deg);
}
</style>
    <main class="page-content">
        <div class="container">
            <h2>ID Card</h2>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">               
                    <div class="flip-box">
                        <div class="flip-box-inner">
                            <div class="flip-box-front" style="width:300px;height:600px">
                                <img src="<?php echo logo;?>" alt="Paris" >
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Special title treatment</h5>
                                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="flip-box-back">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Special title treatment</h5>
                                        <p class="card-text">With supporting <br>text below as <br>a natural <br>lead-in <br>to <br>additional <br>content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </main>
    <!-- page-content" -->
</div>
<?php $this->load->view('footer');?>