<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> </h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Payout/Index')?>">Payout</a></li>
                        <li class="breadcrumb-item active"><?php echo $header;?></li>
                    </ul>
                </div>
                <div class="row">
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Amount</th>
                                <th>Tds</th>
                                <th>Admin Charges</th>
                                <th>Payable Amount</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo currency . ' ' .$record['amount']; ?></td>
                                    <td><?php echo currency . ' ' .$record['tds']; ?></td>
                                    <td><?php echo currency . ' ' .$record['admin_charges']; ?></td>
                                    <td><?php echo currency . ' ' .$record['payable_amount']; ?></td>
                                    <td><?php echo $record['type']; ?></td>
                                    <td><?php 
                                    if($record['status'] == 0)
                                        echo'<span class="text-info">Pending</span>';
                                    elseif($record['status'] == 1)
                                        echo'<span class="text-success">Approved</span>';
                                    elseif($record['status'] == 2)
                                        echo'<span class="text-success">Rejected</span>';
                                    ?></td>
                                    <td><?php echo $record['created_at']; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                    echo $this->pagination->create_links();
                    ?>
                </div>
            </div>

        </main>
        <!-- page-content" -->
    </div>
    <?php $this->load->view('footer');?>