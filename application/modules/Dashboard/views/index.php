<?php $this->load->view('header');
$userinfo = userinfo();
?>
<!---Chart Assets-->
<link rel="stylesheet" type="text/css" href="https://www.chartjs.org/samples/latest/style.css">
<script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.4.0/dist/chartjs-plugin-datalabels.min.js"></script>
<!---Chart Assets-->
<style> 
.news {
  /* background-color: red; */
  position: relative;
  animation-name: example;
  animation-duration: 20s;
  animation-iteration-count: infinite;
}

@keyframes example {
  0%   {right:-90%;}
  25%  {right:0%;}
}
</style>
    <main class="page-content">
        <div class="container-fluid">
            <h2>Welcome User</h2>
            <hr>
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Refer !</h4>
                        <p>Click here Register New Member: <a href="<?php echo base_url('Site/register/?sponser_id='.$userinfo->user_id);?>" target="_blank"> New Member</a></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    
                        <?php
                        foreach($news as $key => $n){
                            echo'<div class="alert alert-danger  alert-dismissible" role="alert">';
                            echo'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                            echo'<div class="news">';
                            echo'<h4 class="alert-heading">'.$n['title'].'</h4>';
                            echo'<p>'.$n['news'].'</p>';
                            echo'</div>';
                            echo'</div>';
                        }
                        ?>
                        
                    
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">Payout Reports</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <canvas id="chart-0"></canvas>
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-group">
                                        <li class="list-group-item bg-info">Payout</li>
                                        <?php
                                        $total_payout = 0;
                                        foreach($incomes as $key => $income){
                                            $total_payout = $income + $total_payout;
                                            echo'<li class="list-group-item">'.get_income_name($key).' <a href="'.base_url('Dashboard/Payout/Income/'.$key).'" class="btn btn-primary btn-sm"  style="float:right">'.currency.$income.'</a></li>';
                                        }
                                        ?>
                                        <li class="list-group-item">Total Payout<a href="<?php echo base_url('Dashboard/Payout/');?>"  class="btn btn-primary btn-sm" style="float:right"><?php echo currency.$total_payout ;?></a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <ul class="list-group">
                        <li class="list-group-item active">User Details</li>
                        <li class="list-group-item">User Id : <?php echo $user['user_id']?></li>
                        <li class="list-group-item">User Name : <?php echo $user['name']?></li>
                        <li class="list-group-item">Sponser ID : <?php echo $user['sponser_id']?></li>
                        <li class="list-group-item">Phone : <?php echo $user['phone']?></li>
                        <li class="list-group-item">Email : <?php echo $user['email']?></li>
                        <li class="list-group-item">Package : <?php echo $user['package_amount']?></li>
                        <li class="list-group-item">
                            <?php
                            if($user['paid_status'] == 0)
                                echo'Registration Date : ' . $user['created_at'] ;
                            else
                                echo'Activation Date : ' . $user['topup_date'] ;
                            ?>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-group">
                        <li class="list-group-item active">Network</li>
                        <li class="list-group-item">Paid Directs : <?php echo $user['directs']?></li>
                        <li class="list-group-item">Total Team : <?php echo $paid_team['paid_team'] + $free_team['free_team'];?></li>
                        <li class="list-group-item">Paid Team : <?php echo $paid_team['paid_team'];?></li>
                        <li class="list-group-item">Free Team : <?php echo $free_team['free_team'];?></li>
                        <?php
                        if(!empty($pool)){
                            echo'<li class="list-group-item active">Pool</li>';
                            echo'<li class="list-group-item">Level 1 : '.$pool['level1'].' Members</li>';
                            echo'<li class="list-group-item">Level 2 : '.$pool['level2'].' Members</li>';
                            echo'<li class="list-group-item">Level 31 : '.$pool['level3'].' Members</li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-group">
                        <li class="list-group-item active">Balance </li>
                        <li class="list-group-item">Withdrawal balance: <?php echo currency .$user['income']?></li>
                        <li class="list-group-item">Total Fund : <?php echo currency .$user['wallet_amount'];?></li>
                        <li class="list-group-item active">Available Pins</li>
                        <?php 
                        foreach($available_pins as $key => $pin)
                            echo'<li class="list-group-item">'.currency .$pin['amount'].' : '.$pin['pin_count'].'pins </li>';
                        ?>
                        
                    </ul>
                </div>
            </div>
        </div>
    </main>
</div>

<!---Popup News---->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Alert</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
            <?php
            if($popup['type'] == 'video')
                echo '<iframe width="100%" height="400px" src="https://www.youtube.com/embed/'.$popup['media'].'"></iframe>';
            else
                echo '<img src="'.base_url('uploads/'.$popup['media']).'">';
            ?>
      </div>


    </div>
  </div>
</div>
<!---Popup News---->
<?php $this->load->view('footer');?>
<script>
$('#myModal').modal('show');
     var data = [{
            data: <?php echo json_encode(array_values($incomes))?>,
            names:<?php echo json_encode($income_names)?>,
            backgroundColor: [
                "#aaaaaa",
                "#333538",
                "#d21243",
                "#B27200",
                "#4b77a9",
                "#5f255f",
                "#B27200",
                "#4b77a9",
                "#5f255f",
            ],
            borderColor: "#fff"
        }];
        
        var options = {
            showAllTooltips: true,
            tooltips: {
                mode: 'label',
                    callbacks: {
                    title: function(key, data) {
                        return data.datasets[0].names[key[0].index];
                    },
                    label: function(key, data) { 
                        return data.datasets[0].data[key.index];
                    },
                },
                enabled: true,
            },
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {         
                  return '<?php echo currency; ?>'+value  ;              
                },
                color: '#fff',
            }
        }
    };
    
    
    var ctx = document.getElementById("chart-0").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: data,
        },
        options: options
    });
        
</script>