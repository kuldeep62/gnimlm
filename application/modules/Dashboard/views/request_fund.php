<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2>Request Fund</h2>
                <hr>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Profile/')?>">Profile</a></li>
                        <li class="breadcrumb-item active">General Settings</li>
                    </ul>
                </div>
                <?php echo form_open_multipart();?>
                <div class="row">
                    <div class="col-md-6">
                        <h2><?php echo $this->session->flashdata('message');?></h2>
                        <div class="form-group">
                            <label>Payment Method</label>
                            <?php
                            echo form_dropdown('payment_method',array('bank' => 'Bank', 'phonepay' => 'Phone Pay', 'googlepay' => 'Google Pay' , 'paytm' => 'Paytm', 'upi' => 'UPI', 'cash' => 'Cash Deposit'),'',array('class'=>'form-control'));
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Amount</label>
                            <?php
                            echo form_input(array('type' => 'number', 'name' => 'amount', 'class' => 'form-control'));
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Upload Proof Here</label>
                            <?php
                            echo form_input(array('type' => 'file', 'name' => 'userfile', 'class' => 'form-control' , 'id' => 'payment_slip','size' => 20));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo form_input(array('type' => 'submit' , 'class' => 'btn btn-success pull-right','name' => 'fundbtn','value' => 'Request'));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="<?php echo base_url('classic/no_image.png');?>" title="Payment Slip" id="slipImage" style="width: 100%;">
                    </div>
                </div>
            <?php echo form_close();?>
            </div>

        </main>
        <!-- page-content" -->
    </div>
    <?php $this->load->view('footer');?>
    <script>
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#slipImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#payment_slip").change(function () {
        readURL(this);
    });
</script>