<?php $this->load->view('header');?>

        <main class="page-content">
            <div class="container-fluid">

                <h2>Kyc Manager</h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Profile/')?>">Profile</a></li>
                        <li class="breadcrumb-item active">Kyc Manager</li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="text-danger"><?php echo $this->session->flashdata('message');?></h2>
                        <?php echo form_open_multipart();?>
                            <div class="form-group">
                                <label>Pan Number</label>
                                <?php echo form_input(['type' => 'text' , 'name' =>'pan','class'=>'form-control','value'=> $bank_details['pan'],'placeholder' =>'Pan Number']);?>
                                <span class="text-danger"><?php echo form_error('pan');?></span>
                            </div>
                            <div class="form-group">
                                <label>Aadhar Card</label>
                                <?php echo form_input(['type' => 'text' , 'name' =>'aadhar','class'=>'form-control','value'=> $bank_details['aadhar'],'placeholder' =>'Aadhar Card']);?>
                                <span class="text-danger"><?php echo form_error('aadhar');?></span>
                            </div>
                            <div class="form-group">
                                <label>ID Proof</label>
                                <?php echo form_input(['type' => 'file' , 'name' =>'userfile','class'=>'form-control','value'=> $bank_details['id_proof']]);?>
                                <span class="text-danger"><?php echo form_error('id_proof');?></span>
                            </div>
                            <div class="form-group">
                                <?php 
                                if($bank_details['kyc_status'] == 0 || $bank_details['kyc_status'] == 2)
                                echo form_input(['type' => 'submit' , 'name' =>'save	','class'=>'btn btn-success','value' => 'Save']);
                                ?>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>