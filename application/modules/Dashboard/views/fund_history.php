<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> </h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Fund/fund_history')?>">Fund</a></li>
                        <li class="breadcrumb-item active"><?php echo $header;?></li>
                    </ul>
                </div>
                <hr>
                <hr>
                <div class="row">
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User ID</th>
                                <th>Amount</th>
                                <th>Sender ID</th>
                                <th>Type</th>
                                <th>Remark</th>
                                <th>CreatedAt</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                    <td><?php echo ($key + 1) ?></td>
                                    <td><?php echo $record['user_id']; ?></td>
                                    <td>Rs.<?php echo $record['amount']; ?></td>
                                    <td><?php echo $record['sender_id']; ?></td>
                                    <td><?php echo ucwords(str_replace('_',' ',$record['type'])); ?></td>
                                    <td><?php echo $record['remark']; ?></td>
                                    <td><?php echo $record['created_at']; ?></td>
                                </tr>
                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
                <div class="row">
                </div>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>