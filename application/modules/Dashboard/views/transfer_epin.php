<?php $this->load->view('header');?>

    <main class="page-content">
        <div class="container-fluid">
            <h2>Epin Transfer </h2><br>
            <?php
            foreach($available_epins as $key => $a_pin){
                echo' Pin Amount : '.$a_pin['amount'] . ' Available ' . $a_pin['pin_count'] . '<br>';
            }
            
            ?>
            <hr>
            <div class="row">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Epins')?>">Pins Management</a></li>
                    <li class="breadcrumb-item active">Transfer EPin</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo form_open('', array('method' => 'post')); ?>
                        <div class="row">
                            <span class="text-center text-danger">
                                <h3><?php echo $this->session->flashdata('message');?></h3>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>EPins</label>
                                    <input type="number" class="form-control" name="epins" placeholder="Epins" value="<?php echo set_value('epins'); ?>"/>
                                    <span class="text-danger"><?php echo form_error('epins') ?></span>
                                </div>
                                <div class="form-group">
                                    <label>User ID</label>
                                    <input type="text" class="form-control" name="user_id" id="user_id" placeholder="User ID" value=""/>
                                    <span class="text-danger" id="errorMessage"><?php echo form_error('user_id') ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Pin Amount</label>
                                    <select class="form-control" name="pin_amount">
                                        <?php
                                        foreach($available_epins as $key => $pin)
                                            echo'<option value="'.$pin['amount'].'">'.$pin['amount'].'</option>';
                                        ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('pin_amount') ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Master Key</label>
                                    <input type="text" class="form-control" name="master_key" placeholder="Master Key" value=""/>
                                    <span class="text-danger"><?php echo form_error('master_key') ?></span>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_input(array('type' => 'submit' , 'class' => 'btn btn-success pull-right','name' => 'fundbtn','value' => 'Transfer'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </main>
</div>
<?php $this->load->view('footer');?>
<script>
$(document).on('blur','#user_id',function(){
    var url = '<?php echo base_url("Dashboard/Fund/check_sponser/")?>'+$(this).val();
    $.get(url,function(res){
        if(res.success == 1){
            $('#userName').text(res.user.name);
        }else{
            $('#userName').text(res.message);
        }
    },'json')
})    
</script>