<?php $this->load->view('header');?>

        <main class="page-content">
            <div class="container-fluid">

                <h2>Profile Management</h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Profile/')?>">Profile</a></li>
                        <li class="breadcrumb-item active">Password Reset</li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <?php echo form_open();
                    ?> <h2 class="text-danger"><?php echo $this->session->flashdata('message');?></h2>
                        <div class="form-group">
                            <label>Current Password</label>
                            <?php echo form_input(['type' => 'password','name'=>'cpassword','class' => 'form-control']);?>
                            <span class="text-danger"><?php echo form_error('cpassword');?></span>
                        </div>
                        <div class="form-group">
                            <label>New Password</label>
                            <?php echo form_input(['type' => 'password','name'=>'npassword','class' => 'form-control']);?>
                            <span class="text-danger"><?php echo form_error('npassword');?></span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <?php echo form_input(['type' => 'password','name'=>'vpassword','class' => 'form-control']);?>
                            <span class="text-danger"><?php echo form_error('vpassword');?></span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success">Update</button>
                        </div>
                    <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>