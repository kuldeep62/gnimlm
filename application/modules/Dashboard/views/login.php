<?php
$this->load->view('header');
?>
<body>

    <div class="container">
    </div>
    <div class="text-center">
        <img src="https://gnimlm.com/wp-content/uploads/2019/03/LOGO-FINAL.jpg" class="img-responsive" />
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-4">
                <form ethod="post" accept-charset="utf-8">
                    <div class="">
                        <div class="details password-form">
                            <fieldset>
                                <div class="form-group">
                                    <label>Username or Email:</label>
                                    <input type="text" name="user_id" value="" class="form-control"
                                        placeholder="User ID  or Email Address" required="true" />
                                </div>
                                <div class="form-group">
                                    <label>Password:</label>
                                    <input type="password" name="password" value="" class="form-control"
                                        placeholder="Enter Your Password" required="true" />
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary" name="Submit" value="Login">Sign in
                                    </button>
                                </div>
                                <div class="form-group">
                                    <span>Don't have an account? <a href="register.html"> Click here</a> for
                                        Signup</span>
                                </div>
                                <div class="form-group">
                                    <a href="" class="text-danger">Forgot Password</a>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>