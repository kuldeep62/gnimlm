<?php $this->load->view('header');?>

    <main class="page-content">
        <div class="container-fluid">
            <h2><?php echo $header;?> (<?php echo currency . $user['income']?>)</h2>
            <hr>
            <div class="row">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                    <li class="breadcrumb-item active"><?php echo $header;?></li>
                </ul>
            </div>
            <hr>
            <div class="row">
                <h2 class="text-info"><?php echo $withdraw_control['caption'];?></h2>
                <div class="col-md-6">
                    <?php 
                    
                    $today = date('N');
                    $time = date('H:i:s');
                    if($today >= $withdraw_control['start_day'] && $today <= $withdraw_control['end_day'] && $time >= $withdraw_control['start_time'] && $time <= $withdraw_control['end_time']){
                     
                        echo form_open(); ?>
                            <div class="row">
                                <span class="text-center text-danger">
                                    <h3><?php echo $this->session->flashdata('message');?></h3>
                                </span>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <?php
                                        echo form_input(array('type' => 'number', 'name' => 'amount', 'class' => 'form-control'));
                                        ?>
                                        <span class="text-danger"><?php echo form_error('amount');?></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Txn. Pin:</label>
                                        <?php
                                        echo form_input(array('type' => 'password', 'name' => 'master_key', 'class' => 'form-control'));
                                        ?>
                                        <span class="text-danger"><?php echo form_error('master_key');?></span>
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        echo form_input(array('type' => 'submit' , 'class' => 'btn btn-success pull-right','name' => 'fundbtn','value' => 'Transfer'));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();
                      
                    }
                    ?>
                </div>
                <div class="col-6">
                    <ul class="list-group">
                        <li class="list-group-item active">Bank Details</li>
                        <li class="list-group-item">Bank Name : <?php echo $bank['bank_name']?></li>
                        <li class="list-group-item">Bank Account Name : <?php echo $bank['account_holder_name']?></li>
                        <li class="list-group-item">Account Number: <?php echo $bank['bank_account_number']?></li>
                        <li class="list-group-item">IFSC Code : <?php echo $bank['ifsc_code']?></li>
                        <li class="list-group-item">PAN Number : <?php echo $bank['pan']?></li>
                        <li class="list-group-item">Aadhar Number : <?php echo $bank['aadhar']?></li>
                        <li class="list-group-item">KYC Status : 
                            <?php 
                            if($bank['kyc_status'] == 0)
                                echo'<span class="text-info">Pending</span>';
                            elseif($bank['kyc_status'] == 1)
                                echo'<span class="text-primary">Applied</span>';
                            elseif($bank['kyc_status'] == 2)
                                echo'<span class="text-success">Approved</span>';
                            elseif($bank['kyc_status'] == 3)
                                echo'<span class="text-danger">Rejected</span>';
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>
</div>
<?php $this->load->view('footer');?>