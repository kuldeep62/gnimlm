<?php $this->load->view('header');?>

    <main class="page-content">
        <div class="container-fluid">
            <h2>Generate Ticket</h2><br>
            <hr>
            <div class="row">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Ticket')?>">Support</a></li>
                    <li class="breadcrumb-item active">Generate Ticket</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php echo form_open('', array('method' => 'post')); ?>
                        <div class="row">
                            <span class="text-center text-danger">
                                <h3><?php echo $this->session->flashdata('message');?></h3>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Subject</label>
                                    <select class="form-control" name="subject">
                                        <?php
                                        foreach($subjects as $key => $subject)
                                            echo'<option value="'.$key.'">'.$subject.'</option>';
                                        ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('subject') ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control" name="message" placeholder="Message"><?php echo set_value('message');?></textarea>
                                    <span class="text-danger"><?php echo form_error('message') ?></span>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo form_input(array('type' => 'submit' , 'class' => 'btn btn-success pull-right','value' => 'Genrate'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </main>
</div>
<?php $this->load->view('footer');?>