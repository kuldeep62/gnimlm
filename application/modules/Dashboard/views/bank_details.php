<?php $this->load->view('header');?>

        <main class="page-content">
            <div class="container-fluid">

                <h2>Profile Management</h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Profile/')?>">Profile</a></li>
                        <li class="breadcrumb-item active">Bank details</li>
                    </ul>
                </div>
                <?php echo form_open();?>
                    <h2 class="text-danger"><?php echo $this->session->flashdata('message');?></h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Bank Name</label>
                            <?php echo form_input(['type' => 'text' , 'name' =>'bank_name','class'=>'form-control','value'=> $bank_details['bank_name'],'placeholder' =>'Bank Name']);?>
                            <span class="text-danger"><?php echo form_error('bank_name');?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Account Holder Name</label>
                            <?php echo form_input(['type' => 'text' , 'name' =>'account_holder_name','class'=>'form-control','value'=> $bank_details['account_holder_name'],'placeholder' =>'Bank Account Holder Name']);?>
                            <span class="text-danger"><?php echo form_error('account_holder_name');?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Account Number</label>
                            <?php echo form_input(['type' => 'text' , 'name' =>'bank_account_number','class'=>'form-control','value'=> $bank_details['bank_account_number'],'placeholder' =>'Bank Account Number']);?>
                            <span class="text-danger"><?php echo form_error('bank_account_number');?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>IFSC Code</label>
                            <?php echo form_input(['type' => 'text' , 'name' =>'ifsc_code','class'=>'form-control text-uppercase','value'=> $bank_details['ifsc_code'],'placeholder' =>'IFSC Code','pattern'=>'[A-Z|a-z]{4}[0][\d]{6}$']);?>
                            <span class="text-danger"><?php echo form_error('ifsc_code');?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pan Number</label>
                            <?php echo form_input(['type' => 'text' , 'name' =>'pan','class'=>'form-control','value'=> $bank_details['pan'],'placeholder' =>'Pan Number']);?>
                            <span class="text-danger"><?php echo form_error('pan');?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Aadhar Card</label>
                            <?php echo form_input(['type' => 'text' , 'name' =>'aadhar','class'=>'form-control','value'=> $bank_details['aadhar'],'placeholder' =>'Aadhar Card']);?>
                            <span class="text-danger"><?php echo form_error('aadhar');?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php 
                            if($bank_details['kyc_status'] != 2){
                                echo form_input(['type' => 'submit' , 'name' =>'save	','class'=>'btn btn-success','value' => 'Save']);                                
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php echo form_close();?>
                <div class="row">
                    <?php
                    if($bank_details['kyc_status'] != 2){
                        ?>
                        <div class="col-md-3">
                            <?php echo form_open_multipart(base_url('Dashboard/Profile/UploadProof/'),array('method' => 'post' , 'class' => 'proofForm'));?>
                                <div class="loader" style="display:none;">
                                </div>
                                <label>Aadhar Card Front</label><br>
                                <input type="file" name="userfile" class="" placeholder=""/><br>
                                <input type="hidden" name="proof_type" value="id_proof"/>
                                <img src="<?php echo base_url('uploads/' . ($bank_details['id_proof'] !=  '' ? $bank_details['id_proof'] : 'no-image.png' )); ?>" class="img-responsive" style="max-width:200px;"><br>
                                <input type="submit" class="btn btn-success" value="upload">
                            <?php echo form_close();?>
                        </div>
                        <div class="col-md-3">
                                <?php echo form_open_multipart(base_url('Dashboard/Profile/UploadProof/'),array('method' => 'post' , 'class' => 'proofForm'));?>
                                <div class="loader" style="display:none;">
                                </div>
                                <label>Aadhar Card Back</label><br>
                                <input type="file" name="userfile" class="" placeholder=""/><br>
                                <input type="hidden" name="proof_type" value="id_proof2"/>
                                <img src="<?php echo base_url('uploads/' . ($bank_details['id_proof2'] !=  '' ? $bank_details['id_proof2'] : 'no-image.png' )); ?>" class="img-responsive" style="max-width:200px;"><br>
                                <input type="submit" class="btn btn-success" value="upload">
                            <?php echo form_close();?>
                        </div>
                        <div class="col-md-3">
                            <?php echo form_open_multipart(base_url('Dashboard/Profile/UploadProof/'),array('method' => 'post'  , 'class' => 'proofForm'));?>
                                <div class="loader" style="display:none;">
                                </div>
                                <label>Pan Card</label><br>
                                <input type="file" name="userfile" class="" placeholder=""/><br>
                                <input type="hidden" name="proof_type" value="id_proof3"/>
                                <img src="<?php echo base_url('uploads/' . ($bank_details['id_proof3'] !=  '' ? $bank_details['id_proof3'] : 'no-image.png' )); ?>" class="img-responsive" style="max-width:200px;"><br>
                                <input type="submit" class="btn btn-success" value="upload">
                            <?php echo form_close();?>
                        </div>
                        <div class="col-md-3">
                            <?php echo form_open_multipart(base_url('Dashboard/Profile/UploadProof/'),array('method' => 'post', 'class' => 'proofForm'));?>
                                <div class="loader" style="display:none;">
                                </div>
                                <label>Bank Passbook/Cancel Check</label><br>
                                <input type="file" name="userfile" class="" placeholder=""/><br>
                                <input type="hidden" name="proof_type" value="id_proof4"/>
                                <img src="<?php echo base_url('uploads/' . ($bank_details['id_proof4'] !=  '' ? $bank_details['id_proof4'] : 'no-image.png' )); ?>" class="img-responsive" style="max-width:200px;"><br>
                                <input type="submit" class="btn btn-success" value="upload">
                            <?php echo form_close();?>
                        </div>
                        <?php
                    }else{
                        echo'<div class="col-md-3">
                            <label>Aadhar Card Front</label><br>
                            <img src="'.base_url('uploads/' . $bank_details['id_proof']).'" class="img-responsive" style="max-width:200px;">
                        </div>';
                        echo'<div class="col-md-3">
                            <label>Aadhar Card Back</label><br>
                            <img src="'.base_url('uploads/' . $bank_details['id_proof2']).'" class="img-responsive" style="max-width:200px;">
                        </div>';
                        echo'<div class="col-md-3">
                            <label>Pan Card</label><br>
                            <img src="'.base_url('uploads/' . $bank_details['id_proof3']).'" class="img-responsive" style="max-width:200px;">
                        </div>';
                        echo'<div class="col-md-3">
                            <label>Bank Passbook/Cancel Check</label><br>
                            <img src="'.base_url('uploads/' . $bank_details['id_proof4']).'" class="img-responsive" style="max-width:200px;">
                        </div>';
                    }
                    ?>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>
<script>
$("form.proofForm").submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    var t = $(this);
    t.find('.loader').css('display','block');
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        success: function (data) {
            var res = JSON.parse(data)
            alert(res.message);
            t.append('<input type="hidden" name="'+res.csrfName+'" value="'+res.csrfHash+'" style="display:none;">')
            t.find('.loader').css('display','none');
            if(res.success == 1)
                location.reload();

        },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>