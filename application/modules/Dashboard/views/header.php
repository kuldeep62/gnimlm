<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title><?php echo title;?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="<?php echo base_url('Assets/css/sidebar.css');?>" rel="stylesheet">
</head>
<body>
    <?php  $userinfo = userinfo(); ?>
    <div class="page-wrapper chiller-theme toggled">
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand bg-light">
                    <img src="<?php echo logo;?>" class="img-responsive" style="width:100%" />
                    <div id="close-sidebar">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded logo-img" src="<?php echo logo;?>" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Welcome
                            <strong><?php echo $userinfo->user_id;?></strong>
                        </span>
                        <!-- <span class="user-role">Rank</span> -->
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-menu">
                    <ul>

                        <li>
                            <a href="<?php echo base_url('Dashboard');?>">
                                <i class="fa fa-tachometer-alt"></i>
                                <span>Dashboard</span>
                                <!-- <span class="badge badge-pill badge-primary">Beta</span> -->
                            </a>
                        </li>

                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Network' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Network</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Network' ? 'block' : 'none';?>">
                                <ul>
                                    <li> <a href="<?php echo base_url('Dashboard/Network/Directs');?>">My Directs</a></li>
                                    <li> <a href="<?php echo base_url('Dashboard/Network/Team');?>">My Team</a></li>
                                    <li> <a href="<?php echo base_url('Dashboard/Network/Genelogy');?>">Genelogy View</a></li>
                                    <li> <a href="<?php echo base_url('Dashboard/Network/TreeView/'.$userinfo->user_id);?>">Tree View</a></li>
                                    <?php
                                    if($userinfo->paid_status == 1)
                                        echo'<li> <a href="'.base_url('Dashboard/Network/PoolView/'.$userinfo->user_id).'">Pool View</a></li>';
                                    ?>
                                </ul>
                            </div>
                        </li>
                        <li class="">
                            <a href="<?php echo base_url('Dashboard/AccountActivation/FundActivation');?>">
                                <i class="fa fa-users"></i>
                                <span>Account Activation</span>
                            </a>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Profile' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-dollar-sign"></i>
                                <span>Profile Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Profile' ? 'block' : 'none';?>">
                                <ul>
                                    <li><a href="<?php echo base_url('Dashboard/Profile/');?>">General Settings </a></li>
                                    <li><a href="<?php echo base_url('Dashboard/Profile/BankDetails');?>">Bank Details </a></li>
                                    <li><a href="<?php echo base_url('Dashboard/Profile/id_card');?>">ID Card </a></li>
                                    <li><a href="<?php echo base_url('Dashboard/Profile/Password');?>">Password </a></li>
                                    <li><a href="<?php echo base_url('Dashboard/Profile/TxnPassword');?>">Txn. Password </a></li>
                                    <li><a href="<?php echo base_url('Dashboard/Profile/TdsDetails');?>">Tds Details </a></li>                                   
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Fund' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Fund Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Fund' ? 'block' : 'none';?>">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/Fund/Request_fund');?>">Fund Request</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/Fund/requests');?>">Request Fund History</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/Fund/transfer_fund');?>">Transfer Fund</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/Fund/fund_history');?>">Fund History</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Epins' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Epins Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Epins' ? 'block' : 'none';?>">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/EPins/index/0');?>">Available Epins</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/EPins/index/1');?>">Used Pins</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/EPins/index/2');?>">Transferred Pins</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/EPins/index/3');?>">EPins History</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/EPins/transfer_epins');?>">EPin Transfer</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/EPins/generate_epin');?>">Generate EPin</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Payout' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Payout Reports</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Payout' ? 'block' : 'none';?>">
                                <ul>
                                    <?php
                                    $incomes = incomes();
                                    foreach($incomes as $key => $income)
                                    echo'<li><a href="'.base_url('Dashboard/Payout/Income/'.$key).'">'.$income.'</a></li>';
                                    ?>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/Payout/index');?>">Income Ledgar</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/Payout/DateWise');?>">Datewise Payout</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Withdraw' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Withdraw Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Withdraw' ? 'block' : 'none';?>">
                                <ul>
                                    <li><a href="<?php echo base_url('Dashboard/Withdraw');?>">Withdraw</a></li>
                                    <li><a href="<?php echo base_url('Dashboard/Withdraw/TransferIncome');?>">Transfer Income</a></li>
                                    <li><a href="<?php echo base_url('Dashboard/Withdraw/Summary');?>">Withdraw Summary</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Support' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Support</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Ticket' ? 'block' : 'none';?>">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/Ticket/Generate');?>">Generate Ticket</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/Ticket/index');?>">Previous Ticket</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="<?php echo base_url('Admin/logout');?>">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </nav>