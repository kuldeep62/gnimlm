<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> </h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/EPins/index/3')?>">EPins</a></li>
                        <li class="breadcrumb-item active"><?php echo $header;?></li>
                    </ul>
                </div>
                <hr>
                <form method="GET" action="<?php echo base_url('Dashboard/Network/Directs');?>">
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="date" name="start_date" class="form-control float-right"
                                value="<?php echo $start_date;?>">
                        </div>
                            <div class="col-sm-3">
                            <input type="date" name="end_date" class="form-control float-right"
                                value="<?php echo $end_date;?>">
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name="type">
                                <option value="name" <?php echo $type == 'name' ? 'selected' : '';?>>
                                    Name</option>
                                <option value="user_id" <?php echo $type == 'user_id' ? 'selected' : '';?>>
                                    User ID</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="value" class="form-control float-right"
                                value="<?php echo $value;?>" placeholder="Search">
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-light"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="row">
                    <table class="table table-bordered table-striped dataTable" id="tableView">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User ID</th>
                                <th>Name</th>
                                <th>Sponser ID</th>
                                <th>Level</th>
                                <th>Registered At</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $record['user']['user_id']; ?></td>
                                    <td><?php echo $record['user']['name']; ?></td>
                                    <td><?php echo $record['user']['sponser_id']; ?></td>
                                    <td><?php echo $record['level']; ?></td>
                                    <td><?php echo $record['user']['created_at']; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>

                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p> Showing <?php echo ($segament + 1) .' to ' .($i - 1) . ' of '.$total_records; ?> records</p>
                    </div>
                    <div class="col-sm-9">
                        <?php
                            echo $this->pagination->create_links();
                        ?>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>