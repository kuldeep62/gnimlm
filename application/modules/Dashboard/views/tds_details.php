<?php $this->load->view('header');?>
<main class="page-content">
    <div class="container-fluid">
        <h2><?php echo $header;?> (<?php echo $total_records;?>)</h2>
        <hr>
        <div class="row">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Profile/')?>">Profile</a></li>
                <li class="breadcrumb-item active">Tds Details</li>
            </ul>
        </div>
        <form method="GET">
            <div class="row">
                <div class="col-sm-3">
                    <input type="date" name="start_date" class="form-control float-right"
                        value="<?php //echo $start_date;?>">
                </div>
                    <div class="col-sm-3">
                    <input type="date" name="end_date" class="form-control float-right"
                        value="<?php //echo $end_date;?>">
                </div>
                <div class="col-sm-2">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-light"><i class="fas fa-search"></i></button>
                        <a class="btn btn-light" href="<?php echo base_url('Dashboard/Profile/ExportTds/');?>">
                            <i class="fas fa-download"></i>
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <hr>
        <div class="row">
            <table class="table table-hover" id="">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Amount</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = $segament + 1;
                    foreach ($records as $key => $record) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $record['tds']; ?></td>
                            <td><?php echo $record['created_at']; ?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>

                </tbody>
            </table>
            <?php
            echo $this->pagination->create_links();
            ?>
        </div>
    </div>

</main>
</div>
<?php $this->load->view('footer');?>