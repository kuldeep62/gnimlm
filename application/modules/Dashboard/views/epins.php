<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> </h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/EPins/index/3')?>">EPins</a></li>
                        <li class="breadcrumb-item active"><?php echo $header;?></li>
                    </ul>
                </div>
                <hr>
                <form method="GET">
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="date" name="start_date" class="form-control float-right"
                                value="<?php echo $start_date;?>">
                        </div>
                            <div class="col-sm-3">
                            <input type="date" name="end_date" class="form-control float-right"
                                value="<?php echo $end_date;?>">
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-light"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="row">
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Epin</th>
                                <th>Received From</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Used For</th>
                                <th> Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $record['epin']; ?></td>
                                    <td><?php echo $record['sender_id'] == '' ? 'Admin' : $record['sender_id']; ?></td>
                                    <td><?php echo $record['amount']; ?></td>
                                    <td>
                                        <?php
                                        if($record['status'] == 0){
                                            echo '<span class="text-info">Unused</span>';
                                        }elseif($record['status'] == 1){
                                            echo'<span class="text-success">Used</span>';
                                        }elseif($record['status'] == 2){
                                            echo'<span class="text-primary">Transferred</span>';
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $record['used_for']; ?></td>
                                    <td><?php
                                    if($record['status'] == 0){
                                        echo'<a href="'.base_url('Dashboard/AccountActivation/EpinActivation/'.$record['epin']).'">Acivate User</a>';
                                    }
                                    ?></td>
                                    <td><?php echo $record['created_at']; ?></td>
                                  </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p> Showing <?php echo ($segament + 1) .' to ' .($i - 1) . ' of '.$total_records; ?> records</p>
                    </div>
                    <div class="col-sm-9">
                        <?php
                            echo $this->pagination->create_links();
                        ?>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>