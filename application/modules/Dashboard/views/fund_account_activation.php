<?php $this->load->view('header');?>

        <main class="page-content">
            <div class="container-fluid">

                <h2>Fund Account Activation</h2>
                Available Balance (<?php echo currency . $user['wallet_amount']?>)
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/')?>">Account Acitvation</a></li>
                        <li class="breadcrumb-item active">Fund Account Activation</li>
                    </ul>
                </div>
                <?php echo form_open();?>
                    <h2 class="text-danger"><?php echo $this->session->flashdata('message');?></h2>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group">
                            <label>Choose Package</label>
                            <select class="form-control" name="package_id">
                                <?php
                                foreach($packages as $key => $package){
                                    echo'<option value="'.$package['id'].'">'.$package['title'].' ('. currency .$package['price'].')</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>User ID</label>
                            <input type="text" class="form-control" id="user_id" name="user_id" value="<?php echo set_value('user_id'); ?>" placeholder="User ID" style="max-width: 400px"/>
                            <span class="text-danger"><?php echo form_error('user_id') ?></span>
                            <span class="text-danger" id="errorMessage"></span>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo form_input(['type' => 'submit' , 'name' =>'Activate','class'=>'btn btn-success','value' => 'Acitvate']);                                
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>
<script>
$("form.proofForm").submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    var t = $(this);
    t.find('.loader').css('display','block');
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        success: function (data) {
            var res = JSON.parse(data)
            alert(res.message);
            t.append('<input type="hidden" name="'+res.csrfName+'" value="'+res.csrfHash+'" style="display:none;">')
            t.find('.loader').css('display','none');
            if(res.success == 1)
                location.reload();

        },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>