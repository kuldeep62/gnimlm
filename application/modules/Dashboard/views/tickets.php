<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> </h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/EPins/index/3')?>">EPins</a></li>
                        <li class="breadcrumb-item active"><?php echo $header;?></li>
                    </ul>
                </div>
                <hr>
                <!-- <form method="GET">
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="date" name="start_date" class="form-control float-right"
                                value="<?php echo $start_date;?>">
                        </div>
                            <div class="col-sm-3">
                            <input type="date" name="end_date" class="form-control float-right"
                                value="<?php echo $end_date;?>">
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-light"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form> -->
                <hr>
                <div class="row">
                    <table class="table table-bordered table-striped dataTable" id="tableView">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Status</th>
                                <th>Remark</th>
                                <th>CreatedAt</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($tickets as $key => $ticket) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo ucwords(str_replace('_',' ',$ticket['subject'])); ?></td>
                                    <td>Rs. <?php echo $ticket['message']; ?></td>
                                    <td><?php
                                        if ($ticket['status'] == 0) {
                                            echo'<span class="text-primary">Pending</span>';
                                        } elseif ($ticket['status'] == 1) {
                                            echo'<span class="text-success">Resolved</span>';
                                        }
                                        ?></td>
                                    <td><?php echo $ticket['remark']; ?></td>
                                    <td><?php echo $ticket['created_at']; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>

                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p> Showing <?php echo ($segament + 1) .' to ' .($i - 1) . ' of '.$total_records; ?> records</p>
                    </div>
                    <div class="col-sm-9">
                        <?php
                            echo $this->pagination->create_links();
                        ?>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>