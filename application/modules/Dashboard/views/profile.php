<?php $this->load->view('header');?>

        <main class="page-content">
            <div class="container-fluid">

                <h2>Profile Management</h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard/Profile/')?>">Profile</a></li>
                        <li class="breadcrumb-item active">General Settings</li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <?php echo form_open();
                    ?> <h2 class="text-danger"><?php echo $this->session->flashdata('message');?></h2>
                        <div class="form-group">
                            <label>UserID</label>
                            <?php echo form_input(['type' => 'text','class' => 'form-control','value'=>$user['user_id'] ,'disabled' => true]);;?>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <?php echo form_input(['type' => 'text','class' => 'form-control','value'=>$user['phone'] ,'disabled' => true]);?>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <?php echo form_input(['type' => 'text','class' => 'form-control','value'=>$user['email'] ,'disabled' => true]);?>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <?php echo form_input(['type' => 'text','name'=> 'name','class' => 'form-control','value'=>$user['name'] ]);?>
                            <span class="text-danger"><?php echo form_error('name');?></span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success">Save</button>
                        </div>
                    <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php $this->load->view('footer');?>