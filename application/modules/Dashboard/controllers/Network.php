<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Network extends MX_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'encryption', 'form_validation', 'security', 'pagination'));
        $this->load->model(array('Dashboard_model'));
        $this->load->helper(array('dashboard'));
    }
	public function index()
	{
		if (is_logged_in()) {
			$this->load->view('index');
		} else {
            redirect('Site/login');
        }
	}
    public function Directs(){
        if (is_logged_in()) {
            $response = [];
            $response['header'] = 'Direct Refferals';
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);
            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }
            $where['sponser_id'] = $this->session->userdata['user_id'];
            $config['base_url'] = base_url('Dashboard/Network/Directs/') ;
            $config['total_rows'] = $this->Dashboard_model->get_sum('tbl_users', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            $config['suffix'] = '?'.http_build_query($_GET);
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end' style='margin:20px 0'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];

            $response['records'] = $this->Dashboard_model->get_limit_records('tbl_users', $where, 'id,user_id,name,phone,email,income,wallet_amount,created_at', $config['per_page'], $segment);
            $this->load->view('directs',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function Team(){
        if (is_logged_in()) {
            $response = [];
            $response['header'] = 'Team';
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);
            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }
            $where['user_id'] = $this->session->userdata['user_id'];
            $config['base_url'] = base_url('Dashboard/Network/Team/') ;
            $config['total_rows'] = $this->Dashboard_model->get_sum('tbl_sponser_count', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            $config['suffix'] = '?'.http_build_query($_GET);
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end' style='margin:20px 0'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];

            $response['records'] = $this->Dashboard_model->get_limit_records('tbl_sponser_count', $where, 'id,downline_id,level', $config['per_page'], $segment);
            foreach($response['records'] as $key => $record){
                $response['records'][$key]['user'] = $this->Dashboard_model->get_single_record('tbl_users',['user_id' => $record['downline_id']],'id,user_id,sponser_id,name,created_at');
            }
            $this->load->view('team',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function Genelogy() {
        if (is_logged_in()) {
            $response = array();
            $response['header'] = 'Genelogy View';
            $response['directs'] = $this->Dashboard_model->get_records('tbl_users', ['sponser_id' => $this->session->userdata['user_id']], 'id,user_id,name,sponser_id');
            $this->load->view('genelogy', $response);
        } else {
            redirect('Site/login');
        }
    }
    public function genelogy_users($user_id) {
        if (is_logged_in()) {
            $response = array();
            $response['directs'] = $this->Dashboard_model->get_records('tbl_users', array('sponser_id' => $user_id), 'id,user_id,name,sponser_id');
            echo json_encode($response);
        } else {
            redirect('Site/login');
        }
    }
    public function TreeView($user_id) {
        if (is_logged_in()) {
            $response = array();
            $response['header'] = 'Tree View';
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $user_id), 'id,user_id,sponser_id,role,name,email,phone,paid_status,created_at');
            $response['users'] = $this->Dashboard_model->get_records('tbl_users', array('sponser_id' => $user_id), 'id,user_id,sponser_id,role,name,email,phone,paid_status,created_at');
            foreach($response['users'] as $key => $directs){
                $response['users'][$key]['sub_directs'] = $this->Dashboard_model->get_records('tbl_users', array('sponser_id' => $directs['user_id']), 'id,user_id,sponser_id,role,name,email,phone,paid_status,created_at');
            }
            $this->load->view('tree_view', $response);
        } else {
            redirect('Site/login');
        }
    }
    public function PoolView($user_id) {
        if (is_logged_in()) {
            $response = array();
            $response['header'] = 'Pool View';
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_pool', array('user_id' => $user_id), 'id,user_id,upline_id,created_at');
            $response['users'] = $this->Dashboard_model->get_records('tbl_pool', array('upline_id' => $user_id), 'id,user_id,upline_id,created_at');
            foreach($response['users'] as $key => $directs){
                $response['users'][$key]['sub_directs'] = $this->Dashboard_model->get_records('tbl_pool', array('upline_id' => $directs['user_id']), 'id,user_id,upline_id,created_at');
            }
            $this->load->view('pool_view', $response);
        } else {
            redirect('Site/login');
        }
    }
}
