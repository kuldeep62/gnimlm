<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdraw extends MX_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'encryption', 'form_validation', 'security', 'pagination'));
        $this->load->model(array('Dashboard_model'));
        $this->load->helper(array('dashboard'));
        $this->minimum_withdraw = 100;
        $this->withdraw_mulitple = 200;
        $this->admin_charges = 10;
        $this->tds = 5;
        $this->payable_amount = 85;
    }
	public function index()
	{
		if (is_logged_in()) {
            $response['header'] = 'Withdraw';
            $response['bank'] = $this->Dashboard_model->get_single_record('tbl_user_details',['user_id' => $this->session->userdata['user_id']],'*');
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
                $this->form_validation->set_rules('master_key', 'Txn. Pin', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    if($data['amount'] >= $this->minimum_withdraw){
                        if($data['amount'] % $this->withdraw_mulitple == 0){
                            if($response['bank']['kyc_status'] == 2){
                                $user = $this->Dashboard_model->get_single_record('tbl_users',['user_id' => $this->session->userdata['user_id']],'id,user_id,master_key,phone,income');
                                if($user['income'] >= $data['amount']){
                                    if($user['master_key'] == $data['master_key']){
                                        $incomededuction = array(
                                            'user_id' => $this->session->userdata['user_id'],
                                            'amount' => - $data['amount'] ,
                                            'type' => 'withdraw_request', 
                                            'description' => 'Withdraw Request',
                                        );
                                        income_transaction($incomededuction);
                                        $withdrawArr = array(
                                            'user_id' => $this->session->userdata['user_id'],
                                            'amount' => $data['amount'],
                                            'type' => 'withdraw',
                                            'tds' => $data['amount'] * $this->tds /100,
                                            'admin_charges' => $data['amount'] * $this->admin_charges /100,
                                            'fund_conversion' => 0,
                                            'payable_amount' => $data['amount'] * $this->payable_amount / 100,
                                        );
                                        $this->Dashboard_model->add('tbl_withdraw', $withdrawArr);
                                        $this->session->set_flashdata('message', 'Withdraw Requested submitted Successfully');
                                    }else{
                                        $this->session->set_flashdata('message', 'Invalid Txn. Pin');
                                    }
                                }else{
                                    $this->session->set_flashdata('message', 'Insufficient Balance');
                                }
                            }else{
                                $this->session->set_flashdata('message', 'Your Kyc Status is Not Apporved Please Contact to Administrator');
                            }
                        }else{
                            $this->session->set_flashdata('message', 'Withdraw Amount Should be multiple of '.currency.$this->withdraw_mulitple);
                        }
                    }else{
                        $this->session->set_flashdata('message', 'Minimum Withdrawal Amount is '.currency.$this->minimum_withdraw);
                    }
                }
            }
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_users',['user_id' => $this->session->userdata['user_id']],'id,user_id,name,phone,income');
            $response['withdraw_control'] = $this->Dashboard_model->get_single_record('tbl_withdraw_control',['type' => 'withdraw'],'*');
            $this->load->view('withdraw',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function TransferIncome(){
        if (is_logged_in()) {
            $response['header'] = 'Generate Fund';
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
                $this->form_validation->set_rules('master_key', 'Txn. Pin', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $user = $this->Dashboard_model->get_single_record('tbl_users',['user_id' => $this->session->userdata['user_id']],'id,user_id,master_key,phone,income');
                    if($user['income'] >= $data['amount']){
                        if($user['master_key'] == $data['master_key']){
                            $incomededuction = array(
                                'user_id' => $this->session->userdata['user_id'],
                                'amount' => - $data['amount'] ,
                                'type' => 'fund_generation', 
                                'description' => 'Amount Used for fund generation',
                            );
                            income_transaction($incomededuction);
                            $withdrawArr = array(
                                'user_id' => $this->session->userdata['user_id'],
                                'amount' => $data['amount'],
                                'type' => 'fund_generation',
                                'tds' => $data['amount'] * $this->tds /100,
                                'admin_charges' => $data['amount'] * $this->admin_charges /100,
                                'fund_conversion' => 0,
                                'payable_amount' => $data['amount'] * $this->payable_amount / 100,
                            );
                            $walletArr = [
                                'user_id' => $this->session->userdata['user_id'],
                                'amount' => $withdrawArr['payable_amount'] ,
                                'sender_id' => $this->session->userdata['user_id'],
                                'type' => 'income_transfer', 
                                'remark' => 'Fund generated from income',
                            ];
                            fund_transaction($walletArr);
                            $this->Dashboard_model->add('tbl_withdraw', $withdrawArr);
                            $this->session->set_flashdata('message', 'Fund generated Successfully');
                        }else{
                            $this->session->set_flashdata('message', 'Invalid Txn. Pin');
                        }
                    }else{
                        $this->session->set_flashdata('message', 'Insufficient Balance');
                    }
                }
            }
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_users',['user_id' => $this->session->userdata['user_id']],'id,user_id,name,phone,income');
            $this->load->view('transfer_income',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function Summary(){
        if (is_logged_in()) {
            $response = array();
            $response['header'] = 'Withdraw Summary';
            $config['base_url'] = base_url('Dashboard/Withdraw/Summary') ;
            $config['total_rows'] = $this->Dashboard_model->get_sum('tbl_withdraw', ['user_id' => $this->session->userdata['user_id']], 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            $config['suffix'] = '?'.http_build_query($_GET);
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end' style='margin:20px 0'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Dashboard_model->get_limit_records('tbl_withdraw', ['user_id' => $this->session->userdata['user_id']], '*', $config['per_page'], $segment);
            $this->load->view('withdraw_summary', $response);
        } else {
            redirect('Site/login');
        }
    }
}
