<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payout extends MX_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'pagination', 'form_validation', 'security', 'email'));
        $this->load->model(array('Dashboard_model'));
        $this->load->helper(array('dashboard'));
    }
	public function index()
	{
		if (is_logged_in()) {
            $response = array();
            $response['header'] = 'Income Ledgar';
            $config['base_url'] = base_url() . 'Dashboard/Payout/index';
            $config['total_rows'] = $this->Dashboard_model->get_sum('tbl_income_wallet', ['user_id' => $this->session->userdata('user_id')], 'ifnull(count(id),0) as sum');
            $config['per_page'] = 10;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['total_sum'] = $this->Dashboard_model->get_sum('tbl_income_wallet', ['user_id' => $this->session->userdata('user_id')], 'ifnull(sum(amount),0) as sum');
            $response['records'] = $this->Dashboard_model->get_limit_records('tbl_income_wallet', ['user_id' => $this->session->userdata('user_id')], '*', $config['per_page'], $segment);

        	$this->load->view('income_ledgar',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function income($type)
	{
        if (is_logged_in()) {
            $response = array();
            $response['header'] = get_income_name($type);
            $config['base_url'] = base_url() . 'Dashboard/Payout/income/'.$type;
            $config['total_rows'] = $this->Dashboard_model->get_sum('tbl_income_wallet', ['user_id' => $this->session->userdata('user_id') , 'type' => $type], 'ifnull(count(id),0) as sum');
            $config['per_page'] = 10;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['total_sum'] = $this->Dashboard_model->get_sum('tbl_income_wallet', ['user_id' => $this->session->userdata('user_id'), 'type' => $type], 'ifnull(sum(amount),0) as sum');
            $response['records'] = $this->Dashboard_model->get_limit_records('tbl_income_wallet', ['user_id' => $this->session->userdata('user_id'), 'type' => $type], '*', $config['per_page'], $segment);

            $this->load->view('income_ledgar',$response);
        } else {
            redirect('Site/login');
        }
    }
    public function DateWise()
	{
        if (is_logged_in()) {
            $response = array();
            $response['header'] = 'Datewise Payout';
            $config['base_url'] = base_url() . 'Dashboard/Payout/DateWise/';
            $config['total_rows'] = $this->Dashboard_model->payout_dates_count($this->session->userdata('user_id'));
            $config['per_page'] = 10;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Dashboard_model->datewise_payout($this->session->userdata('user_id'),$config['per_page'], $segment);
            foreach($response['records'] as $key => $record){
                $incomes = $this->Dashboard_model->get_incomes('tbl_income_wallet', array('date(created_at)' => $record['date'],'amount > ' => 0,'user_id' => $this->session->userdata('user_id')), 'ifnull(sum(amount),0) as income , type');
                $response['records'][$key]['incomes'] = calculate_incomes($incomes);
            }
            $this->load->view('datewise_payout',$response);
        } else {
            redirect('Site/login');
        }
    }
}
