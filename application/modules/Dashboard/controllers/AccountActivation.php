<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountActivation extends MX_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'encryption', 'form_validation', 'security', 'email'));
        $this->load->model(array('Dashboard_model'));
        $this->load->helper(array('dashboard'));
    }
	public function index()
	{
		if (is_logged_in()) {
			$this->load->view('index');
		} else {
            redirect('Site/login');
        }
	}

    public function EpinActivation($epin) {
        if (is_logged_in()) {
            $response['epin'] = $epin;
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $user_id = $data['user_id'];
                    $user = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $user_id), '*');
                    $pin_status = $this->Dashboard_model->get_single_record('tbl_epins', array('user_id' => $this->session->userdata['user_id'], 'epin' => $data['epin']), '*');
                    if (!empty($pin_status)) {
                        $package = $this->Dashboard_model->get_single_record('tbl_package', array('id' => $data['package_id']), '*');
                        if (!empty($user)) {
                            if ($pin_status['status'] == 0) {
                                    if ($user['paid_status'] == 0) {
                                        $topupData = array(
                                            'paid_status' => 1,
                                            'package_id' => $data['package_id'],
                                            'package_amount' => $package['price'],
                                            'topup_date' => date('Y-m-d H:i:s'),
                                            'capping' => $package['capping'],
                                        );
                                        $this->Dashboard_model->update('tbl_users', array('user_id' => $user_id), $topupData);
                                        $this->Dashboard_model->update('tbl_sponser_count', array('downline_id' => $user_id), ['paid_status' => 1]);
                                        $this->Dashboard_model->update('tbl_epins', array('id' => $pin_status['id']), array('used_for' => $user['user_id'], 'status' => 1));
                                        $this->Dashboard_model->update_directs($user['sponser_id']);
                                        $incomeArr = array(
                                            'user_id' => $user['sponser_id'],
                                            'amount' => $package['direct_income'] ,
                                            'type' => 'direct_income', 
                                            'description' => 'Direct Income from Activation of Member '.$user_id,
                                        );
                                        income_transaction($incomeArr);
                                        $this->pool_entry($user['user_id']);
                                        $this->level_income($user['sponser_id'] , $user['user_id'],$package['level_income']);
                                        $sms_text = 'Dear User Login ID : '.$user['user_id'] .' Account Has been Successfully Activated Please Wait for your PH links '.base_url();
                                        // notify_user($user['user_id'], $sms_text);
                                        $this->session->set_flashdata('message', 'Account Activated Successfully');
                                    } else {
                                        $this->session->set_flashdata('message', 'This Account Already Acitvated');
                                    }

                            } else {
                                $this->session->set_flashdata('message', 'Expired Epin');
                            }
                        } else {
                            $this->session->set_flashdata('message', 'Invalid User ID');
                        }
                    } else {
                        $this->session->set_flashdata('message', 'This Pin is not valid for you');
                    }
                }
            }
            $response['pin'] = $this->Dashboard_model->get_single_record('tbl_epins',['user_id' => $this->session->userdata['user_id'], 'epin' => $epin],'*');
            $response['packages'] = $this->Dashboard_model->get_records('tbl_package',['price' => $response['pin']['amount']],'*');
            // pr($response,true);
			$this->load->view('epin_account_activation',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function FundActivation() {
        if (is_logged_in()) {
            $response = [];
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $user_id = $data['user_id'];
                    $user = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $user_id), '*');
                    $balance = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $this->session->userdata['user_id']), 'wallet_amount');
                    $package = $this->Dashboard_model->get_single_record('tbl_package', array('id' => $data['package_id']), '*');
                    if (!empty($user)) {
                        if ($balance['wallet_amount'] >= $package['price']) {
                                if ($user['paid_status'] == 0) {
                                    $topupData = array(
                                        'paid_status' => 1,
                                        'package_id' => $data['package_id'],
                                        'package_amount' => $package['price'],
                                        'topup_date' => date('Y-m-d H:i:s'),
                                        'capping' => $package['capping'],
                                    );
                                    $this->Dashboard_model->update('tbl_users', array('user_id' => $user_id), $topupData);
                                    $this->Dashboard_model->update('tbl_sponser_count', array('downline_id' => $user_id), ['paid_status' => 1]);
                                    $walletArr = [
                                        'user_id' => $this->session->userdata['user_id'],
                                        'amount' => - $package['price'],
                                        'type' => 'account_activation',
                                        'remark' => 'Fund Used For '.$user_id . ' Account Activation ',
                                    ];
                                    fund_transaction($walletArr); 
                                    $this->Dashboard_model->update_directs($user['sponser_id']);
                                    $incomeArr = array(
                                        'user_id' => $user['sponser_id'],
                                        'amount' => $package['direct_income'] ,
                                        'type' => 'direct_income', 
                                        'description' => 'Direct Income from Activation of Member '.$user_id,
                                    );
                                    income_transaction($incomeArr);
                                    $this->pool_entry($user['user_id']);
                                    $this->level_income($user['sponser_id'] , $user['user_id'],$package['level_income']);
                                    $sms_text = 'Dear User Login ID : '.$user['user_id'] .' Account Has been Successfully Activated Please Wait for your PH links '.base_url();
                                    // notify_user($user['user_id'], $sms_text);
                                    $this->session->set_flashdata('message', 'Account Activated Successfully');
                                } else {
                                    $this->session->set_flashdata('message', 'This Account Already Acitvated');
                                }

                        } else {
                            $this->session->set_flashdata('message', 'Insufficient Balance');
                        }
                    } else {
                        $this->session->set_flashdata('message', 'Invalid User ID');
                    }
                }
            }
            $response['packages'] = $this->Dashboard_model->get_records('tbl_package',[],'*');
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_users',['user_id' => $this->session->userdata['user_id']],'wallet_amount');
			$this->load->view('fund_account_activation',$response);
		} else {
            redirect('Site/login');
        }
    }
    function level_income($sponser_id , $activated_id,$package_income){
        $incomes = explode(',',$package_income);
        foreach($incomes as $key => $income){
            $sponser = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $sponser_id), 'id,user_id,sponser_id,paid_status');
            if(!empty($sponser)){
                if($sponser['paid_status'] == 1){
                    $LevelIncome = array(
                        'user_id' => $sponser['user_id'],
                        'amount' => $income ,
                        'type' => 'level_income', 
                        'description' => 'Level Income from Activation of Member '.$activated_id  . ' At level '.($key + 1),
                    );
                    income_transaction($LevelIncome);
                }
                $sponser_id = $sponser['sponser_id'];
            }
        }
    }
    public function pool_entry($user_id){
        $pool_upline = $this->Dashboard_model->get_single_record('tbl_pool', array('level1 <' => 3), 'id,user_id,level1');
        if(!empty($pool_upline)){
            $poolArr =  array(
                'user_id' => $user_id,
                'upline_id' => $pool_upline['user_id'],
            );
            $this->Dashboard_model->add('tbl_pool', $poolArr);
            $this->update_pool_count($pool_upline['user_id'] , 1);
            // $this->check_pool_stats();
        }else{
            $poolArr =  array(
                'user_id' => $user_id,
                'upline_id' => 'none',
            );
            $this->Dashboard_model->add('tbl_pool', $poolArr);
        }
    }
    public function update_pool_count($user_id , $level){
        if($level <= 3){
            $upline = $this->Dashboard_model->get_single_record('tbl_pool', array('user_id' => $user_id), 'id,user_id,upline_id,level1,level2,level3');
            if(!empty($upline)){
                $pool_count = $upline['level'.$level] + 1;
                if($pool_count == pow(3,$level))
                {
                    $this->credit_pool_income($upline['user_id'],$level);                   
                }
                $this->Dashboard_model->update('tbl_pool', array('user_id' => $upline['user_id']),array('level'.$level => $upline['level'.$level] + 1));
                $this->update_pool_count($upline['upline_id'],$level + 1);
            }
        }
    }
    public function credit_pool_income($user_id , $level){
        $user = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $user_id), 'id,user_id,package_id');
        $package = $this->Dashboard_model->get_single_record('tbl_package', array('id' => $user['package_id']), 'id,pool_income');
        $pool_incomes = explode(',',$package['pool_income']);
        $pool_income = $pool_incomes[$level - 1];
        $poolIncome = array(
            'user_id' => $user['user_id'],
            'amount' => $pool_income ,
            'type' => 'pool_income', 
            'description' => 'Pool Income At Level '.$level,
        );
        income_transaction($poolIncome);
    }

}
