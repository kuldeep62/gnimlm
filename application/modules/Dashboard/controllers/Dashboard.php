<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'encryption', 'form_validation', 'security', 'email'));
        $this->load->model(array('dashboard_model'));
        $this->load->helper(array('dashboard'));
    }
	public function index()
	{
		if (is_logged_in()) {
            $response = [];
            $user_id = $this->session->userdata('user_id');
            $response['user'] = $this->dashboard_model->get_single_record('tbl_users',['user_id' => $user_id],'*');
            $response['pool'] = $this->dashboard_model->get_single_record('tbl_pool',['user_id' => $user_id],'*');
            $incomes = $this->dashboard_model->get_incomes('tbl_income_wallet', ['user_id' => $user_id ,'amount > ' => 0 ,'type !=' => 'withdraw_reject'], 'ifnull(sum(amount),0) as income,type');
            $response['incomes'] = calculate_incomes($incomes);
            $response['income_names'] = [];
            foreach($response['incomes'] as $key =>$income){
                array_push($response['income_names'],get_income_name($key));
            }

            $response['paid_team'] = $this->dashboard_model->get_single_record('tbl_sponser_count',['user_id' => $user_id ,'paid_status' => 1],'ifnull(count(id),0) as paid_team');
            $response['free_team'] = $this->dashboard_model->get_single_record('tbl_sponser_count',['user_id' => $user_id ,'paid_status' => 0],'ifnull(count(id),0) as free_team');
            $response['available_pins'] = $this->dashboard_model->available_pins($user_id);
            $response['news'] = $this->dashboard_model->get_records('tbl_news',[],'*');
            $response['popup'] = $this->dashboard_model->get_single_record('tbl_popup',[],'*');
            // pr($response,true);
			$this->load->view('index',$response);
		} else {
            redirect('Site/login');
        }
	}

    public function logout() {
        $this->session->unset_userdata(array('user_id', 'role'));
        redirect('Site/login');
    }

}
