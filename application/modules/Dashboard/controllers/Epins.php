<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Epins extends MX_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'pagination', 'form_validation', 'security', 'email', 'pagination'));
        $this->load->model(array('Dashboard_model'));
        $this->load->helper(array('dashboard'));
    }
    public function index($status) {
        if (is_logged_in()) {
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);
            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }

            if($status == 0){
                $where['user_id'] = $this->session->userdata['user_id'];
                $where['status'] = 0;
                $response['header'] = 'Avaialble Epins';
            }elseif($status == 1){
                $where['user_id'] = $this->session->userdata['user_id'];
                $where['status'] = 1;
                $response['header'] = 'Used Epins';
            }elseif($status == 2){
                $where['user_id'] = $this->session->userdata['user_id'];
                $where['status'] = 2;
                $response['header'] = 'Transferred Epins';
            }
            else if ($status == 3) {
                $where['user_id'] = $this->session->userdata['user_id'];
                $response['header'] = 'Epins History';
            }

            $config['base_url'] = base_url('Dashboard/EPins/index/'.$status) ;
            $config['total_rows'] = $this->Dashboard_model->get_sum('tbl_epins', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 10;
            $config['suffix'] = '?'.http_build_query($_GET);
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end' style='margin:20px 0'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Dashboard_model->get_limit_records('tbl_epins', $where, '*', $config['per_page'], $segment);
            $this->load->view('epins', $response);
        } else {
            redirect('Site/login');
        }
    }
    public function transfer_epins() {
        if (is_logged_in()) {
            $response = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('epins', 'Epins', 'trim|required|numeric|xss_clean');
                $this->form_validation->set_rules('user_id', 'User ID', 'trim|xss_clean');
                $this->form_validation->set_rules('master_key', 'Master Key', 'trim|required|xss_clean');
                $this->form_validation->set_rules('pin_amount', 'Pin Amount', 'trim|required|numeric|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $user = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $data['user_id']), 'user_id,phone');
                    $master_key = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $this->session->userdata['user_id']), 'user_id,phone,master_key');
                    if ($master_key['master_key'] == $data['master_key']) {
                        if (!empty($user)) {
                            $available_pins = $this->Dashboard_model->get_single_record('tbl_epins', array('user_id' => $this->session->userdata['user_id'], 'status' => 0 , 'amount' => $data['pin_amount']), 'ifnull(count(id),0) as total_epins');
                            if ($data['epins'] > 0) {
                                if ($data['epins'] <= $available_pins['total_epins']) {
                                    for ($i = 1; $i <= $data['epins']; $i++) {
                                        $pin = $this->Dashboard_model->get_single_record('tbl_epins', array('user_id' => $this->session->userdata['user_id'], 'status' => 0, 'amount' => $data['pin_amount']), '*');
                                        $this->Dashboard_model->update('tbl_epins', array('id' => $pin['id']), array('status' => 2, 'used_for' => $data['user_id']));
                                        $packArr = array(
                                            'user_id' => $data['user_id'],
                                            'epin' => $this->generate_pin(),
                                            'amount' => $data['pin_amount'],
                                            'sender_id' => $this->session->userdata['user_id']
                                        );
                                        $res = $this->Dashboard_model->add('tbl_epins', $packArr);
                                        $this->session->set_flashdata('message', 'Pin Transferred Successfully');
                                    }
                                }else{
                                    $this->session->set_flashdata('message', 'Insuffcient Pin Balance');
                                }
                            } else {
                                $this->session->set_flashdata('message', 'Please Set Atleast 1 Pin');
                            }
                        } else {
                            $this->session->set_flashdata('message', 'Invalid User ID');
                        }
                    } else {
                        $this->session->set_flashdata('message', 'Invalid Master Key');
                    }
                }
            }
            $response['available_epins'] = $this->Dashboard_model->available_pins($this->session->userdata['user_id']);
            $this->load->view('transfer_epin', $response);
        } else {
            redirect('Site/login');
        }
    }
    public function generate_pin() {
        if (is_logged_in()) {
            $epin = md5(rand(100000, 9999999));
            $pin = $this->Dashboard_model->get_single_record('tbl_epins', array('epin' => $epin), '*');
            if (!empty($pin)) {
                return $this->generate_pin();
            } else {
                return $epin;
            }
        }
    }
    public function generate_epin() {
        if (is_logged_in()) {
            $response = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('epins', 'Epins', 'trim|required|numeric|xss_clean');
                $this->form_validation->set_rules('master_key', 'Master Key', 'trim|required|xss_clean');
                $this->form_validation->set_rules('pin_amount', 'Pin Amount', 'trim|required|numeric|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $user = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $this->session->userdata['user_id']), 'user_id,income');
                    $master_key = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $user['user_id']), 'user_id,phone,master_key');
                    if ($master_key['master_key'] == $data['master_key']) {
                        $total_pin_amount = $data['pin_amount'] * $data['epins'];
                        if ($user['income'] >= $total_pin_amount) {
                            $incomeArr = array(
                                'user_id' => $user['user_id'],
                                'amount' => - $total_pin_amount ,
                                'type' => 'pin_generation', 
                                'description' => 'Amount Used For Pin Generation',
                            );
                            income_transaction($incomeArr);
                            for ($i = 1; $i <= $data['epins']; $i++) {
                                 $packArr = array(
                                    'user_id' => $user['user_id'],
                                    'epin' => $this->generate_pin(),
                                    'amount' => $data['pin_amount'],
                                    'sender_id' => $this->session->userdata['user_id']
                                );
                                $res = $this->Dashboard_model->add('tbl_epins', $packArr);
                                $this->session->set_flashdata('message', 'Pin Generated Successfully');
                            }
                        } else {
                            $this->session->set_flashdata('message', 'Insufficient Balance');
                        }
                    } else {
                        $this->session->set_flashdata('message', 'Invalid Master Key');
                    }
                }
            }
            $response['packages'] = $this->Dashboard_model->get_records('tbl_package',[],'id,price');
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_users',['user_id' => $this->session->userdata['user_id']],'id,income');
            $this->load->view('generate_epin', $response);
        } else {
            redirect('Site/login');
        }
    }
}