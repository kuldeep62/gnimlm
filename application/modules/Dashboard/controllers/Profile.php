<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MX_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'pagination', 'form_validation', 'security', 'email'));
        $this->load->model(array('Dashboard_model'));
        $this->load->helper(array('dashboard'));
    }
	public function index()
	{
		if (is_logged_in()) {
            $response = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $userData['name'] = $data['name'];
                    $updres = $this->Dashboard_model->update('tbl_users', array('user_id' => $this->session->userdata['user_id']), $userData);
                    if ($updres == true) {
                        $this->session->set_flashdata('message', 'Details Updated Successfully');
                    } else {
                        $this->session->set_flashdata('message', 'There is an error while updating Bank details Please try Again ..');
                    }
                }
            }
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_users',array('user_id' => $this->session->userdata('user_id')),'*');
        	$this->load->view('profile',$response);
		} else {
            redirect('Site/login');
        }
	}
    public function BankDetails(){
        if (is_logged_in()) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');
                $this->form_validation->set_rules('bank_account_number', 'Bank Account Number', 'trim|required|numeric|xss_clean');
                $this->form_validation->set_rules('ifsc_code', 'Ifsc Code', 'trim|required|xss_clean');
                $this->form_validation->set_rules('account_holder_name', 'Account Holder Name', 'trim|required|xss_clean');
                $this->form_validation->set_rules('pan', 'Pan Number', 'trim|required|xss_clean');
                $this->form_validation->set_rules('aadhar', 'Aadhar', 'trim|required|min_length[12]|max_length[12]|numeric|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    // $userData['account_type'] = $data['account_type'];
                    $userData['bank_account_number'] = $data['bank_account_number'];
                    $userData['bank_name'] = $data['bank_name'];
                    $userData['account_holder_name'] = $data['account_holder_name'];
                    $userData['ifsc_code'] = $data['ifsc_code'];
                    $userData['pan'] = $data['pan'];
                    $userData['aadhar'] = $data['aadhar'];
                    // $userData['kyc_status'] = 1;
                    $updres = $this->Dashboard_model->update('tbl_user_details', array('user_id' => $this->session->userdata['user_id']), $userData);
                    if ($updres == true) {
                        $this->session->set_flashdata('message', 'Details Updated Successfully');
                    } else {
                        $this->session->set_flashdata('message', 'There is an error while updating Bank details Please try Again ..');
                    }
                }
            }
            $response = array();
            $response['bank_details'] = $this->Dashboard_model->get_single_record('tbl_user_details',array('user_id' => $this->session->userdata('user_id')),'*');
        	$this->load->view('bank_details',$response);
		} else {
            redirect('Site/login');
        }
    }

    public function UploadProof() {
        if (is_logged_in()) {
            $response = array();
            $response['csrfName'] = $this->security->get_csrf_token_name();
            $response['csrfHash'] = $this->security->get_csrf_hash();

            if (!empty($_FILES['userfile'])) {
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = 100000;
                $config['file_name'] = 'id_proof' . time();
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('userfile')) {
                    $response['message'] = $this->upload->display_errors();
                    // $this->session->set_flashdata('error', $this->upload->display_errors());
                    $response['success'] = '0';
                } else {
                    $type = $this->input->post('proof_type');
                    $fileData = array('upload_data' => $this->upload->data());
                    $userData[$type] = $fileData['upload_data']['file_name'];
                    $updres = $this->Dashboard_model->update('tbl_user_details', array('user_id' => $this->session->userdata['user_id']), $userData);
                    if ($updres == true) {
                        $response['success'] = '1';
                        $response['message'] = 'Proof Uploaded Successfully';
                    } else {
                        $response['success'] = '0';
                        $response['message'] = 'There is an error while updating Bank details Please try Again ..';
                    }
                }
            } else {
                $response['message'] = 'There is an error while updating Bank details Proof Please try Again ..';
                $response['success'] = '0';
            }
            echo json_encode($response);
        } else {
            redirect('Dashboard/User/login');
        }
    }
    public function id_card() {
        $response = array();
        $this->load->view('id_card',$response);
    }
    public function Password()
	{
		if (is_logged_in()) {
            $response = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|xss_clean');
                $this->form_validation->set_rules('npassword', 'New Password', 'trim|required|xss_clean');
                $this->form_validation->set_rules('vpassword', 'Verify Password', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $cpassword = $data['cpassword'];
                    $npassword = $data['npassword'];
                    $vpassword = $data['vpassword'];
                    $user = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $this->session->userdata['user_id']), 'id,user_id,password');
                    if ($npassword !== $vpassword) {
                        $this->session->set_flashdata('message', 'Verify Password Doed Not Match');
                    } elseif ($cpassword !== $user['password']) {
                        $this->session->set_flashdata('message', 'Wrong Current Password');
                    } else {
                        $updres = $this->Dashboard_model->update('tbl_users', array('user_id' => $this->session->userdata['user_id']), array('password' => $vpassword));
                        if ($updres == true) {
                            $this->session->set_flashdata('message', 'Password Updated Successfully');
                        } else {
                            $this->session->set_flashdata('message', 'There is an error while Changing Password Please Try Again');
                        }
                    }
                }
            }
        	$this->load->view('password',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function TxnPassword()
	{
		if (is_logged_in()) {
            $response = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('cpassword', 'Confirm Txn. Password', 'trim|required|xss_clean');
                $this->form_validation->set_rules('npassword', 'New Txn. Password', 'trim|required|xss_clean');
                $this->form_validation->set_rules('vpassword', 'Verify Txn. Password', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $cpassword = $data['cpassword'];
                    $npassword = $data['npassword'];
                    $vpassword = $data['vpassword'];
                    $user = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $this->session->userdata['user_id']), 'id,user_id,master_key');
                    if ($npassword !== $vpassword) {
                        $this->session->set_flashdata('message', 'Verify Txn. Password Doed Not Match');
                    } elseif ($cpassword !== $user['master_key']) {
                        $this->session->set_flashdata('message', 'Wrong Current Txn. Password');
                    } else {
                        $updres = $this->Dashboard_model->update('tbl_users', array('user_id' => $this->session->userdata['user_id']), array('master_key' => $vpassword));
                        if ($updres == true) {
                            $this->session->set_flashdata('message', 'Txn. Password Updated Successfully');
                        } else {
                            $this->session->set_flashdata('message', 'There is an error while Changing Txn. Password Please Try Again');
                        }
                    }
                }
            }
        	$this->load->view('password',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function TdsDetails()
	{
		if (is_logged_in()) {

            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);
            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }
            $where['user_id'] = $this->session->userdata['user_id'];
            $response['header'] = 'Tds Details';
            $config['base_url'] = base_url() . 'Admin/Users/index/';
            $config['total_rows'] = $this->Dashboard_model->get_sum('tbl_withdraw', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Dashboard_model->get_limit_records('tbl_withdraw', $where, '*', $config['per_page'], $segment);
			$this->load->view('tds_details',$response);
		} else {
            redirect('Site/login');
        }
    }
    public function ExportTds(){
        $filename = 'tds_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");
        $usersData = $this->Dashboard_model->get_records('tbl_withdraw', array(), 'tds,created_at');
        $file = fopen('php://output','w');
        $header = array("TDs Amount","Date"); 
        fputcsv($file, $header);
        foreach ($usersData as $key=>$line){ 
            fputcsv($file,$line); 
        }
        fclose($file); 
        exit; 
    }
}
