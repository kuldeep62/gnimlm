<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fund extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'pagination', 'form_validation', 'security', 'email', 'pagination'));
        $this->load->model(array('Dashboard_model'));
        $this->load->helper(array('dashboard'));
    }

    public function Request_fund() {
        if (is_logged_in()) {
            $response = array();
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $this->session->set_userdata['user_id']), '*');
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] = 'payment_slip';
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('userfile')) {
                    $this->session->set_flashdata('message', $this->upload->display_errors());
                } else {
                    $fileData = array('upload_data' => $this->upload->data());
                    $reqArr = array(
                        'user_id' => $this->session->userdata['user_id'],
                        'amount' => $data['amount'],
                        'payment_method' => $data['payment_method'],
                        'image' => $fileData['upload_data']['file_name'],
                        'status' => 0,
                    );
                    $res = $this->Dashboard_model->add('tbl_payment_request', $reqArr);
                    if ($res) {
                        $this->session->set_flashdata('message', 'Payment Request Submitted Successfully');
                    } else {
                        $this->session->set_flashdata('message', 'Error While Submitting Payment Request Please Try Again ...');
                    }
                }
            }
            $this->load->view('header', $response);
            $this->load->view('request_fund', $response);
        } else {
            redirect('Site/login');
        }
    }

    public function requests() {
        if (is_logged_in()) {
            $response = array();
            $response['header'] = 'Fund Requests';
            $config['base_url'] = base_url('Dashboard/Fund/requests') ;
            $config['total_rows'] = $this->Dashboard_model->get_sum('tbl_payment_request', ['user_id' => $this->session->userdata['user_id']], 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            $config['suffix'] = '?'.http_build_query($_GET);
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end' style='margin:20px 0'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['requests'] = $this->Dashboard_model->get_limit_records('tbl_payment_request', ['user_id' => $this->session->userdata['user_id']], '*', $config['per_page'], $segment);

            // $response['requests'] = $this->Dashboard_model->get_records('tbl_payment_request', array('user_id' => $this->session->userdata['user_id']), '*');
            $this->load->view('requests', $response);
        } else {
            redirect('Site/login');
        }
    }

    public function fund_history() {
        if (is_logged_in()) {
            $response = array();
            $response['header'] = 'Fund History';
            $response['records'] = $this->Dashboard_model->get_records('tbl_wallet', array('user_id' => $this->session->userdata['user_id']), '*');
            $response['wallet_amount'] = $this->Dashboard_model->get_single_record('tbl_wallet', array('user_id' => $this->session->userdata['user_id']), 'ifnull(sum(amount),0) as wallet_amount');
            $this->load->view('fund_history', $response);
        } else {
            redirect('Site/login');
        }
    }

    public function transfer_fund() {
        if (is_logged_in()) {
            $response = array();
            $response['user'] = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $this->session->set_userdata['user_id']), '*');
            $response['wallet_amount'] = $this->Dashboard_model->get_single_record('tbl_wallet', array('user_id' => $this->session->userdata['user_id']), 'ifnull(sum(amount),0) as wallet_amount');
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
                $this->form_validation->set_rules('amount', 'Amount', 'trim|required|numeric|xss_clean');
                $this->form_validation->set_rules('remark', 'Remarks', 'trim|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    if ($data['amount'] > 0) {
                        if ($data['user_id'] != $this->session->set_userdata['user_id']) {
                            $receiver = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $data['user_id']), '*');
                            if (!empty($receiver)) {
                                if ($response['wallet_amount']['wallet_amount'] >= $data['amount']) {
                                    $senderData = array(
                                        'user_id' => $this->session->userdata['user_id'],
                                        'amount' => -$data['amount'],
                                        'sender_id' => $data['user_id'],
                                        'type' => 'fund_transfer',
                                        'remark' => $data['remark'],
                                    );
                                    $res = fund_transaction($senderData);
                                    // $res = $this->Dashboard_model->add('tbl_wallet', $senderData);
                                    // if ($res) {
                                        $response['wallet_amount']['wallet_amount'] = $response['wallet_amount']['wallet_amount'] - $data['amount'];
                                        $this->session->set_flashdata('message', 'Fund Transferred Successfully');
                                        $receiverData = array(
                                            'user_id' => $data['user_id'],
                                            'amount' => $data['amount'],
                                            'sender_id' => $this->session->userdata['user_id'],
                                            'type' => 'fund_transfer',
                                            // 'status' => 1,
                                            'remark' => $data['remark'],
                                        );
                                        fund_transaction($receiverData);
                                        // $this->Dashboard_model->add('tbl_wallet', $receiverData);
                                    // } else {
                                    //     $this->session->set_flashdata('message', 'Error While Transferring Fund Please Try Again ...');
                                    // }
                                } else {
                                    $this->session->set_flashdata('message', 'Maximum Transfer Amount is ' . $response['wallet_amount']['wallet_amount']);
                                }
                            } else {
                                $this->session->set_flashdata('message', 'Invalid Receiver Id');
                            }
                        } else {
                            $this->session->set_flashdata('message', 'You Cannot Transfer Amount In Same Account');
                        }
                    } else {
                        $this->session->set_flashdata('message', 'Minimun Transfer Amount is rs 0');
                    }
                }
            }
            $response['wallet_amount'] = $this->Dashboard_model->get_single_record('tbl_wallet', array('user_id' => $this->session->userdata['user_id']), 'ifnull(sum(amount),0) as wallet_amount');
            $this->load->view('header', $response);
            $this->load->view('transfer_fund', $response);
            $this->load->view('footer', $response);
        } else {
            redirect('Site/login');
        }
    }

	public function check_sponser($user_id){
		$response = array();
        $response['success'] = 0;
        $user = $this->Dashboard_model->get_single_record('tbl_users', array('user_id' => $user_id), 'id,user_id,sponser_id,name');
        if(!empty($user)){
			$response['success'] = 1;
			$response['user'] = $user;
        }else{
			$response['message'] = 'Sponser Not Found';
		}
		echo json_encode($response);
	}

}
