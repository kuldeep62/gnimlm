<?php
class Dashboard_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_single_record($table, $where, $select, $queryshow = false) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->row_array();
        if ($queryshow == true)
            echo $this->db->last_query();
        return $res;
    }
    public function get_sum($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where); 
        $res = $query->row_array();
        return $res['sum'];
    }
    public function get_limit_records($table, $where, $select , $limit , $offset,$show = false) {
        $this->db->select($select);
        $this->db->where($where);
        $this->db->limit($limit , $offset);
        $query = $this->db->get($table);
        $res = $query->result_array();
        if($show == true)
            echo $this->db->last_query();
        return $res;
    }

    public function get_single_record_desc($table, $where, $select, $queryshow = false) {
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_records($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->result_array();
//        echo $this->db->last_query();
        return $res;
    }

    public function update($table, $where, $data) {
        $this->db->where($where);
        $res = $this->db->update($table, $data);
//        echo $this->db->last_query();
        return $res;
    }

    public function add($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function get_single_object($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->row_object();
        return $res;
    }
    public function update_wallet($table,$where,$field,$value) {
        $this->db->set($field,$value ,FALSE);
        $this->db->where($where);
        $this->db->update($table);
    }
    public function available_pins($user_id){
        $this->db->select('ifnull(count(id),0) as pin_count , amount');
        $this->db->where(['user_id' => $user_id , 'status' => 0]);
        $this->db->group_by('amount');
        $this->db->from('tbl_epins');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function update_directs($user_id) {
        $this->db->set('directs',  'directs + 1', FALSE);
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_users');
    }
    /**To get datewise payout */
    public function datewise_payout($user_id ,$limit,$offset) {
        $this->db->select('sum(amount) as payout,date(created_at) as date');
        $this->db->from('tbl_income_wallet');
        $this->db->where(['amount >' => 0 ,'user_id' => $user_id]);
        $this->db->group_by('date(created_at)');
        $this->db->limit($limit , $offset);
        $this->db->order_by('date(created_at)','desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

/**To count dates */
    public function payout_dates_count($user_id) {
        $this->db->select('ifnull(count(date(created_at)),0) as record_count');
        $this->db->from('tbl_income_wallet');
        $this->db->where(['amount >' => 0,'user_id' => $user_id]);
        $this->db->group_by('date(created_at)');
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }

    public function get_incomes($table, $where, $select) {
        $this->db->select($select);
        $this->db->group_by('type');
        $query = $this->db->get_where($table, $where);
        $res = $query->result_array();
        return $res;
    }
}