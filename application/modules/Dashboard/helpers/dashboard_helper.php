<?php
if (!function_exists('pr')) {
    function pr($array, $die = false) {
        echo'<pre>';
        print_r($array);
        echo'</pre>';
        if ($die)
            die();
    }
}
if (!function_exists('is_logged_in')) {
    function is_logged_in() {
        $ci = & get_instance();
        $ci->load->library('session');
        if (isset($ci->session->userdata['user_id'])) {
            return true;
        } else {
            return false;
        }
    }
}
if (!function_exists('userinfo')) {
    function userinfo() {
        $ci = & get_instance();
        $ci->load->model('dashboard_model');
        $userdetails = $ci->dashboard_model->get_single_object('tbl_users', array('user_id' => $ci->session->userdata['user_id']), '*');
        return $userdetails;
    }
}
if (!function_exists('income_transaction')) {
    function income_transaction($incomeArr) {
        $ci = & get_instance();
        $ci->load->model('dashboard_model');
        $ci->Dashboard_model->add('tbl_income_wallet', $incomeArr);
        $ci->Dashboard_model->update_wallet('tbl_users',['user_id' => $incomeArr['user_id']],'income' ,'income + '.$incomeArr['amount']);
    }
}
if (!function_exists('fund_transaction')) {
    function fund_transaction($incomeArr) {
        $ci = & get_instance();
        $ci->load->model('dashboard_model');
        $ci->Dashboard_model->add('tbl_wallet', $incomeArr);
        $ci->Dashboard_model->update_wallet('tbl_users',['user_id' => $incomeArr['user_id']],'wallet_amount' ,'wallet_amount + '.$incomeArr['amount']);
    }
}

if (!function_exists('get_income_name')) {

    function get_income_name($income_name) {
        $incomes = array(
            'direct_income'=> 'Direct Refferal Bonus',
            'level_income'=> 'Team Refferal Bonus',
            'pool_income'=> 'Level Achievement Bonus',
            'pin_generation' => 'Epin Generation Deduction',
            'fund_generation' => 'Fund Generation',
            'withdraw_request' => 'Withdraw Request',
            'withdraw_reject' => 'Withdraw Reject',
        );
        // return array_search($income_name, $incomes);
        return $incomes[$income_name];
    }
}
if (!function_exists('incomes')) {

    function incomes() {
        $incomes = array(
            'direct_income'=> 'Direct Refferal Bonus',
            'level_income'=> 'Team Refferal Bonus',
            'pool_income'=> 'Level Achievement Bonus',
        );
        return $incomes;
    }
}

if(!function_exists('calculate_incomes')){
    function calculate_incomes($incomeArr){
        $incomeNames = array_keys(incomes());
        $incomeVal = array();
        foreach($incomeNames as $key => $name){
            foreach($incomeArr as $k => $i){
                if($i['type'] == $name){
                    $incomeVal[ $i['type']] = $i['income'];
                    break;
                }else{
                    $incomeVal[$name] = 0;
                }
            }
        }
        return $incomeVal;
    }
}