<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
        content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title><?php echo title;?></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="<?php echo base_url('Assets/css/sidebar.css');?>" rel="stylesheet">
</head>

<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-4">
                <div class="text-center">
                    <img src="<?php echo logo;?>" class="img-responsive" style="width:100%"/>
                </div>
                <form method="post" accept-charset="utf-8">
                    <div class="">
                        <div class="">
                            <?php echo form_open();?>
                                <h3 class="text-danger"><?php echo $this->session->flashdata('message');?></h3>
                                <div class="form-group">
                                    <label>Username or Email:</label>
                                    <input type="text" name="user_id" value="" class="form-control"
                                        placeholder="User ID  or Email Address" required="true" />
                                    <span class="text-danger"><?php echo form_error('user_id');?></span>
                                </div>
                                <div class="form-group">
                                    <label>Password:</label>
                                    <input type="password" name="password" value="" class="form-control"
                                        placeholder="Enter Your Password" required="true" />
                                    <span class="text-danger"><?php echo form_error('password');?></span>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary" name="Submit" value="Login">Sign in
                                    </button>
                                </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>