<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?></h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Admin');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Admin/Withdraw/index/0')?>">Withdraw</a></li>
                        <li class="breadcrumb-item active"><?php echo $header;?></li>
                    </ul>
                </div>
                <h3 class="text-danger"><?php echo $this->session->flashdata('message');?></h3>
                <div class="row">
                    <div class="col-4">
                        <ul class="list-group">
                            <li class="list-group-item active">User Details</li>
                            <li class="list-group-item">User Id : <?php echo $user['user_id']?></li>
                            <li class="list-group-item">User Name : <?php echo $user['name']?></li>
                            <li class="list-group-item">Sponser ID : <?php echo $user['sponser_id']?></li>
                            <li class="list-group-item">Phone : <?php echo $user['phone']?></li>
                            <li class="list-group-item">Email : <?php echo $user['email']?></li>
                            <li class="list-group-item">Package : <?php echo $user['package_amount']?></li>
                            <li class="list-group-item">Activation Date : <?php echo $user['topup_date']?></li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="list-group">
                            <li class="list-group-item active">Bank Details</li>
                            <li class="list-group-item">Bank Name : <?php echo $bank['bank_name']?></li>
                            <li class="list-group-item">Bank Account Name : <?php echo $bank['account_holder_name']?></li>
                            <li class="list-group-item">Account Number: <?php echo $bank['bank_account_number']?></li>
                            <li class="list-group-item">IFSC Code : <?php echo $bank['ifsc_code']?></li>
                            <li class="list-group-item">PAN Number : <?php echo $bank['pan']?></li>
                            <li class="list-group-item">Aadhar Number : <?php echo $bank['aadhar']?></li>
                            <li class="list-group-item">KYC Status : 
                                <?php 
                                if($bank['kyc_status'] == 0)
                                    echo'<span class="text-info">Pending</span>';
                                elseif($bank['kyc_status'] == 1)
                                    echo'<span class="text-primary">Applied</span>';
                                elseif($bank['kyc_status'] == 2)
                                    echo'<span class="text-success">Approved</span>';
                                elseif($bank['kyc_status'] == 3)
                                    echo'<span class="text-danger">Rejected</span>';
                                ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="list-group">
                            <li class="list-group-item active">Withdraw Details</li>
                            <li class="list-group-item">Total Amount : <?php echo currency . $withdraw['amount']?></li>
                            <li class="list-group-item">TDS : <?php echo currency . $withdraw['tds']?></li>
                            <li class="list-group-item">Admin Charges : <?php echo currency . $withdraw['admin_charges']?></li>
                            <li class="list-group-item">Payable Amount : <?php echo currency . $withdraw['payable_amount']?></li>
                           
                            <li class="list-group-item">Withdraw Status : 
                                <?php 
                                if($withdraw['status'] == 0)
                                    echo'<span class="text-info">Pending</span>';
                                elseif($withdraw['status'] == 1)
                                    echo'<span class="text-success">Approved</span>';
                                elseif($withdraw['status'] == 2)
                                echo'<span class="text-danger">Rejected</span>';
                                ?>
                            </li>
                            <?php
                            if($withdraw['status'] == 0){
                                echo '<li class="list-group-item">';
                                echo form_open();
                                echo' Remarks : <input type="text" name="remark" class="form-control"><br>';
                                echo'<button class="btn btn-success" name="status" value="1">Approve</button>';
                                echo'<button class="btn btn-danger" name="status" value="2">Reject</button></li>';
                                echo form_close();
                                echo '</li>';
                            }else{
                                echo '<li class="list-group-item">Remarks :'.$withdraw['remark'].'</li>';
                                
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <hr>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>