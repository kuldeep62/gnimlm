<?php $this->load->view('header');?>
<main class="page-content">
    <div class="container-fluid">
        <h2><?php echo $header;?> (<?php echo $total_records;?>)</h2>
        <hr>
        <div class="row">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('Admin');?>">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url('Admin/Users/')?>">Users</a></li>
                <li class="breadcrumb-item active">All Users</li>
            </ul>
        </div>
        <form method="GET" action="<?php echo base_url('Admin/Users/PaidUsers')?>">
            <div class="row">
                <div class="col-sm-3">
                    <input type="date" name="start_date" class="form-control float-right"
                        value="<?php echo $start_date;?>">
                </div>
                    <div class="col-sm-3">
                    <input type="date" name="end_date" class="form-control float-right"
                        value="<?php echo $end_date;?>">
                </div>
                <div class="col-sm-2">
                    <select class="form-control" name="type">
                        <option value="name" <?php echo $type == 'name' ? 'selected' : '';?>>
                            Name</option>
                        <option value="user_id" <?php echo $type == 'user_id' ? 'selected' : '';?>>
                            User ID</option>
                        <option value="phone" <?php echo $type == 'phone' ? 'selected' : '';?>>Phone
                        </option>
                        <option value="sponser_id"
                            <?php echo $type == 'sponser_id' ? 'selected' : '';?>>Sponser ID
                        </option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <input type="text" name="value" class="form-control float-right"
                        value="<?php echo $value;?>" placeholder="Search">
                </div>
                <div class="col-sm-2">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-light"><i class="fas fa-search"></i></button>
                        <a class="btn btn-light" href="<?php echo base_url('Admin/Users/ExportUsers/');?>"><i class="fas fa-download"></i></a>
                    </div>
                </div>
            </div>
        </form>
        <hr>
        <div class="row">
            <table class="table table-hover" id="">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User ID</th>
                        <th>Name</th>
                        <th>Sponser ID</th>
                        <th>Phone</th>
                        <th>Pacakge Amount</th>
                        <th>Directs</th>
                        <th>Joining Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = $segament + 1;
                    foreach ($records as $key => $record) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $record['user_id']; ?></td>
                            <td><?php echo $record['name']; ?></td>
                            <td><?php echo $record['sponser_id']; ?></td>
                            <td><?php echo $record['phone']; ?></td>
                            <td><?php echo $record['package_amount']; ?></td>
                            <td><?php echo $record['directs']; ?></td>
                            <td><?php echo $record['created_at']; ?></td>
                            <td>
                                <a href="<?php echo base_url('Admin/Users/user_login/'.$record['user_id']);?>" target="_blank">Login </a>/ 
                                <a href="<?php echo base_url('Admin/Users/view/'.$record['user_id']);?>">View </a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>

                </tbody>
            </table>
            <?php
            echo $this->pagination->create_links();
            ?>
        </div>
    </div>

</main>
</div>
<?php $this->load->view('footer');?>