<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2>Send Fund</h2>
                <hr>
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-danger"><?php echo $this->session->flashdata('message'); ?></h3>
                        <?php echo form_open('',array('id' => 'walletForm'));?>
                        <div class="form-group">
                            <label>User ID</label>
                            <input type="text" class="form-control" name="user_id" value="<?php echo set_value('user_id');?>" id="user_id" placeholder="User ID"/>
                            <span class="text-danger" id="UserError"><?php echo form_error('user_id')?></span>
                        </div>
                        <div class="form-group">
                            <label>Amount</label>
                            <input type="number" class="form-control" name="amount" placeholder="Amount" value="<?php echo set_value('amount');?>"/>
                            <span class="text-danger"><?php echo form_error('amount')?></span>
                        </div>
                        <div class="form-group">
                            <button type="subimt" name="save" class="btn btn-success" />Send</button>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>
<script>
$(document).on('blur','#user_id',function(){
    var url = '<?php echo base_url('Admin/Epins/CheckUser/')?>'+$(this).val();
    $.get(url,function(res){
        if(res.success){
            $('#UserError').html(res.user.name)
        }else{
            $('#UserError').html(res.message);
        }
    },'json');
})
</script>