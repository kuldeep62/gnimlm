<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> (<?php echo currency . $total_amount;?>)</h2>
                <hr>
                <div class="row">
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User ID</th>
                                <th>Amount</th>
                                <th>TDS</th>
                                <th>Admin Charges</th>
                                <th>Payable Amount</th>
                                <th>Status</th>
                                <th>Request Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                <td><?php echo ($key + 1) ?></td>
                                    <td><?php echo $record['user_id']; ?></td>
                                    <td><?php echo currency . $record['amount']; ?></td>
                                    <td><?php echo currency .$record['tds']; ?></td>
                                    <td><?php echo currency .$record['admin_charges']; ?></td>
                                    <td><?php echo currency .$record['payable_amount']; ?></td>
                                    <td><?php 
                                        if($record['status'] == 0){
                                            echo'<span class="text-primary">Pending</span>';
                                        }elseif($record['status'] == 1){
                                            echo'<record class="text-success">Approved</span>';
                                        }elseif($record['status'] == 2){
                                            echo'<span class="text-danger">Rejected</span>';
                                        }
                                    ?></td>
                                    <td><?php echo $record['created_at']; ?></td>
                                    <td><a href="<?php echo base_url('Admin/Withdraw/view/'.$record['id']);?>" class="btn btn-info">View</a></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                    echo $this->pagination->create_links();
                    ?>
                </div>
            </div>

        </main>
        <!-- page-content" -->
    </div>
    <?php $this->load->view('footer');?>