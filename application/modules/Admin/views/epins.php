<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> (<?php echo $total_records;?>)</h2>
                <hr>
                <div class="row">
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User ID</th>
                                <th>Epin</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Used For</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $record['user_id']; ?></td>
                                    <td><?php echo $record['epin']; ?></td>
                                    <td><?php echo currency . ' ' .$record['amount']; ?></td>
                                    <td><?php echo $record['status']; ?></td>
                                    <td><?php echo $record['used_for']; ?></td>
                                    <td><?php echo $record['created_at']; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                    echo $this->pagination->create_links();
                    ?>
                </div>
            </div>

        </main>
        <!-- page-content" -->
    </div>
    <?php $this->load->view('footer');?>