<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> </h2>
                <hr>
                <div class="row">
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>UserID</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Status</th>
                                <th>Remark</th>
                                <th>Action</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                <td><?php echo ($key + 1) ?></td>
                                    <td><?php echo $record['user_id']; ?></td>
                                    <td><?php echo  $record['subject']; ?></td>
                                    <td><?php echo  $record['message']; ?></td>
                                    <td><?php 
                                        if($record['status'] == 0){
                                            echo'<span class="text-primary">Pending</span>';
                                        }elseif($record['status'] == 1){
                                            echo'<record class="text-success">Approved</span>';
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo  $record['remark']; ?></td>
                                    <td>
                                        <?php
                                        if($record['status'] == 0){
                                            echo'<a href="'.base_url('Admin/Support/Ticket/'.$record['id']).'">View</a>';
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $record['created_at']; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                    echo $this->pagination->create_links();
                    ?>
                </div>
            </div>

        </main>
        <!-- page-content" -->
    </div>
    <?php $this->load->view('footer');?>