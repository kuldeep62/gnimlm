<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2>News Create</h2>
                <hr>
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-danger"><?php echo $this->session->flashdata('message'); ?></h3>
                        <?php echo form_open('',array());?>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title" value="<?php echo set_value('title');?>" placeholder="News Title"/>
                            <span class="text-danger" id="UserError"><?php echo form_error('title')?></span>
                        </div>
                        <div class="form-group">
                            <label>News</label>
                            <textarea class="form-control" name="news" placeholder="News"><?php echo set_value('news');?></textarea>
                            <span class="text-danger"><?php echo form_error('news')?></span>
                        </div>
                        <div class="form-group">
                            <button type="subimt" name="save" class="btn btn-success" />Create</button>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>