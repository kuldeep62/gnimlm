<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2>Fund Request</h2>
                <hr>
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-danger"><?php echo $this->session->flashdata('message');?></h3>
                        <?php echo form_open(); ?>
                            <div class="form-group">
                                <label>User Id</label>
                                <input type="text" id="userId" name="user_id" class="form-control" value="<?php echo set_value('user_id')?>" placeholder="User ID"/>
                                <label class="text-danger" id="UserError"><?php echo form_error('user_id');?></label>
                            </div>
                            <div class="form-group">
                                <label> Amount</label>
                                <?php echo form_input(array('type' => 'text', 'class' => 'form-control', 'value' =>  $request['amount'])); ?>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Proof :</label>
                                <div class="col-lg-9 col-xl-6">
                                    <img src="<?php echo $request['image'] != '' ? base_url('uploads/' . $request['image']) : base_url('classic/logo.png'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Remark :</label>
                                <div class="col-lg-9 col-xl-6">
                                    <?php
                                    echo form_textarea(array('rows' => '2', 'class' => 'form-control', 'value' => $request['remarks'], 'name' => 'remarks'));
                                    ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Status :</label>
                                <div class="col-lg-9 col-xl-6">
                                    <?php
                                    if ($request['status'] == 0) {
                                        echo'<span class="btn btn-primary">Pending</span>';
                                    } elseif ($request['status'] == 1) {
                                        echo'<span class="btn btn-success">Approved</span>';
                                    } elseif ($request['status'] == 2) {
                                        echo'<span class="btn btn-danger">Rejected</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($request['status'] == 0) {
                                    echo form_input(array('type' => 'submit', 'class' => 'btn btn-success', 'value' => 'Approve', 'name' => 'status'));
                                    echo'&nbsp;';
                                    echo form_input(array('type' => 'submit', 'class' => 'btn btn-danger', 'value' => 'Reject', 'name' => 'status'));
                                } elseif ($request['status'] == 2) {
                                    echo form_input(array('type' => 'submit', 'class' => 'btn btn-success', 'value' => 'Approve', 'name' => 'status'));
                                }
                                ?>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>
<script>
$(document).on('blur','#userId',function(){
    var url = '<?php echo base_url('Admin/Epins/CheckUser/')?>'+$(this).val();
    $.get(url,function(res){
        if(res.success){
            $('#UserError').html(res.user.name)
        }else{
            $('#UserError').html(res.message);
        }
    },'json');
})
</script>