<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2>Popup</h2>
                <hr>
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-danger"><?php echo $this->session->flashdata('message'); ?></h3>
                        <?php echo form_open_multipart();?>
                        <div class="form-group">
                            <label>Caption</label>
                            <input type="text" class="form-control" name="caption" value="<?php echo set_value('caption');?>" id="user_id" placeholder="Caption"/>
                            <span class="text-danger"><?php echo form_error('caption')?></span>
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <select class="form-control" name="type" id="selectType"/>
                                <option value="image">Image</option>
                                <option value="video">Video</option>
                            </select>
                        </div>
                        <div class="form-group" id="imageField">
                            <label>Media</label>
                            <?php echo form_input(array('class' => 'form-control', 'type' => 'file', 'name' => 'media'));?>
                            <span class="text-danger"><?php echo form_error('media')?></span>
                        </div>
                        <div class="form-group" id="videoField" style="display:none;">
                            <label>VIdeo link</label>
                            <?php echo form_input(array('class' => 'form-control', 'type' => 'text', 'name' => 'media'));?>
                            <span class="text-danger"><?php echo form_error('media')?></span>
                        </div>
                        <div class="form-group">
                            <button type="subimt" name="save" class="btn btn-success" />Update</button>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>
<script>
  $(document).on('change','#selectType',function(){
        $('#imageField').toggle();
        $('#videoField').toggle();
  })
</script>