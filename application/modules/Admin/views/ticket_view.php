<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?></h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Admin');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Admin/Withdraw/index/0')?>">Withdraw</a></li>
                        <li class="breadcrumb-item active"><?php echo $header;?></li>
                    </ul>
                </div>
                <h3 class="text-danger"><?php echo $this->session->flashdata('message');?></h3>
                <div class="row">
                    <div class="col-4">
                        <ul class="list-group">
                            <li class="list-group-item active">User Details</li>
                            <li class="list-group-item">User Id : <?php echo $user['user_id']?></li>
                            <li class="list-group-item">User Name : <?php echo $user['name']?></li>
                            <li class="list-group-item">Sponser ID : <?php echo $user['sponser_id']?></li>
                            <li class="list-group-item">Phone : <?php echo $user['phone']?></li>
                            <li class="list-group-item">Email : <?php echo $user['email']?></li>
                            <li class="list-group-item">Package : <?php echo $user['package_amount']?></li>
                            <li class="list-group-item">Activation Date : <?php echo $user['topup_date']?></li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="list-group">
                            <li class="list-group-item active">Ticket Details</li>
                            <li class="list-group-item">Subject : <?php echo $ticket['subject']?></li>
                           
                            <li class="list-group-item">Ticket Status : 
                                <?php 
                                if($ticket['status'] == 0)
                                    echo'<span class="text-info">Pending</span>';
                                elseif($ticket['status'] == 1)
                                    echo'<span class="text-success">Resolved</span>';
                                ?>
                            </li>
                            <?php
                            if($ticket['status'] == 0){
                                echo '<li class="list-group-item">';
                                echo form_open();
                                echo' Remarks : <textarea name="remark" class="form-control"></textarea><br>';
                                echo'<button class="btn btn-success" name="status" value="1">Resolve</button>';
                                echo form_close();
                                echo '</li>';
                            }else{
                                echo '<li class="list-group-item">Remarks :'.$ticket['remark'].'</li>';
                                
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <hr>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>