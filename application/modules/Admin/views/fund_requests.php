<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> (<?php echo $total_fund;?>)</h2>
                <hr>
                <div class="row">
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User ID</th>
                                <th>Amount</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Payment Method</th>
                                <th>Remark</th>
                                <th>CreatedAt</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                <td><?php echo ($key + 1) ?></td>
                                    <td><?php echo $record['user_id']; ?></td>
                                    <td><?php echo $record['amount']; ?></td>
                                    <td><img src="<?php echo base_url('uploads/' . $record['image']); ?>" height="100px" width="100px"></td>
                                    <td><?php 
                                        if($record['status'] == 0){
                                            echo'<span class="btn btn-primary">Pending</span>';
                                        }elseif($record['status'] == 1){
                                            echo'<record class="btn btn-success">Approved</span>';
                                        }elseif($request['status'] == 2){
                                            echo'<span class="btn btn-danger">Rejected</span>';
                                        }
                                    ?></td>
                                    <td><?php echo $record['payment_method']; ?></td>
                                    <td><?php echo $record['remarks']; ?></td>
                                    <td><?php echo $record['created_at']; ?></td>
                                    <td><a href="<?php echo base_url('Admin/Fund/update_fund_request/'.$record['id']);?>" class="btn btn-info">View</a></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                    echo $this->pagination->create_links();
                    ?>
                </div>
            </div>

        </main>
        <!-- page-content" -->
    </div>
    <?php $this->load->view('footer');?>