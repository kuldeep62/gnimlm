<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?></h2>
                <hr>
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-danger"><?php echo $this->session->flashdata('message');?></h3>
                        <?php echo form_open();?>
                            <div class="form-group">
                                <label>Caption</label>
                                <input type="text" name="caption" class="form-control" value="<?php echo $withdraw_control['caption'];?>"/>
                                <label class="text-danger" id="UserError"><?php echo form_error('caption');?></label>
                            </div>
                            <div class="form-group">
                                <label>Start Day</label>
                                <select class="form-control" name="start_day">
                                    <?php
                                    foreach($days as $key => $day)
                                        echo'<option value="'.$key.'" '.($key == $withdraw_control['start_day'] ? 'selected' : '').'>'.$day.'</option>';
                                    ?>
                                    <label class="text-danger"><?php echo form_error('start_day');?></label>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>End Day</label>
                                <select class="form-control" name="end_day">
                                    <?php
                                    foreach($days as $key => $day)
                                        echo'<option value="'.$key.'" '.($key == $withdraw_control['end_day'] ? 'selected' : '').'>'.$day.'</option>';
                                    ?>
                                    <label class="text-danger"><?php echo form_error('end_day');?></label>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Start Time</label>
                                <input type="time" name="start_time" class="form-control" value="<?php echo $withdraw_control['start_time']?>"/>
                                <label class="text-danger"><?php echo form_error('start_time');?></label>
                            </div>
                            <div class="form-group">
                                <label>End Time</label>
                                <input type="time" name="end_time" class="form-control" value="<?php echo $withdraw_control['end_time']?>"/>
                                <label class="text-danger"><?php echo form_error('end_time');?></label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success pull-right">Create</button>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>
<script>
$(document).on('blur','#userId',function(){
    var url = '<?php echo base_url('Admin/Epins/CheckUser/')?>'+$(this).val();
    $.get(url,function(res){
        if(res.success){
            $('#UserError').html(res.user.name)
        }else{
            $('#UserError').html(res.message);
        }
    },'json');
})
</script>