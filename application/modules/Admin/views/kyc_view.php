<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2>Kyc Details</h2>
                <hr>
                <div class="row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Admin');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Admin/KYC/index/0')?>">KYC</a></li>
                        <li class="breadcrumb-item active">Kyc Details</li>
                    </ul>
                </div>
                <h3 class="text-danger"><?php echo $this->session->flashdata('message');?></h3>
                <div class="row">
                    <div class="col-6">
                        <ul class="list-group">
                            <li class="list-group-item active">User Details</li>
                            <li class="list-group-item">User Id : <?php echo $user['user_id']?></li>
                            <li class="list-group-item">User Name : <?php echo $user['name']?></li>
                            <li class="list-group-item">Sponser ID : <?php echo $user['sponser_id']?></li>
                            <li class="list-group-item">Phone : <?php echo $user['phone']?></li>
                            <li class="list-group-item">Email : <?php echo $user['email']?></li>
                            <li class="list-group-item">Package : <?php echo $user['package_amount']?></li>
                            <li class="list-group-item">Registration Date : <?php echo $user['created_at']?></li>
                            <li class="list-group-item">Activation Date : <?php echo $user['topup_date']?></li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="list-group">
                            <li class="list-group-item active">Bank Details</li>
                            <li class="list-group-item">Bank Name : <?php echo $bank['bank_name']?></li>
                            <li class="list-group-item">Bank Account Name : <?php echo $bank['account_holder_name']?></li>
                            <li class="list-group-item">Account Number: <?php echo $bank['bank_account_number']?></li>
                            <li class="list-group-item">IFSC Code : <?php echo $bank['ifsc_code']?></li>
                            <li class="list-group-item">PAN Number : <?php echo $bank['pan']?></li>
                            <li class="list-group-item">Aadhar Number : <?php echo $bank['aadhar']?></li>
                            <li class="list-group-item">KYC Status : 
                                <?php 
                                if($bank['kyc_status'] == 0)
                                    echo'<span class="text-info">Pending</span>';
                                elseif($bank['kyc_status'] == 1)
                                    echo'<span class="text-primary">Applied</span>';
                                elseif($bank['kyc_status'] == 2)
                                    echo'<span class="text-success">Approved</span>';
                                elseif($bank['kyc_status'] == 3)
                                    echo'<span class="text-danger">Rejected</span>';
                                ?>
                            </li>
                            <li class="list-group-item">
                            <?php
                            echo form_open();
                            echo'<button class="btn btn-success" name="status" value="2">Approve</button>';
                            echo'<button class="btn btn-danger" name="status" value="3">Reject</button>';
                            echo form_close();
                            ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card">
                            <span class="text-center text-primary">Aadhar Card</span>
                            <a href="<?php echo base_url('uploads/'.$bank['id_proof']);?>"><img src="<?php echo base_url('uploads/'.$bank['id_proof']);?>"></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <span class="text-center text-primary">Aadhar Card Back</span>
                            <a href="<?php echo base_url('uploads/'.$bank['id_proof']);?>"><img src="<?php echo base_url('uploads/'.$bank['id_proof2']);?>"></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <span class="text-center text-primary">Pan Card</span>
                            <a href="<?php echo base_url('uploads/'.$bank['id_proof']);?>"><img src="<?php echo base_url('uploads/'.$bank['id_proof3']);?>"></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <span class="text-center text-primary">Bank Passbook / Cancel Check</span>
                            <a href="<?php echo base_url('uploads/'.$bank['id_proof']);?>"><img src="<?php echo base_url('uploads/'.$bank['id_proof4']);?>"></a>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>