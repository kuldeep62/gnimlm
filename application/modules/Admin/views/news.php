<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> </h2>
                <hr>
                <div class="row">
                    <a href="<?php echo base_url('Admin/Settings/NewsCreate');?>" class="btn btn-success">Create News</a>
                    <h2 class="text-danger"><?php echo $this->session->flashdata('message');?></h2>
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>News</th>
                                <th>Action</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $segament + 1;
                            foreach ($records as $key => $record) {
                                ?>
                                <tr>
                                <td><?php echo ($key + 1) ?></td>
                                    <td><?php echo $record['title']; ?></td>
                                    <td><?php echo  $record['news']; ?></td>
                                    <td>
                                        <a href="<?php echo  base_url('Admin/Settings/NewsEdit/'.$record['id']); ?>">Edit</a><br>
                                        <a href="<?php echo  base_url('Admin/Settings/NewsDelete/'.$record['id']); ?>">Delete</a>
                                    </td>
                                    <td><?php echo $record['created_at']; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                    echo $this->pagination->create_links();
                    ?>
                </div>
            </div>

        </main>
        <!-- page-content" -->
    </div>
    <?php $this->load->view('footer');?>