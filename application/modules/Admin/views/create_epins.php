<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2>Create Epins</h2>
                <hr>
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-danger"><?php echo $this->session->flashdata('message');?></h3>
                        <?php echo form_open();?>
                            <div class="form-group">
                                <label>User Id</label>
                                <input type="text" id="userId" name="user_id" class="form-control" value="<?php echo set_value('user_id')?>" placeholder="User ID"/>
                                <label class="text-danger" id="UserError"><?php echo form_error('user_id');?></label>
                            </div>
                            <div class="form-group">
                                <label>Pin Amount</label>
                                <select class="form-control" name="pin_amount">
                                    <?php
                                    foreach($pin_amount as $key => $p_amount)
                                        echo'<option value="'.$p_amount['price'].'">'.$p_amount['price'].'</option>';
                                    ?>
                                    <label class="text-danger"><?php echo form_error('pin_amount');?></label>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>No. of Epins</label>
                                <input type="number" name="pin_count" class="form-control" value="<?php echo set_value('epins')?>"/>
                                <label class="text-danger"><?php echo form_error('pin_count');?></label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success pull-right">Create</button>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>

        </main>
    </div>
<?php $this->load->view('footer');?>
<script>
$(document).on('blur','#userId',function(){
    var url = '<?php echo base_url('Admin/Epins/CheckUser/')?>'+$(this).val();
    $.get(url,function(res){
        if(res.success){
            $('#UserError').html(res.user.name)
        }else{
            $('#UserError').html(res.message);
        }
    },'json');
})
</script>