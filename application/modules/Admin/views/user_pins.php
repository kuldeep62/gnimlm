<?php $this->load->view('header');?>
        <main class="page-content">
            <div class="container-fluid">
                <h2><?php echo $header;?> (<?php echo count($user_pins);?>)</h2>
                <hr>
                <div class="row">
                    <table class="table table-hover" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User ID</th>
                                <th>Pin Balance</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($user_pins as $key => $user_pin) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $user_pin['user_id']; ?></td>
                                    <td><?php  echo $user_pin['pin_count']; ?></td>
                                    <td><a href="<?php  echo base_url('Admin/Epins/UserPinView/'.$user_pin['user_id']); ?>">View</a></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                    //echo $this->pagination->create_links();
                    ?>
                </div>
            </div>

        </main>
        <!-- page-content" -->
    </div>
    <?php $this->load->view('footer');?>