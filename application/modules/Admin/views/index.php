<?php $this->load->view('header');?>
<link rel="stylesheet" type="text/css" href="https://www.chartjs.org/samples/latest/style.css">
<script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.4.0/dist/chartjs-plugin-datalabels.min.js"></script>

<main class="page-content bg-light">
    <div class="container-fluid">
        <h2>Dashboard</h2>
        <hr>
        <h4 class="text-center"> Members Area</h4>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Total Members <b><?php echo $members;?></b> &nbsp;&nbsp;&nbsp;
                            <a href="<?php echo base_url('Admin/Users');?>" class="btn btn-primary btn-sm">View</a>
                        </h6>
                    </div>
                    <div class="card-body text-center">
                        <h6 class="card-title">Paid Members <b><?php echo $paid_memebers;?></b> &nbsp;&nbsp;&nbsp;
                            <a href="<?php echo base_url('Admin/PaidUsers');?>" class="btn btn-primary btn-sm">View</a>
                        </h6>
                    </div>
                    <div class="card-body text-center">
                        <h6 class="card-title">Today Joinings <b><?php echo $today_joinings;?></b> &nbsp;&nbsp;&nbsp;
                            <a href="<?php echo base_url('Admin/TodayJoinings');?>" class="btn btn-primary btn-sm">View</a>
                        </h6>
                    </div>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <canvas id="curve_chart"></canvas>
                    </div>
                   
                </div>
            </div>           
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-header">
                        <h3 class="card-title">Payout Reports</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <canvas id="chart-0"></canvas>
                            </div>
                            <div class="col-md-4">
                                <ul class="list-group">
                                    <li class="list-group-item bg-info">Payout</li>
                                    <?php
                                    $total_payout = 0;
                                    foreach($incomes as $key => $income){
                                        $total_payout = $income + $total_payout;
                                        echo'<li class="list-group-item">'.get_income_name($key).' <a href="'.base_url('Admin/Payout/income/'.$key).'" class="btn btn-primary btn-sm"  style="float:right">'.currency.$income.'</a></li>';
                                    }
                                    ?>
                                    <li class="list-group-item">Total Payout<a href="<?php echo base_url('Admin/Payout/');?>"  class="btn btn-primary btn-sm" style="float:right"><?php echo currency.$total_payout ;?></a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h4 class="text-center"> Fund Usage</h4>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Total Fund</h6>
                        <h5><?php echo currency.$total_generated_fund;?></h5>
                        <a href="<?php echo base_url('Admin/Fund/index');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Used Fund</h6>
                        <h5><?php echo currency.abs($used_fund);?></h5>
                        <a href="<?php echo base_url('Admin/Fund/index');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Available Fund IN Members  Account</h6>
                        <h5><?php echo currency . ($total_generated_fund + $used_fund);?></h5>
                        <a href="<?php echo base_url('Admin/Fund/index');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h4 class="text-center"> Epins</h4>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Total Epins</h6>
                        <h5><?php echo $total_epins;?></h5>
                        <a href="<?php echo base_url('Admin/EPins/UsersEPins');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Used Epins</h6>
                        <h5><?php echo $used_epins;?></h5>
                        <a href="<?php echo base_url('Admin/Epins/index/1');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">UnUsed Epins</h6>
                        <h5><?php echo $unused_epins;?></h5>
                        <a href="<?php echo base_url('Admin/Epins/index/0');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Transferred Epins</h6>
                        <h5><?php echo $transferred_epins;?></h5>
                        <a href="<?php echo base_url('Admin/Epins/index/2');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h4 class="text-center"> Withdraw</h4>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Total Withdrawal Amount</h6>
                        <h5><?php echo currency.$total_withdraw;?></h5>
                        <a href="<?php echo base_url('Admin/Withdraw/index/1');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Pending Withdraw</h6>
                        <h5><?php echo currency.abs($pending_withdraw);?></h5>
                        <a href="<?php echo base_url('Admin/Withdraw/index/0');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Available Fund IN Members  Account</h6>
                        <h5><?php echo currency . $fund_genration;?></h5>
                        <a href="<?php echo base_url('Admin/Withdraw/fund_generation');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h4 class="text-center"> Support Messages</h4>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Total Messages</h6>
                        <h5><?php echo $total_messages;?></h5>
                        <a href="<?php echo base_url('Admin/Support/index/1');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card rounded-0 p-0 shadow-sm">
                    <div class="card-body text-center">
                        <h6 class="card-title">Pending Messages</h6>
                        <h5><?php echo $unsolved_messages;?></h5>
                        <a href="<?php echo base_url('Admin/Support/index/0');?>" class="btn btn-primary btn-sm">View</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>
<?php $this->load->view('footer');?>
<script>
     var data = [{
            data: <?php echo json_encode(array_values($incomes))?>,
            names:<?php echo json_encode($income_names)?>,
            backgroundColor: [
                "#aaaaaa",
                "#333538",
                "#d21243",
                "#B27200",
                "#4b77a9",
                "#5f255f",
                "#B27200",
                "#4b77a9",
                "#5f255f",
            ],
            borderColor: "#fff"
        }];
        
        var options = {
            showAllTooltips: true,
            tooltips: {
                mode: 'label',
                    callbacks: {
                    title: function(key, data) {
                        return data.datasets[0].names[key[0].index];
                    },
                    label: function(key, data) { 
                        return data.datasets[0].data[key.index];
                    },
                },
                enabled: true,
            },
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {         
                  return '<?php echo currency; ?>'+value  ;              
                },
                color: '#fff',
            }
        }
    };
    
    
    var ctx = document.getElementById("chart-0").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: data,
        },
        options: options
    });
        
</script>
<script>

new Chart(document.getElementById("curve_chart"), {
    "type": "line",
    "data": {
        "labels": <?php echo json_encode(array_keys($date_members));?>,
        "datasets": [{
            "label": "Registered Users",
            "data": <?php echo json_encode(array_values($date_members));?>, 
            "fill": true,
            "borderColor": "rgb(75, 192, 192)",
            // "lineTension": 1
        },
        {
            "label": "Activated Users",
            "data": <?php echo json_encode(array_values($date_paid_members));?>, 
            "fill": true,
            "borderColor": "rgb(237, 57, 69)",
            // "lineTension": 1
        }
        
        ]
    },
    "options": {}
});
</script>
<?php echo json_encode(array_values($date_paid_members));?>,
