<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
        content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title><?php echo title;?></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="<?php echo base_url('Assets/css/sidebar.css');?>" rel="stylesheet">
</head>

<body>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand bg-light">
                    <!-- <a href="#"><?php echo title;?></a> -->
                    <img src="<?php echo logo;?>" class="img-responsive" style="width:100%" />
                    <div id="close-sidebar">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-menu">
                    <ul>
                        <li>
                            <a href="<?php echo base_url('Admin');?>">
                                <i class="fa fa-tachometer-alt"></i>
                                <span>Dashboard</span>
                                <!-- <span class="badge badge-pill badge-primary"></span> -->
                            </a>
                        </li>

                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Epins' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-key"></i>
                                <span class="sr-only">Loading...</span>
                                <span>Epins Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Epins' ? 'block' : 'none';?>">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Epins/index/0');?>">Available Pins</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Epins/index/1');?>">Used Pins</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Epins/index/2');?>">Transferred Pins</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Epins/CreateEpins');?>">Generate Epins</a>
                                    </li>
                                    <li>
                                        <a href="#">Epins History</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Users' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Members Managment</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Users' ? 'block' : 'none';?>">
                                <ul>
                                    <li> <a href="<?php echo base_url('Admin/Users');?>">All Members</a></li>
                                    <li><a href="<?php echo base_url('Admin/Users/PaidUsers');?>">Paid Members</a>
                                        <span class="badge badge-pill badge-success">Activated</span>
                                    </li>
                                    <li><a href="<?php echo base_url('Admin/Users/TodayJoinings');?>">Today Joinings</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Payout' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-dollar-sign"></i>
                                <span>Payout Reports</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Payout' ? 'block' : 'none';?>">
                                <ul>
                                    <?php 
                                    $incomes = incomes();
                                    foreach($incomes as $key => $income)
                                        echo'<li><a href="'.base_url('Admin/Payout/Income/'.$key).'">'.$income.'</a></li>';
                                    ?>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Payout/datewise_payout');?>">Payout Summary</a>
                                        <span class="badge badge-pill badge-success">Datewise</span>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Payout');?>">Income Ledgar</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Kyc' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>KYC Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Kyc' ? 'block' : 'none';?>">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Kyc/index/0');?>">Pending KYC</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Kyc/index/1');?>">Applied KYC</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Kyc/index/2');?>">Approved KYC's</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Kyc/index/3');?>">Rejected Requests</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Fund' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Fund Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Fund' ? 'block' : 'none';?>">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Fund/fund_requests/0');?>">Fund Request List</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Fund/fund_requests/1');?>">Approved Fund List</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Fund/fund_requests/2');?>">Rejected Fund List</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Fund/index');?>">Fund History</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Fund/SendFund');?>">Send Fund Personally</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Withdraw' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Withdraw Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Withdraw' ? 'block' : 'none';?>">
                                <ul>
                                    <li><a href="<?php echo base_url('Admin/Withdraw/index/0');?>">Pending Withdraw Request</a></li>
                                    <li><a href="<?php echo base_url('Admin/Withdraw/index/1');?>">Approved Withdraw Request</a></li>
                                    <li><a href="<?php echo base_url('Admin/Withdraw/index/2');?>">Rejected Withdraw Request</a></li>
                                    <li><a href="<?php echo base_url('Admin/Withdraw/withdraw_control/withdraw');?>">Withdraw Control</a></li>
                                    <li><a href="<?php echo base_url('Admin/Withdraw/fund_generation');?>">Fund Generation</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Support' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Support</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Support' ? 'block' : 'none';?>">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Support/index/0');?>">Pending Queries</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Admin/Support/index/1');?>">Solved Queries</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown <?php echo $this->router->fetch_class() == 'Settings' ? 'active' : '';?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>CMS Management</span>
                            </a>
                            <div class="sidebar-submenu" style="display:<?php echo $this->router->fetch_class() == 'Settings' ? 'block' : 'none';?>">
                                <ul>
                                    <li><a href="<?php echo base_url('Admin/Settings/News');?>">News Management</a></li>
                                    <li><a href="<?php echo base_url('Admin/Settings/Popup');?>">Popup Management</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="<?php echo base_url('Admin/logout');?>">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- sidebar-menu  -->
            </div>
        </nav>
        <!-- sidebar-wrapper  -->