<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdraw extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'security','pagination'));
        $this->load->model(array('Main_model'));
        $this->load->helper(array('admin', 'security'));
    }
	public function index($status)
    {
        if (is_admin()) {
            $response = array();
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);

            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }

            if ($status == 0)
                $response['header'] = 'Pending Withdraw Requests';            
            elseif ($status == 1)
                $response['header'] = 'Applied Withdraw Requests';
            elseif ($status == 2)
                $response['header'] = 'Approved Withdraw Requests';
            elseif ($status == 3)
                $response['header'] = 'Rejected Withdraw Requests';
            
            $where['status'] = $status;
            $where['type'] = 'withdraw';

            $config['base_url'] = base_url() . 'Admin/Withdraw/index/' . $status;
            $config['total_rows'] = $this->Main_model->get_sum('tbl_withdraw', $where ,'ifnull(count(id),0) as sum');
            $response['total_amount'] = $this->Main_model->get_sum('tbl_withdraw',['status' => $status], 'ifnull(sum(payable_amount),0) as sum');
            $config['per_page'] = 10;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_withdraw', $where, '*', $config['per_page'], $segment);
            $this->load->view('withdraw_requests', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function view($id){
        if (is_admin()) {
            $response['header'] = 'Withdraw Details';
            $withdraw = $this->Main_model->get_single_record('tbl_withdraw',['id' => $id],'*');
            $response['user'] = $this->Main_model->get_single_record('tbl_users',['user_id' => $withdraw['user_id']],'id,user_id,sponser_id,name,phone,email,package_amount,created_at,topup_date');
            $response['bank'] = $this->Main_model->get_single_record('tbl_user_details',['user_id' => $withdraw['user_id']],'*');
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());                
                if ($data['status'] == 1) {
                    $wArr = array(
                        'status' => 1,
                        'remark' => $data['remark'],
                    );
                    $res = $this->Main_model->update('tbl_withdraw', array('id' => $id), $wArr);
                    if ($res) {
                        $this->session->set_flashdata('message', 'Withdraw request approved');
                    } else {
                        $this->session->set_flashdata('message', 'Error while apporing request');
                    }
                } elseif ($data['status'] == 2) {
                    $wArr = array(
                        'status' => 2,
                        'remark' => $data['remark'],
                    );
                    $res = $this->Main_model->update('tbl_withdraw', array('id' => $id), $wArr);
                    if ($res) {
                        $productArr = array(
                            'user_id' => $withdraw['user_id'],
                            'amount' => $withdraw['amount'],
                            'type' => 'withdraw_reject',
                            'description' => $data['remark'],
                        );
                        income_transaction($productArr) ;
                        $this->session->set_flashdata('message', 'Withdraw request rejected');
                    } else {
                        $this->session->set_flashdata('message', 'Error while apporing rejection');
                    }
                }
            }
            $response['withdraw'] = $this->Main_model->get_single_record('tbl_withdraw',['id' => $id],'*');
            $this->load->view('withdraw_view', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function withdraw_control($type){
        if (is_admin()) {
            $response['header'] = 'Withdraw Control';
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post()); 
                $this->form_validation->set_rules('caption', 'Caption', 'trim|required|xss_clean');
                $this->form_validation->set_rules('start_day', 'Start Day', 'trim|required|xss_clean');               
                $this->form_validation->set_rules('end_day', 'End Day', 'trim|required|xss_clean');               
                $this->form_validation->set_rules('start_time', 'Start Time', 'trim|required|xss_clean');               
                $this->form_validation->set_rules('end_time', 'End Time', 'trim|required|xss_clean');               
                $wArr = array(
                    'caption' => $data['caption'],
                    'start_day' => $data['start_day'],
                    'end_day' => $data['end_day'],
                    'start_time' => $data['start_time'],
                    'end_time' => $data['end_time'],
                );
                $res = $this->Main_model->update('tbl_withdraw_control', array('type' => $type), $wArr);
                if ($res) {
                    $this->session->set_flashdata('message', 'Withdraw Control Updated Successfully');
                } else {
                    $this->session->set_flashdata('message', 'Error while Updating Withdraw Control');
                }
            }
            $response['withdraw_control'] = $this->Main_model->get_single_record('tbl_withdraw_control',['type' => $type],'*');
            $response['days'] = [
                1 => 'Monday',
                2 => 'Thuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday',
            ];
            $this->load->view('withdraw_control', $response);
        }else{
            redirect('Admin/login');
        }
    }
    public function fund_generation()
    {
        if (is_admin()) {
            $response = array();
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);

            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }
            
            $response['header'] = 'Fund Generation';
            $where['type'] = 'fund_generation';

            $config['base_url'] = base_url() . 'Admin/Withdraw/fund_generation/';
            $config['total_rows'] = $this->Main_model->get_sum('tbl_withdraw', $where ,'ifnull(count(id),0) as sum');
            $response['total_amount'] = $this->Main_model->get_sum('tbl_withdraw',$where, 'ifnull(sum(payable_amount),0) as sum');
            $config['per_page'] = 10;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_withdraw', $where, '*', $config['per_page'], $segment);
            $this->load->view('withdraw_requests', $response);
        } else {
            redirect('Admin/login');
        }
    }
}
