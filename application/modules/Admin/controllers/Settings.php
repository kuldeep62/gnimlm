<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'security','pagination'));
        $this->load->model(array('Main_model'));
        $this->load->helper(array('admin', 'security'));
    }
	public function news()
    {
        if (is_admin()) {
            $response = array();
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);

            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }

            $response['header'] = 'News';            
            
            

            $config['base_url'] = base_url() . 'Admin/Settings/news/';
            $config['total_rows'] = $this->Main_model->get_sum('tbl_news', $where ,'ifnull(count(id),0) as sum');
            
            $config['per_page'] = 10;
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_news', $where, '*', $config['per_page'], $segment);
            $this->load->view('news', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function NewsCreate(){
        if (is_admin()) {
            $response['header'] = 'Create News';
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
                $this->form_validation->set_rules('news', 'News', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {

                    $data = $this->security->xss_clean($this->input->post());                
                    $wArr = array(
                        'title' => $data['title'],
                        'news' => $data['news'],
                    );
                    $res = $this->Main_model->add('tbl_news', $wArr);
                    if ($res) {
                        $this->session->set_flashdata('message', 'News Created Successfully');
                    } else {
                        $this->session->set_flashdata('message', 'Error while Creating News');
                    }
                }
            }
            $this->load->view('news_create', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function NewsEdit($id){
        if (is_admin()) {
            $response['header'] = 'News Edit';
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());   
                $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
                $this->form_validation->set_rules('news', 'News', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {

                    $wArr = array(
                        'title' => $data['title'],
                        'news' => $data['news'],
                    );
                    $res = $this->Main_model->update('tbl_news', array('id' => $id), $wArr);
                    if ($res) {
                        $this->session->set_flashdata('message', 'News Updated Successfully');
                    } else {
                        $this->session->set_flashdata('message', 'Error while Updating News');
                    }
                }             
            }
            $response['news'] = $this->Main_model->get_single_record('tbl_news',['id' => $id],'*');
            $this->load->view('news_edit', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function NewsDelete($id){
        if (is_admin()) {
            $res = $this->Main_model->delete('tbl_news',$id);
            if($res){
                $this->session->set_flashdata('message', 'News Deleted Successfully');
            }else{
                $this->session->set_flashdata('message', 'Error while Deleting News');
            }
            redirect('Admin/Settings/News');
        } else {
            redirect('Admin/login');
        }
    }
    public function popup() {
        if (is_admin()) {
            $response = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                // $data = $this->security->xss_clean($this->input->post());   
                $this->form_validation->set_rules('caption', 'Caption', 'trim|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    
                    $config['upload_path'] = './uploads/';
                    $config['allowed_types'] = 'doc|pdf|jpg|png';
                    $config['file_name'] = 'pop' . time();
                    if($this->input->post('type') == 'image'){
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('media')) {
                            $this->session->set_flashdata('message', $this->upload->display_errors());
                        } else {    
                            $data = array('upload_data' => $this->upload->data());
                            $promoArr = array(
                                'caption' => $this->input->post('caption'),
                                'media' => $data['upload_data']['file_name'],
                                'type' => 'image'
                            ); 
                            $res = $this->Main_model->update('tbl_popup', array('id' => 1),$promoArr);
                            if ($res) {
                                $this->session->set_flashdata('message', 'Image Updated Successfully');
                            } else {
                                $this->session->set_flashdata('message', 'Error While Adding Popup Please Try Again ...');
                            }
                        }
                    }else{
                        $promoArr = array(
                            'caption' => $this->input->post('caption'),
                            'media' => $this->input->post('media'),
                            'type' => 'video'
                        ); 
                        $res = $this->Main_model->update('tbl_popup', array('id' => 1),$promoArr);
                        if ($res) {
                            $this->session->set_flashdata('message', 'Image Updated Successfully');
                        } else {
                            $this->session->set_flashdata('message', 'Error While Adding Popup Please Try Again ...');
                        }
                    }
                }
                
            }
            $response['materials'] = $this->Main_model->get_records('tbl_popup', array(), '*');
            $this->load->view('popup', $response);
        } else {
            redirect('Admin/Management/login');
        }
    }
}
