<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payout extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'security', 'email','pagination'));
        $this->load->model(array('Main_model'));
        $this->load->helper(array('admin', 'security'));
    }
	public function index()
    {
        if (is_admin()) {
            $response = array();
            $response['header'] = 'Income Ledgar';
            $config['base_url'] = base_url() . 'Admin/Payout/index';
            $config['total_rows'] = $this->Main_model->get_sum('tbl_income_wallet', [], 'ifnull(count(id),0) as sum');
            $config['per_page'] = 10;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['total_sum'] = $this->Main_model->get_sum('tbl_income_wallet', [], 'ifnull(sum(amount),0) as sum');
            $response['records'] = $this->Main_model->get_limit_records('tbl_income_wallet', [], '*', $config['per_page'], $segment);
            $this->load->view('income_ledgar', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function income($type){
        if (is_admin()) {
            $response = array();
            $response['header'] = get_income_name($type);
            $config['base_url'] = base_url() . 'Admin/Payout/income/'.$type;
            $where['type'] = $type;
            $config['total_rows'] = $this->Main_model->get_sum('tbl_income_wallet', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['total_sum'] = $this->Main_model->get_sum('tbl_income_wallet', $where, 'ifnull(sum(amount),0) as sum');
            $response['records'] = $this->Main_model->get_limit_records('tbl_income_wallet', $where, '*', $config['per_page'], $segment);
            $this->load->view('income_ledgar', $response);
        } else {
            redirect('Admin/login');
        }
    }

    public function datewise_payout(){
        if (is_admin()) {
            $response = array();
            $response['header'] = 'Datewise Payout Summary';
            $config['base_url'] = base_url() . 'Admin/Payout/datewise_payout/';
            $config['total_rows'] = $this->Main_model->payout_dates_count();
            $config['per_page'] = 5;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->datewise_payout($config['per_page'], $segment);
            foreach($response['records'] as $key => $record){
                $incomes = $this->Main_model->get_incomes('tbl_income_wallet', array('date(created_at)' => $record['date'],'amount > ' => 0), 'ifnull(sum(amount),0) as income , type');
                $response['records'][$key]['incomes'] = calculate_incomes($incomes);
            }
            $this->load->view('datewise_payout', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function date_payout($date){
        if (is_admin()) {
            $response = array();
            $response['header'] = 'Date Wise Payout';
            $config['base_url'] = base_url() . 'Admin/Payout/date_payout/'.$date;
            $where['date(created_at)'] = $date;
            $config['total_rows'] = $this->Main_model->get_sum('tbl_income_wallet', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['total_sum'] = $this->Main_model->get_sum('tbl_income_wallet', $where, 'ifnull(sum(amount),0) as sum');
            $response['records'] = $this->Main_model->get_limit_records('tbl_income_wallet', $where, '*', $config['per_page'], $segment);
            $this->load->view('income_ledgar', $response);
        } else {
            redirect('Admin/login');
        }
    }
}
