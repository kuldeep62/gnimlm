<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'security', 'pagination'));
        $this->load->model(array('Main_model'));
        $this->load->helper(array('admin', 'security'));
    }
	public function index()
    {
        if (is_admin()) {
            $response = array();
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);
            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }
            $response['header'] = 'All Users';
            $config['base_url'] = base_url() . 'Admin/Users/index/';
            $config['total_rows'] = $this->Main_model->get_sum('tbl_users', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_users', $where, 'user_id,sponser_id,name,phone,directs,package_amount,created_at', $config['per_page'], $segment);
            $this->load->view('users', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function ExportUsers(){
        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");
        $usersData = $this->Main_model->get_records('tbl_users', array(), 'user_id,name,phone,sponser_id,income,pin_amount,created_at');
        $file = fopen('php://output','w');
        $header = array("UserID","Name","Phone","Sponser ID","Income","Pin Wallet","Register Date"); 
        fputcsv($file, $header);
        foreach ($usersData as $key=>$line){ 
            fputcsv($file,$line); 
        }
        fclose($file); 
        exit; 
    }
    
    public function user_login($user_id) {
        if (is_admin()) {
            $this->session->set_userdata('user_id', $user_id);
            redirect('Dashboard/');
            exit();
        } else {
            redirect('Admin/login');
        }
    }

    public function PaidUsers(){
        if (is_admin()) {
            $response = array();
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);
            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }
            $where['paid_status'] = 1;
            $response['header'] = 'Paid Users';
            $config['base_url'] = base_url() . 'Admin/Users/PaidUsers/';
            $config['total_rows'] = $this->Main_model->get_sum('tbl_users', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_users', $where, 'user_id,sponser_id,name,phone,directs,package_amount,created_at', $config['per_page'], $segment);
            $this->load->view('paid_users', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function TodayJoinings(){
        if (is_admin()) {
            $response = array();
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);
            if (empty($where[$field]))
                $where = array();
                
            $where['date(created_at)'] = date('Y-m-d');
            $response['header'] = 'Today Joinings';
            $config['base_url'] = base_url() . 'Admin/Users/PaidUsers/';
            $config['total_rows'] = $this->Main_model->get_sum('tbl_users', $where, 'ifnull(count(id),0) as sum');
            $config['per_page'] = 5;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(4);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_users', $where, 'user_id,sponser_id,name,phone,directs,package_amount,created_at', $config['per_page'], $segment);
            $this->load->view('today_joinings', $response);
        } else {
            redirect('Admin/login');
        }
    }
}
