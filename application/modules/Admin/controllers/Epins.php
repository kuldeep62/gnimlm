<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Epins extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'security','pagination'));
        $this->load->model(array('Main_model'));
        $this->load->helper(array('admin', 'security'));
    }
	public function index($status)
    {
        if (is_admin()) {
            $response = array();
            if ($status == 0)
                $response['header'] = 'Unused Epins';
            elseif ($status == 1)
                $response['header'] = 'Used Epins';
            elseif ($status == 2)
                $response['header'] = 'Transferred';
            $config['base_url'] = base_url() . 'Admin/Epins/index/' . $status;
            $config['total_rows'] = $this->Main_model->get_sum('tbl_epins', array('status' => $status), 'ifnull(count(id),0) as sum');
            $config['per_page'] = 10;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_epins', array('status' => $status), '*', $config['per_page'], $segment);
            $this->load->view('epins', $response);
        } else {
            redirect('Admin/login');
        }
    }
    
    public function CreateEpins() {
        if (is_admin()) {
            $response = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());
                $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
                $this->form_validation->set_rules('pin_count', 'Pin Count', 'trim|numeric|required|xss_clean');
                $this->form_validation->set_rules('pin_amount', 'Pin Amount', 'trim|numeric|required|xss_clean');
                if ($this->form_validation->run() != FALSE) {
                    $user = $this->Main_model->get_single_record('tbl_users', array('user_id' => $data['user_id']), 'user_id');
                    if (!empty($user)) {
                        $pin_count = $this->input->post('pin_count');
                        for ($i = 1; $i <= $pin_count; $i++) {
                            $packArr = array(
                                'user_id' => $data['user_id'],
                                'epin' => $this->generate_pin(),
                                'amount' => $data['pin_amount']
                            );
                            $res = $this->Main_model->add('tbl_epins', $packArr);
                        }

                        if ($res == TRUE) {
                            $this->session->set_flashdata('message', 'Epins Generated Successfully');
                        } else {
                            $this->session->set_flashdata('message', 'Error While Generating Epins  Please Try Again ...');
                        }
                    } else {
                        $this->session->set_flashdata('message', 'Invalid User ID');
                    }
                }
            }
            $response['pin_amount'] = $this->Main_model->get_records('tbl_package', array(), '*');
            $this->load->view('create_epins', $response);
        } else {
            redirect('Admin/login');
        }
    }

    public function generate_pin() {
        if (is_admin()) {
            $epin = md5(rand(100000, 9999999));
            $pin = $this->Main_model->get_single_record('tbl_epins', array('epin' => $epin), '*');
            if (!empty($pin)) {
                return $this->generate_pin();
            } else {
                return $epin;
            }
        }
    }

    public function UsersEPins() {
        if (is_admin()) {
            $response = array();
            $response['header'] = 'Users Epins';
            $response['user_pins'] = $this->Main_model->user_epins();
            $this->load->view('user_pins', $response);
        } else {
            redirect('Admin/login');
        }
    }

    public function userPinView($user_id) {
        if (is_admin()) {
            $response = array();
            $config['base_url'] = base_url() . 'Admin/Epins/userPinView/' . $user_id;
            $response['header'] = 'User Pins View';
            $config['total_rows'] = $this->Main_model->get_sum('tbl_epins', array('user_id' => $user_id), 'ifnull(count(id),0) as sum');
            $config['per_page'] = 10;
            
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_epins', array('user_id' => $user_id), '*', $config['per_page'], $segment);

            $this->load->view('epins', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function CheckUser($user_id){
        if (is_admin()) {
            $response = [];
            $user = $this->Main_model->get_single_record('tbl_users',['user_id' => $user_id],'name');
            if(!empty($user)){
                $response['success'] = true;
                $response['user'] = $user;
            }else{
                $response['success'] = false;
                $response['message'] = 'User Not Found';
            }
            echo json_encode($response);
        }else{
            redirect('Admin/login');
        }
    }
}
