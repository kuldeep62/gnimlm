<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'security','pagination'));
        $this->load->model(array('Main_model'));
        $this->load->helper(array('admin', 'security'));
    }
	public function index($status)
    {
        if (is_admin()) {
            $response = array();
            $field = $this->input->get('type');
            $value = $this->input->get('value');
            $where = array($field => $value);

            if (empty($where[$field]))
                $where = array();

            if (!empty($this->input->get('start_date'))){
                $start_date = $this->input->get('start_date');
                $where['date(created_at) >='] = $start_date;
            }else{
                $start_date = '';
            }
            if (!empty($this->input->get('end_date'))){
                $end_date = $this->input->get('end_date');
                $where['date(created_at) <='] = $end_date;
            }else{
                $end_date = '';
            }

            if ($status == 0)
                $response['header'] = 'Pending Tickets';            
            elseif ($status == 1)
                $response['header'] = 'Resolved Tickets';
            
            $where['status'] = $status;

            $config['base_url'] = base_url() . 'Admin/Support/index/' . $status;
            $config['total_rows'] = $this->Main_model->get_sum('tbl_support_message', $where ,'ifnull(count(id),0) as sum');
            
            $config['per_page'] = 10;
            $config['attributes'] = array('class' => 'page-link');
            $config['full_tag_open'] = "<ul class='pagination justify-content-end'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item ">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item  active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item ">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item next">';
            $config['last_tag_close'] = '</li>';
            $config['prev_link'] = 'Previous';
            $config['prev_tag_open'] = '<li class="page-item previous">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li  class="page-item next">';
            $config['next_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $segment = $this->uri->segment(5);
            $response['segament'] = $segment;
            $response['type'] = $field;
            $response['value'] = $value;
            $response['start_date'] = $start_date;
            $response['end_date'] = $end_date;
            $response['total_records'] = $config['total_rows'];
            $response['records'] = $this->Main_model->get_limit_records('tbl_support_message', $where, '*', $config['per_page'], $segment);
            $this->load->view('tickets', $response);
        } else {
            redirect('Admin/login');
        }
    }
    public function Ticket($id){
        if (is_admin()) {
            $response['header'] = 'Withdraw Details';
            $ticket = $this->Main_model->get_single_record('tbl_support_message',['id' => $id],'*');
            $response['user'] = $this->Main_model->get_single_record('tbl_users',['user_id' => $ticket['user_id']],'id,user_id,sponser_id,name,phone,email,package_amount,created_at,topup_date');
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data = $this->security->xss_clean($this->input->post());                
                $wArr = array(
                    'status' => 1,
                    'remark' => $data['remark'],
                );
                $res = $this->Main_model->update('tbl_support_message', array('id' => $id), $wArr);
                if ($res) {
                    $this->session->set_flashdata('message', 'Ticket Resolved Successuflly');
                } else {
                    $this->session->set_flashdata('message', 'Error while Resolving Ticket');
                }
            }
            $response['ticket'] = $this->Main_model->get_single_record('tbl_support_message',['id' => $id],'*');
            $this->load->view('ticket_view', $response);
        } else {
            redirect('Admin/login');
        }
    }
}
