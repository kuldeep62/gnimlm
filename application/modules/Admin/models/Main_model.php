<?php

class Main_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_single_record($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->row_array();
        return $res;
    }

    public function get_sum($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->row_array();
        return $res['sum'];
    }

    public function get_single_record_desc($table, $where, $select) {
        $this->db->select($select);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get_where($table, $where);
        $res = $query->row_array();
        return $res;
    }

    public function get_records($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->result_array();
        return $res;
    }
    public function get_limit_records($table, $where, $select , $limit , $offset,$show = false) {
        $this->db->select($select);
        $this->db->where($where);
        $this->db->limit($limit , $offset);
        $query = $this->db->get($table);
        $res = $query->result_array();
        if($show == true)
            echo $this->db->last_query();
        return $res;
    }

    public function get_single_object($table, $where, $select) {
        $this->db->select($select);
        $query = $this->db->get_where($table, $where);
        $res = $query->row_array();
        return $res;
    }

    public function add($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update($table, $where, $data) {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function delete($table, $id) {
        $this->db->where('id', $id);
        return $this->db->delete($table);
    }
/**to update wallet amount */
    public function update_wallet($table,$where,$field,$value) {
        $this->db->set($field,$value ,FALSE);
        $this->db->where($where);
        $this->db->update($table);
    }
/** To get User Wise Epins */
    public function user_epins($where = '') {
        $this->db->select('ifnull(count(id),0) as pin_count , user_id');
        $this->db->group_by('user_id');
        if (!empty($where))
            $this->db->where($where);
        $query = $this->db->get('tbl_epins');
        $res = $query->result_array();
        return $res;
    }

/**To get datewise payout */
    public function datewise_payout($limit,$offset) {
        $this->db->select('sum(amount) as payout,date(created_at) as date');
        $this->db->from('tbl_income_wallet');
        $this->db->where(['amount >' => 0]);
        $this->db->group_by('date(created_at)');
        $this->db->limit($limit , $offset);
        $this->db->order_by('date(created_at)','desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

/**To count dates */
    public function payout_dates_count() {
        $this->db->select('ifnull(count(date(created_at)),0) as record_count');
        $this->db->from('tbl_income_wallet');
        $this->db->where(['amount >' => 0]);
        $this->db->group_by('date(created_at)');
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }

    public function get_incomes($table, $where, $select) {
        $this->db->select($select);
        $this->db->group_by('type');
        $query = $this->db->get_where($table, $where);
        $res = $query->result_array();
        return $res;
    }

    public function date_wise_records(){
        $this->db->select('ifnull(count(date(created_at)),0) as record_count,date(created_at) as date');
        $this->db->from('tbl_users');
        $this->db->group_by('date(created_at)');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function date_wise_paid_records(){
        $this->db->select('ifnull(count(date(topup_date)),0) as record_count,date(topup_date) as date');
        $this->db->from('tbl_users');
        $this->db->group_by('date(topup_date)');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
}